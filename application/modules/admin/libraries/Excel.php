<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 13/06/2016
 * Time: 13:02
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/Classes/PHPExcel.php";

class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
}