<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 28/10/2016
 * Time: 11:55
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class chunkReadFilter implements PHPExcel_Reader_IReadFilter
{

    private $_startRow = 0;

    private $_endRow = 0;

    /**  We expect a list of the rows that we want to read to be passed into the constructor  */
    public function __construct($test) {

        $this->_startRow	= $test[0];
        $this->_endRow		= $test[0] + $test[1];
    }

    public function readCell($column, $row, $worksheetName = '') {
        //  Only read the heading row, and the rows that were configured in the constructor
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}
