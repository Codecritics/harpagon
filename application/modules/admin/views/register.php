<header class="main-header bg-blue">
<nav class="navbar navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand"><b>Formulaire d'inscription</b></a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <ul class="nav navbar-nav navbar-right" >
                <li><a href="login"><b style="color:white">Retour à la page de connexion</b></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
</header>


<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>DARK VADOR</b></a>
    </div>

    <div class="row">

        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User Info</h3>
                </div>
                <div class="box-body">
                    <?php echo $form->open(); ?>

                    <?php echo $form->bs3_text('First Name', 'first_name'); ?>
                    <?php echo $form->bs3_text('Last Name', 'last_name'); ?>
                    <?php echo $form->bs3_text('Username', 'username'); ?>
                    <?php echo $form->bs3_text('Email', 'email'); ?>

                    <?php echo $form->bs3_password('Password', 'password'); ?>
                    <?php echo $form->bs3_password('Retype Password', 'retype_password'); ?>

                    <?php if ( !empty($groups) ): ?>
                        <div class="form-group">
                            <label for="groups">Groups</label>
                            <div>
                                <?php foreach ($groups as $group): ?>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="groups[]" value="<?php echo $group->id; ?>"> <?php echo $group->name; ?>
                                    </label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php echo $form->bs3_submit(); ?>

                    <?php echo $form->close(); ?>
                </div>
            </div>
        </div>

    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.0 -->
<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>