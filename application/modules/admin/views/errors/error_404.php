<div class="error-page">
	<h2 class="headline text-yellow"> 404</h2>
	<div class="error-content">
		<h3><i class="fa fa-warning text-yellow"></i> Oups! Page inconnue.</h3>
		<p>
			Nous n'avons pas pu trouver le page que vous recherchez.
			Cependant, vous pouvez  <a href="">revenir à la page d'accueil</a> ou utiliser votre côté obscur pour creer la page en question.
		</p>
		<form action="home" class="search-form" method="GET">
			<div class="input-group">
				<input type="text" name="search" class="form-control" placeholder="Chercher">
				<div class="input-group-btn">
					<button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>