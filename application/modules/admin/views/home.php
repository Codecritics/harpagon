<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="row">
    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/box_open', 'Raccourcis'); ?>
        <?php echo modules::run('adminlte/widget/app_btn', 'fa fa-user', 'Compte', 'panel/account'); ?>
        <?php echo modules::run('adminlte/widget/app_btn', 'fa fa-sign-out', 'Déconnexion', 'panel/logout'); ?>
        <?php echo modules::run('adminlte/widget/box_close'); ?>
    </div>

    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'yellow', $count['admin_users'], '<b>Utilisateurs</b>', 'fa fa-users', 'panel/admin_user'); ?>
    </div>
    <div class="col-md-4">
        <div class='info-box'>
            <a>
                <span class='info-box-icon bg-green'><i class='fa fa-euro'></i></span>
            </a>
            <div class='info-box-content'>
                <span class='info-box-text'><b> Conversion En Kilo Euros</b></span>
                <span class='info-box-number'>
                    <input id="conversion" type="checkbox" data-toggle="toggle" data-on="Kilo Euros" data-off="UO">
                </span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#conversion').change(function() {
                console.log($(this).prop('checked'));
                if( $(this).prop('checked') == true)
                {
                    $.ajax({
                        url: "table/convert_uo/1",
                    })
                }
                else if( $(this).prop('checked') == false)
                {
                    $.ajax({
                        url: "table/convert_uo/0",
                    })
                }
            })
        })
    </script>
</div>
<div class="row">
    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/box_open', 'Paramètres de l\'application'); ?>
        <?php echo modules::run('adminlte/widget/app_btn', 'fa fa-eraser', 'Reinitialiser', 'panel/delete'); ?>
        <?php echo modules::run('adminlte/widget/box_close'); ?>
    </div>
    <div class="col-md-8">
        <div class="box box-info collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Suivi des fichiers importés</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Mois</th>
                            <th>Photos Serveur</th>
                            <th>Photos Application</th>
                            <th>MOULIBEX</th>
                            <th>GPF</th>
                            <th>Clotûre SR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="">Janvier</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("January", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("January", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("January", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("January", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("January", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Février</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("February", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("February", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("February", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("February", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("February", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>


                        </tr>
                        <tr>
                            <td><a href="">Mars</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("March", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("March", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("March", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("March", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("March", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Avril</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("April", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("April", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("April", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("April", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("April", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Mai</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("May", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("May", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("May", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("May", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("May", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Juin</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("June", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("June", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("June", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("June", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("June", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Juillet</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("July", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("July", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("July", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("July", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("July", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Août</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("August", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("August", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("August", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("August", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("August", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Septembre</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("September", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("September", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("September", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("September", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("September", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Octobre</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("October", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("October", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("October", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("October", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("October", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Novembre</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("November", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("November", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("November", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("November", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("November", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="">Decembre</a></td>
                            <td><?php
                                if (array_key_exists('Serveur',$result_array_value) && in_array("December", $result_array_value['Serveur'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td><?php
                                if (array_key_exists('Application',$result_array_value) && in_array("December", $result_array_value['Application'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?></td>
                            <td>
                                <?php
                                if (array_key_exists('MOULIBEX',$result_array_value) && in_array("December", $result_array_value['MOULIBEX'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('gpf',$result_array_value) && in_array("December", $result_array_value['gpf'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (array_key_exists('sr',$result_array_value) && in_array("December", $result_array_value['sr'])) {
                                    echo '<span class="label label-success">Importé</span></td>';
                                }
                                else {
                                    echo '<span class="label label-danger">Non Importé</span></td>';
                                }
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
        </div>
    </div>
</div>