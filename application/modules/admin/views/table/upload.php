<link href="../bower_components/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet"
      type="text/css"/>
<link href="../bower_components/toastr/toastr.css" rel="stylesheet"/>
<script src="../bower_components/toastr/toastr.js"></script>

<div class="form-group">
    <label for="input-id" class="control-label">Veuillez importer votre fichier</label>
    <input id="input-id" type="file" class="file" data-preview-file-type="text" name="input-name" >
</div>

<div id="div_nna" class="form-group " onload="$('#div_nna').fadeOut("slow");">
    <label for="nna" class="control-label">Veuillez indiquer le NNA</label>
    <!--  <input id="application" type="text" class="form-control" name="application"> !-->
    <select data-placeholder="Inserez un Nna ou Selectionnez un NNA" class="form-control select2" id="nna" name="nna"
            multiple="multiple">
        <?php
        $temp_ar =["January","0000-01-01","February","0000-02-01","March","0000-03-01","April","0000-04-01","May","0000-05-01","June","0000-06-01","July","0000-07-01","August","0000-08-01","September","0000-09-01","October","0000-10-01","November","0000-11-01","December","0000-12-01"];
        for ($i = 0; $i < count($nna); $i++) {
            foreach ($nna[$i] as $row) {
                if ($row === $this->input->post('nna'))
                    echo "<option selected value='" . $row . "' >" . $row . "</option>";
                else
                    echo "<option value='" . $row . "'>" . $row . "</option>";
            }
        }
        ?>
    </select>
</div>

<div id="div_application" class="form-group " >
    <label for="application" class="control-label">Veuillez indiquer l'Application</label>
    <!--  <input id="application" type="text" class="form-control" name="application"> !-->
    <select data-placeholder="Inserez une Application ou Selectionez une Application" class="form-control select2"
            id="application" name="application" multiple="multiple">
        <?php
        for ($i = 0; $i < count($application); $i++) {
            foreach ($application[$i] as $row) {
                if ($row === $this->input->post('application'))
                    echo "<option selected value='" . $row . "' >" . $row . "</option>";
                else
                    echo "<option value='" . $row . "'>" . $row . "</option>";
            }
        }
        ?>
    </select>
</div>
<div id="div_mois_c" class="form-group ">
    <label for="mois_c" class="control-label">Veuillez Selectionner le mois du Réalisé</label>
    <select class="form-control select2" multiple="multiple"
            data-placeholder="Choisir un mois" id="mois_c" name="mois_c">

        <?php
        /*  for ($i = 0; $i < count($check); $i++) {
             foreach ($check[$i] as $row) {
                 if ($row == "January")
                     echo "<option value='0000-02-01'>Février</option>";
                 elseif ($row == "February")
                     echo "<option value='0000-03-01'>Mars</option>";
                 elseif ($row == "March")
                     echo "<option value='0000-04-01'>Avril</option>";
                 elseif ($row == "April")
                     echo "<option value='0000-05-01'>Mai</option>";
                 elseif ($row == "May")
                     echo "<option value='0000-06-01'>Juin</option>";
                 elseif ($row == "June")
                     echo "<option value='0000-07-01'>Juillet</option>";
                 elseif ($row == "July")
                     echo "<option value='0000-08-01'>Août</option>";
                 elseif ($row == "August")
                     echo "<option value='0000-09-01'>Septembre</option>";
                 elseif ($row == "September")
                     echo "<option value='0000-10-01'>Octobre</option>";
                 elseif ($row == "Octobre")
                     echo "<option value='0000-11-01'>Novembre</option>";
                 elseif ($row == "November")
                     echo "<option value='0000-12-01'>Decembre</option>";
             }
         }*/
        ?>


    </select>
</div>
<div id="div_commentaire"class="form-group " >
    <label for="default" class="control-label">Veuillez entrer un commentaire pour votre fichier</label>
    <textarea id="commentaire" class="form-control" maxlength="225" rows="2"
              placeholder="Un maximum de 225 Caractères"></textarea>
</div>

<?//php var_dump($check);
?>


<div id="div_previsionel" class="form-group ">
    <label for="previsionel" class="control-label" id="label_previsionel">Veuillez Selectionner le Type de
        prévisionel </label>
    <select class="form-control select2" multiple="multiple"
            data-placeholder="Choisir un mois" id="previsionel" name="previsionel">
        <option value='BA'>Prévisionel BA</option>
        <option value='R1'>Prévisionel R1</option>
        <option value='R2'>Prévisionel R2</option>
        <option value='interm_janvier'>Interm Janvier</option>
        <option value='interm_fevrier'>Interm Février</option>
        <option value='interm_mars'>Interm Mars</option>
        <option value='interm_avril'>Interm Avril</option>
        <option value='interm_mai'>Interm Mai</option>
        <option value='interm_juin'>Interm Juin</option>
        <option value='interm_juillet'>Interm Juillet</option>
        <option value='interm_aout'>Interm Aout</option>
        <option value='interm_septembre'>Interm Septembre</option>
        <option value='interm_octobre'>Interm Octobre</option>
        <option value='interm_novembre'>Interm Novembre</option>
        <option value='interm_decembre'>Interm Decembre</option>
    </select>
</div>


<!-- </form> --!>

<script src="../bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
     This must be loaded before fileinput.min.js -->
<script src="../bower_components/bootstrap-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for HTML files.
     This must be loaded before fileinput.min.js -->
<script src="../bower_components/bootstrap-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- the main fileinput plugin file -->
<script src="../bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
<!-- bootstrap.js below is needed if you wish to zoom and view file content
     in a larger detailed modal dialog -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<!-- optionally if you need a theme like font awesome theme you can include
    it as mentioned below -->
<script src="../bower_components/bootstrap-fileinput/themes/fa/theme.js"></script>
<!-- optionally if you need translation for your language then include
    locale file as mentioned below -->
<script src="../bower_components/bootstrap-fileinput/js/locales/fr.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
<script src="../assets/plugins/select2/select2.full.min.js"></script>


<script type="text/javascript">
    var application = $('#application').val();
    var nna = $('#nna').val();
    var previsionel = $('#previsionel').val();
    var commentaire = $('#commentaire').val();
    var mois_c = $('#mois_c').val();
    $('#div_nna').fadeOut();
    $('#div_mois_c').fadeOut();
    $('#div_previsionel').fadeOut();
    $('#div_application').fadeOut();
    $('#div_commentaire').fadeOut();

    $('#previsionel').prop("disabled", true);
    $('#application').prop("disabled", true);
    $('#nna').prop("disabled", true);
    $('#mois_c').prop("disabled", true);
    $(".select2").select2({
        tags: true,
        maximumSelectionLength: 1,
        language: "fr"
    });
    $('select').trigger('change');

    initPluginSelectApp = function(){  $("#mois_c").select2({
        data: [

            <?php
            if(!empty($result_array_value['Application'])) {
                for ($i = 0, $j = 1; $i < count($result_array_value['Application']); $i = $i + 2, $j = $j + 2) {
                    if (in_array($result_array_value['Application'][$i], $result_array_value['Application'])) {
                        echo '{
                                id: "' . $result_array_value['Application'][$j] . '",' .
                            'text:"' . $result_array_value['Application'][$i] . '"},';
                    }
                }
                echo '{id: "' . $temp_ar[$j] . '",' .
                    'text:"' . $temp_ar[$i] . '"},';
            }
            else if (empty($result_array_value['Application'])) {
                echo '{
                                id: "0000-01-01",' .
                    'text:"January"},';
            }
            ?>
        ]
    });
    }

   var $el =  $("#input-id"), initPlugin = function(){
       $el.fileinput({
        showUpload: true,
        uploadUrl: "upload/do_upload",
        allowedFileExtensions: ['csv', 'xlsx', 'xls'],
        uploadAsync: false,
        removeFromPreviewOnError: true,
        uploadExtraData: function () {
            return {
                mois_c: $('#mois_c').val(),
                application: $('#application').val(),
                nna: $('#nna').val(),
                commentaire: $('#commentaire').val(),
                previsionel: $('#previsionel').val()
            };
        },
        'previewFileType': 'any',
        language: 'fr'
    });
   };
    initPlugin();
    $('#form_test input').change(function () {

    });
    $("#input-id").on('filebatchselected', function () {
        var name_file = $('input[type=file]').val().split('\\').pop();
        if (name_file.indexOf("moulibex") !== -1) {
            toastr.success("Fichier moulibex");
            $('#div_previsionel').fadeIn();
            $('#div_application').fadeIn();
            $('#div_commentaire').fadeIn();
            $('#div_nna').fadeIn();
            $('#div_mois_c').fadeOut("slow");
            $('#previsionel').prop("disabled", false);
            $('#application').prop("disabled", false);
            $('#nna').prop("disabled", false);
            $('#div_previsionel').prop("disabled", true);
            $('#div_application').prop("disabled", true);
            $('#div_nna').prop("disabled", true);
            $('#mois_c').prop("disabled", true);
            // `str` contains "geordie" *exactly* (doesn't catch "Geordie" or similar)
        }
        else {
            $('#div_mois_c').fadeIn();
            $('#div_commentaire').fadeIn();
            $('#div_previsionel').fadeOut("slow");
            $('#div_application').fadeOut("slow");
            $('#div_nna').fadeOut("slow");
            $('#previsionel').prop("disabled", true);
            $('#application').prop("disabled", true);
            $('#nna').prop("disabled", true);
            $('#mois_c').prop("disabled", false);
            if (name_file.indexOf("Applications") !== -1) {
                toastr.success("Fichier Photo Applications");
                initPluginSelectApp();
            }
            else if (name_file.indexOf("Serveurs") !== -1) {
                toastr.success("Fichier Photo Serveurs");
                $("#mois_c").select2({
                    data: [

                        <?php
                            if(!empty($result_array_value['Serveur'])) {
                                for ($i = 0, $j = 1; $i < count($result_array_value['Serveur']); $i = $i + 2, $j = $j + 2) {
                                    if (in_array($result_array_value['Serveur'][$i], $result_array_value['Serveur'])) {
                                        echo '{
                                id: "' . $result_array_value['Serveur'][$j] . '",' .
                                            'text:"' . $result_array_value['Serveur'][$i] . '"},';
                                    }
                                }
                                echo '{id: "' . $temp_ar[$j] . '",' .
                                    'text:"' . $temp_ar[$i] . '"},';
                            }
                            else if (count($result_array_value['Serveur']) == 0) {
                                echo '{
                                id: "0000-01-01",' .
                                    'text:"January"},';
                            }
                        ?>
                    ]
                });
            }
            else if (name_file.indexOf("Exploitation") !== -1) {
                toastr.success("Fichier GPF");
                $("#mois_c").select2({
                    data: [

                        <?php
                        if(!empty($result_array_value['gpf'])) {
                            for ($i = 0, $j = 1; $i < count($result_array_value['gpf']); $i = $i + 2, $j = $j + 2) {
                                if (in_array($result_array_value['gpf'][$i], $result_array_value['gpf'])) {
                                    echo '{
                                id: "' . $result_array_value['gpf'][$j] . '",' .
                                        'text:"' . $result_array_value['gpf'][$i] . '"},';
                                }
                            }
                            echo '{id: "' . $temp_ar[$j] . '",' .
                                'text:"' . $temp_ar[$i] . '"},';
                        }
                        else if (count($result_array_value['gpf']) == 0) {
                            echo '{
                                id: "0000-01-01",' .
                                'text:"January"},';
                        }
                        ?>
                    ]
                });
            }
            else if (name_file.indexOf("IQ") !== -1) {
                toastr.success("Fichier Cloture SR");
                $("#mois_c").select2({
                    data: [

                        <?php
                        if(!empty($result_array_value['sr'])) {
                            for ($i = 0, $j = 1; $i < count($result_array_value['sr']); $i = $i + 2, $j = $j + 2) {
                                if (in_array($result_array_value['sr'][$i], $result_array_value['sr'])) {
                                    echo '{
                                id: "' . $result_array_value['sr'][$j] . '",' .
                                        'text:"' . $result_array_value['sr'][$i] . '"},';
                                }
                            }
                            echo '{id: "' . $temp_ar[$j] . '",' .
                                'text:"' . $temp_ar[$i] . '"},';
                        }
                        else if (count($result_array_value['sr']) == 0) {
                            echo '{
                                id: "0000-01-01",' .
                                'text:"January"},';
                        }
                        ?>
                    ]
                });
            }

        }

        //window.alert(name_file);

    });
    $('#input-id').on('filebatchpreupload', function () {
        var application = $('#application').val();
        var previsionel = $('#previsionel').val();
        var commentaire = $('#commentaire').val();
        var nna = $('#commentaire').val();
        var mois_c = $('#mois_c').val();
        console.log(application);
        console.log(commentaire);
        console.log(previsionel);
        var name_file = $('input[type=file]').val().split('\\').pop();
       if (name_file.indexOf("moulibex") !== -1) {
            if (application === '' && commentaire === '' && previsionel === null) {

                return {
                    message: 'Veuillez tout d\'abord indiquer l\'Application, le commentaire et le prévisionel',

                };
            }

            else if (application === '') {

                return {
                    message: 'Veuillez tout d\'abord indiquer l\'Application',

                };
            }
            else if (commentaire === '') {
               // initPlugin();
                return {
                    message: 'Veuillez tout d\'abord indiquer le Commentaire',
                };
            }
            else if (previsionel === null) {
                initPlugin();
               return {
                    message: 'Veuillez tout d\'abord indiquer le previsionel',
                };

                //toastr.error("Veuillez tout d\'abord indiquer le previsionel");
            }

        }
        else {
            if (commentaire === '') {

                return {
                    message: 'Veuillez tout d\'abord indiquer le commentaire',
                };

            }
            else if (mois_c === null) {

                return {
                    message: 'Veuillez tout d\'abord indiquer le Mois concerné',
                };
            }
        }

        console.log('File batch pre upload');
    });


    $('#input-id').on('filepreajax', function (event, previewId, index) {
        console.log('File pre ajax triggered');
    });
    $('#input-id').on('filepreupload', function () {
        console.log('File pre upload triggered');
    });
    $('#input-id').on('fileloaded', function () {
        console.log("fileloaded");
        console.log(application);
    });
    $("#input-id").on('filebatchuploadcomplete', function () {
        initPluginSelectApp();
        toastr.success('Import Réussi avec Succès');
         setTimeout(function () {
         location.reload();
         }, 3000);

        console.log();
    });

</script>


<script type="text/javascript">
    function bs_input_file() {
        $(".input-file").before(
            function () {
                if (!$(this).prev().hasClass('input-ghost')) {
                    var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                    element.attr("name", $(this).attr("name"));
                    element.change(function () {
                        element.next(element).find('input').val((element.val()).split('\\').pop());
                    });
                    $(this).find("button.btn-choose").click(function () {
                        element.click();
                    });
                    $(this).find("button.btn-reset").click(function () {
                        element.val(null);
                        $(this).parents(".input-file").find('input').val('');
                    });
                    $(this).find('input').css("cursor", "pointer");
                    $(this).find('input').mousedown(function () {
                        $(this).parents('.input-file').prev().click();
                        return false;
                    });
                    return element;
                }
            }
        );
    }
    $(function () {
        bs_input_file();
    });
</script>
<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 04/05/2016
 * Time: 15:28
 */
