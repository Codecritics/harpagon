<script type="text/javascript" src="../assets/plugins/fusioncharts-suite-xt/js/fusioncharts.js"></script>
<script type="text/javascript" src="../assets/plugins/angular/angular.js"></script>
<script type="text/javascript" src="../assets/plugins/Chart.js/Chart.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-chart.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-fusioncharts.min.js"></script>
<script type="text/javascript"
        src="../assets/plugins/fusioncharts-suite-xt/js/themes/fusioncharts.theme.ocean.js"></script>
<div class="row">

    <div class="col-md-6">
        <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Graphique 1</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->

        </div><!-- /.box-header -->
        <div id="chart" class="box-body">
            <canvas id="mystat" ></canvas>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- box-footer -->
    </div><!-- /.box -->
    <script>
        var ctx = document.getElementById("mystat").getContext("2d");
        var data = {
            labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
            datasets: [
                {
                    label: "My First dataset",
                    backgroundColor: "rgba(179,181,198,0.2)",
                    borderColor: "rgba(179,181,198,1)",
                    pointBackgroundColor: "rgba(179,181,198,1)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(179,181,198,1)",
                    data: [65, 59, 90, 81, 56, 55, 40]
                },
                {
                    label: "My Second dataset",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    pointBackgroundColor: "rgba(255,99,132,1)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(255,99,132,1)",
                    data: [28, 48, 40, 19, 96, 27, 100]
                }
            ]
        };
        var mystat = new Chart(ctx, {
            type: "radar",
            data: data,
            options: {
                scale: {
                    reverse: true,
                    ticks: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
</div>
    <div class="col-md-6">
    <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Graphique 2</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->

        </div><!-- /.box-header -->
        <div id="chart" class="box-body">
            <canvas id="myChart" width="740" height="200"></canvas>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Evolution de AEL et SIMM en fonction de UOH
        </div><!-- box-footer -->
    </div><!-- /.box -->
    <script>
        // Get the context of the canvas element we want to select
        var ctx = document.getElementById("myChart").getContext("2d");
        var data = {
            labels: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet"],
            datasets: [
                {
                    label: "SIMM",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(215, 137, 255, 0.9)",
                    borderColor: "rgba(215, 137, 255, 0.9)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(215, 137, 255, 0.9)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(215, 137, 255, 0.9)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 30, 10, 63, 76, 89, 100],
                },
                {
                    label: "AEL",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40],
                }
            ]
        };
        // Instantiate a new chart using 'data' (defined below)
        var myChart = new Chart.Line(ctx, {
            data: data,
        });

    </script>
    </div>

</div>
