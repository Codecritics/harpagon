<script type="text/javascript" src="../assets/plugins/fusioncharts-suite-xt/js/fusioncharts.js"></script>
<script type="text/javascript" src="../assets/plugins/angular/angular.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-chart.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-fusioncharts.min.js"></script>
<script type="text/javascript"
        src="../assets/plugins/fusioncharts-suite-xt/js/fusioncharts-jquery-plugin.min.js"></script>

<script type="text/javascript" src="http://momentjs.com/downloads/moment.min.js"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="../assets/plugins/ionslider/ion.rangeSlider.min.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
<script src="../assets/plugins/select2/select2.full.min.js"></script>
<!-- Ion Slider -->
<link rel="stylesheet" href="../assets/plugins/ionslider/ion.rangeSlider.css">
<!-- ion slider Nice -->
<link rel="stylesheet" href="../assets/plugins/ionslider/ion.rangeSlider.skinNice.css">
<script type="text/javascript"
        src="../assets/plugins/fusioncharts-suite-xt/js/themes/fusioncharts.theme.ocean.js"></script>

<link href="../assets/plugins/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
<script src="../assets/plugins/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
<div class="container-fluid">
    <?php $this->load->view('table/sum_table_filters'); ?><!-- /.box -->

    <?php $this->load->view('table/sum_table_compare'); ?>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-stats-tab" data-toggle="tab"><i class="fa fa-wrench"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Activités Récentes</h3>


            <h3 class="control-sidebar-heading">Années</h3>
            <form action="admin/table/sum_table/" method="post" name="form1" id="form1">
                <div>
                    <div class="btn-group btn-group-vertical" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option1" onchange=" window.location.replace('table/sum_table/2016')"> 2016
                        </label>
                        <br>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option2" value="2015" onchange=" window.location.replace('table/sum_table/2015')"> 2015
                        </label>
                        <br>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" value="2014" onchange=" window.location.replace('table/sum_table/2014')"> 2014
                        </label>
                    </div>
                </div>
            </form>

            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Onglet 2</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane"
             id="control-sidebar-settings-tab" <?php if (count($date) == 0) echo 'style="display:none"'; ?>>

            <h3 class="control-sidebar-heading">Paramètres</h3>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                    Conversion en k€
                    <?php if ($this->input->post('trigger_convert') == 'k€')
                        echo '
                    <input class="pull-right" id="trigger_convert" type="radio" name="trigger_convert" value="k€" checked>';
                    else
                        echo '<input class="pull-right" id="trigger_convert" type="radio" name="trigger_convert" value="k€">' ?>
                </label>
                <p>
                    Cliquez ci-dessus pour convertir en Kilo Euros
                </p>
            </div>
            <div class="form-group">
                <label class="control-sidebar-subheading">
                    Conversion en UO
                    <?php if ($this->input->post('trigger_convert') == 'uo')
                        echo '
                    <input class="pull-right" id="trigger_convert" type="radio" name="trigger_convert" value="k€" checked>';
                    else if ($this->input->post('trigger_convert') == null)
                        echo '<input class="pull-right" id="trigger_convert" type="radio" name="trigger_convert" value="uo" checked>';
                    else
                        echo '<input class="pull-right" id="trigger_convert" type="radio" name="trigger_convert" value="uo" checked>';
                    ?>
                </label>
                <p>
                    Cliquez ci-dessus pour convertir en UO
                </p>
            </div>
            <ul class="control-sidebar-menu">
                <li>
                    <div class="menu-info">
                        <?php echo $form->bs3_submit('Filtre', 'btn bg-maroon control-sidebar-subheading'); ?>
                    </div>
                </li>
            </ul>
            <?php echo $form->close(); ?>
            <!--   <div class="form-group">
                   <label class="control-sidebar-subheading">
                       Report panel usage
                       <input type="checkbox" class="pull-right" checked>
                   </label>

                   <p>
                       Some information about this general settings option
                   </p>
               </div>
               <!-- /.form-group -->

            <!--    <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Allow mail redirect
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

            <!--   <div class="form-group">
                   <label class="control-sidebar-subheading">
                       Expose author name in posts
                       <input type="checkbox" class="pull-right" checked>
                   </label>

                   <p>
                       Allow the user to show his name in blog posts
                   </p>
               </div>
               <!-- /.form-group -->

            <!--  <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                  <label class="control-sidebar-subheading">
                      Show me as online
                      <input type="checkbox" class="pull-right" checked>
                  </label>
              </div>
              <!-- /.form-group -->

            <!--    <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Turn off notifications
                        <input type="checkbox" class="pull-right">
                    </label>
                </div>
                <!-- /.form-group -->

            <!--    <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Delete chat history
                        <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div>
                <!-- /.form-group -->
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>

