<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Section des Filtres à appliquer</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-toggle="control-sidebar"><i class="fa fa-cog"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <!-- /.box-header -->
        <?php
        echo $form->open('admin/table/sum_table#tab_3', 'class="container"'); ?>

        <label
                for="range_1"> <?php if (count($date) == 0) echo 'Veuillez tout d\'abord uploader des fichiers  avant de pouvoir selectionner les dates.'; else echo 'Veuillez Selectionner votre plage de mois'; ?></label>
        <br>
        <div <?php if (count($date) == 0) echo 'style="display:none"'; ?>>
            <input id="range_1" type="text" name="range_1"
                   value="<?php echo $this->input->post('range_1'); ?>">
        </div>
        <br>

        <br>


        <div <?php if (count($date) == 0) echo 'style="display:none"'; ?> class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>OTP</label>
                    <select class="form-control select2" multiple="multiple"
                            data-placeholder="Selectionner Un ou plusieur OTP" id="otp" name="otp[]">
                        <?php
                        $otp_input = $this->input->post('otp');
                        for ($i = 0; $i < count($otp); $i++) {/*
                            if ($i == 0)
                                echo "<option selected  value='null' >Choisir une OTP</option>";*/
                            foreach ($otp[$i] as $row) {
                                for ($j = 0; $j < count($otp_input); $j++) {
                                    if ($row === $otp_input[$j])
                                        echo "<option selected value='" . $row . "' >" . $row . "</option>";
                                }
                                echo "<option value='" . $row . "'>" . $row . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <label for="application" class="col-sm-2 control-label ">Application</label>
                <select data-placeholder="Selectionner une Application" class="form-control select2" id="application"
                        name="application" multiple="multiple">
                    <?php
                    for ($i = 0; $i < count($application); $i++) {
                        foreach ($application[$i] as $row) {
                            if ($row === $this->input->post('application'))
                                echo "<option selected value='" . $row . "' >" . $row . "</option>";
                            else
                                echo "<option value='" . $row . "'>" . $row . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-4">
                <label for="previ" class=" control-label">Type de Prévisionnel</label>
                <select class="form-control" id="previ" name="previ">
                    <?php
                    for ($i = 0; $i < count($previ); $i++) {
                        if ($i == 0)
                            echo "<option selected value='null' >Choisir un type de Prévisionel</option>";
                        foreach ($previ[$i] as $row) {
                            if ($row === $this->input->post('previ'))
                                echo "<option selected value='" . $row . "' >" . $row . "</option>";
                            else
                                echo "<option value='" . $row . "'>" . $row . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
         <!--   <div class="col-md-4">
                <label for="conversion" class=" control-label">Conversion en Kilo Euros</label>
                <input  id="conversion" type="checkbox" data-toggle="toggle" data-on="Kilo Euros" data-off="UO">
            </div> !-->
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-5">
                <?php
                if (count($date) == 0)
                    echo $form->bs3_submit('Effectuer le Filtre', 'btn bg-maroon hidden');
                else
                    echo $form->bs3_submit('Effectuer le Filtre', 'btn bg-maroon');
                ?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <script type="text/javascript">
        $(document).ready(function () {


            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
            $("#application").select2({
                maximumSelectionLength: 1,
                language: "fr"
            });
        });
    </script>
</div>


</div>