<?php ?>
<div class="tab-pane" id="cloture_sr">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li><a href="#graph_cloture_sr" data-toggle="tab">
                    <i class='fa fa-bar-chart'></i>
                    Graphique
                </a>
            </li>
            <li><a href="#table_cloture_sr" data-toggle="tab">
                    <i class='fa fa-bar-chart'></i>
                    Tableau
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content">

        <div class="tab-pane fade" id="graph_cloture_sr">
            <div id="bar_cloture_sr"></div>
            <script type="text/javascript">
                $("#bar_cloture_sr").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Insertions en Exploitation",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs (<?php if (!isset($_POST['trigger_convert'])) echo 'uo'; else echo $this->input->post('trigger_convert');?>)",
                            "exportEnabled": "1",
                            "theme": "ocean",
                            <?php if (!isset($_POST['trigger_convert'])) echo ''; else if ($_POST['trigger_convert'] == "k€") echo '"numberSuffix": "€",'; else if ($_POST['trigger_convert'] == "uo") echo '';?>
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($IntégrationQualification['label']['IntégrationQualification']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($IntégrationQualification['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {

                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($IntégrationQualification['sr']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($Stockagededonnées['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
            </script>
        </div>

        <div class="tab-pane fade" id="table_cloture_sr">
            <table class="table  table-hover table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Insertion en exploitation - Sécurisation renforcée</th>
                    <th>Type de Fichier</th>
                    <!--  <th>Location</th> -->
                    <!--  <th>Intégration/Qualification</th> -->
                </tr>
                </thead>
                <tbody>
                <?php
                if (array_key_exists('sr', $IntégrationQualification)) {
                    echo '<tr>' .
                        '<td >' . '' . $IntégrationQualification['sr'][0]['value'] . '' . '</td >' .
                        '<td>SR</td >'
                        . '</tr>';
                }
                ?>
                </tbody>
            </table>
            <div class="table-responsive">
                <?php
                echo $table_sum->generate($table_sum_array['Application Hosting']);
                ?>
            </div>
        </div>

        <!--
        <div class="tab-pane fade" id="toile_3">
            Ici le graphique des toiles
        </div>
        <div class="tab-pane fade" id="pie_3">
            Ici le graphique des camemberts
        </div> !-->
    </div>


</div>