<?php ?>
<div class="tab-pane" id="details_services">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li>
                <a href="#application_hosting" data-toggle="tab">
                    <i class='fa fa-table'></i>
                    Application Hosting
                </a>
            </li>
            <li>
                <a href="#serveur" data-toggle="tab">
                    <i class='fa fa-table'></i>
                    Serveur Virtualisé
                </a>
            </li>
            <li>
                <a href="#serveur_dedie" data-toggle="tab">
                    <i class='fa fa-table'></i>
                    Serveur Dédié
                </a>
            </li>
            <li><a href="#stockage" data-toggle="tab">
                    <i class='fa fa-bar-chart'></i>
                    Stockage
                </a>
            </li>
            <li><a href="#sauvegarde" data-toggle="tab">
                    <i class='fa fa-bar-chart'></i>
                    Sauvegarde
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade" id="application_hosting">
            Graphiques des serveurs
            <!--
                                <div class="box-body table-responsive no-padding">
                                    <table class="table  table-hover table-bordered table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Type de Fichier</th>
                                            <th>Mois Concerne</th>
                                            <th>Sauvegardes du mois - Bas débit en Go</th>
                                            <th>Sauvegardes du mois - Standard en Go</th>
                                            <th>Sauvegardes du mois - Haut débit en Go</th>
                                            <th>Sauvegardes du mois - Très haut débit en Go</th>
                                            <th>Sauvegardes du mois - Externalisation journalière ou hebdo en Go</th>
                                        </tr>
                                        </thead>
                                        <tbody>
            <?php foreach ($sum_table as $row) {
                echo '<tr>' .
                    '<td >' . $row->type_fichier . '</td >' .
                    '<td >' . $row->{'MONTHNAME(mois_concerne)'} . '</td >' .
                    '<td >' . $row->{'SUM(smbd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(sms_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smhd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smthd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smejh_go)'} . '</td >'
                    . '</tr>';
            }
            ?>
                                        </tbody>
                                    </table>

                                </div>
                                !-->
            <div id="bar_application_hosting"></div>
            <br>
            <div id="toile_application_hosting"></div>
            <script type="text/javascript">
                $("#bar_application_hosting").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Applications Hosting",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($ApplicationHosting['label']['ApplicationHosting']);
                                ?>
                            }
                        ]


                        ,
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($ApplicationHosting['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($ApplicationHosting['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $ApplicationHosting)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($ApplicationHosting['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                $("#toile_application_hosting").insertFusionCharts({
                    type: "radar",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Applications Hosting",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($ApplicationHosting['label']['ApplicationHosting']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($ApplicationHosting['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($ApplicationHosting['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $ApplicationHosting)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($ApplicationHosting['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });


            </script>
        </div>
        <div class="tab-pane fade" id="serveur">
            Graphiques des serveurs
            <!--
                                <div class="box-body table-responsive no-padding">
                                    <table class="table  table-hover table-bordered table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Type de Fichier</th>
                                            <th>Mois Concerne</th>
                                            <th>Sauvegardes du mois - Bas débit en Go</th>
                                            <th>Sauvegardes du mois - Standard en Go</th>
                                            <th>Sauvegardes du mois - Haut débit en Go</th>
                                            <th>Sauvegardes du mois - Très haut débit en Go</th>
                                            <th>Sauvegardes du mois - Externalisation journalière ou hebdo en Go</th>
                                        </tr>
                                        </thead>
                                        <tbody>
            <?php foreach ($sum_table as $row) {
                echo '<tr>' .
                    '<td >' . $row->type_fichier . '</td >' .
                    '<td >' . $row->{'MONTHNAME(mois_concerne)'} . '</td >' .
                    '<td >' . $row->{'SUM(smbd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(sms_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smhd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smthd_go)'} . '</td >' .
                    '<td >' . $row->{'SUM(smejh_go)'} . '</td >'
                    . '</tr>';
            }
            ?>
                                        </tbody>
                                    </table>

                                </div>
                                !-->
            <div id="bar_serveur"></div>
            <br>
            <div id="toile_serveur"></div>
            <script type="text/javascript">
                $("#bar_serveur").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Serveurs Hostings",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($HostingVirtualisé['label']['HostingVirtualisé']);
                                ?>
                            }
                        ]


                        ,
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingVirtualisé['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingVirtualisé['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($HostingVirtualisé['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                $("#toile_serveur").insertFusionCharts({
                    type: "radar",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Serveurs Hostings",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($HostingVirtualisé['label']['HostingVirtualisé']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingVirtualisé['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingVirtualisé['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($HostingVirtualisé['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });


            </script>
        </div>
        <div class="tab-pane fade" id="serveur_dedie">
            <!--     <form id="myForm2">
                     <div class="btn-group btn-group-justified"
                          data-toggle="buttons">
                         <label class="btn btn-primary active" id="sort_graph">
                             <input type="radio" name="options" id="option1" value="mscombi2d">
                             <i class='fa fa-bar-chart'></i> Représentation en Barres
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option2" value="radar">
                             Représentation en Toiles
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option3" value="msline">
                             <i class='fa fa-line-chart'></i>
                             Représentation en Lignes
                         </label>
                     </div>
                 </form> !-->
            <div id="bar_serveur_dedie"></div>
            <br>
            <div id="toile_serveur_dedie"></div>

            <script type="text/javascript">
                /*  $('#myForm2 input').on('change', function () {*/
                $("#bar_serveur_dedie").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Serveurs Dédiés",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($HostingDédié['label']['HostingDédié']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingDédié['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {


                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingDédié['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($HostingDédié['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                $("#toile_serveur_dedie").insertFusionCharts({
                    type: "radar",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Serveurs Dédiés",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean"
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($HostingDédié['label']['HostingDédié']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingDédié['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {


                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($HostingDédié['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($HostingDédié['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                /*   $('#chart-container').updateFusionCharts({
                 'type': $('input[name="options"]:checked', '#myForm').val()
                 });*/
                /*     if ($('input[name="options"]:checked', '#myForm2').val() == "pie3d") {

                 }
                 });*/

                batchExportConfig1 = function () {
                    FusionCharts.batchExport({
                        "charts": [{
                            "id": "bar_serveur",
                        }, {
                            "id": "toile_serveur",
                        }, {
                            "id": "bar_sauvegarde",
                        }, {
                            "id": "toile_sauvegarde",
                        }],
                        "exportFileName": "batchExport",
                        "exportFormats": "jpg",
                        "exportAtClientSide": "1"
                    })
                }
            </script>
        </div>
        <div class="tab-pane fade" id="stockage">
            <!--     <form id="myForm2">
                     <div class="btn-group btn-group-justified"
                          data-toggle="buttons">
                         <label class="btn btn-primary active" id="sort_graph">
                             <input type="radio" name="options" id="option1" value="mscombi2d">
                             <i class='fa fa-bar-chart'></i> Représentation en Barres
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option2" value="radar">
                             Représentation en Toiles
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option3" value="msline">
                             <i class='fa fa-line-chart'></i>
                             Représentation en Lignes
                         </label>
                     </div>
                 </form> !-->
            <div id="bar_stockage"></div>
            <br>
            <div id="toile_stockage"></div>
            <script type="text/javascript">
                $("#bar_stockage").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Stockages de données",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs (<?php if (!isset($_POST['trigger_convert'])) echo 'uo'; else echo $this->input->post('trigger_convert');?>)",
                            "exportEnabled": "1",
                            "theme": "ocean",
                            <?php if (!isset($_POST['trigger_convert'])) echo ''; else if ($_POST['trigger_convert'] == "k€") echo '"numberSuffix": "€",'; else if ($_POST['trigger_convert'] == "uo") echo '';?>
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($Stockagededonnées['label']['Stockagededonnées']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Stockagededonnées['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {

                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Stockagededonnées['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($Stockagededonnées['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                $("#toile_stockage").insertFusionCharts({
                    type: "radar",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des Stockages de données",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs en (<?php if (!isset($_POST['trigger_convert'])) echo 'uo'; else echo $this->input->post('trigger_convert');?>)",
                            "exportEnabled": "1",
                            "theme": "ocean",
                            <?php if (!isset($_POST['trigger_convert'])) echo ''; else if ($_POST['trigger_convert'] == "k€") echo '"numberSuffix": "€",'; else if ($_POST['trigger_convert'] == "uo") echo '';?>
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($Stockagededonnées['label']['Stockagededonnées']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Stockagededonnées['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {

                                        if ($input_range[0] == $input_range[1])
                                            echo(" de " . $input_range[0]);
                                        else
                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data": <?php
                                echo json_encode($Stockagededonnées['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($Stockagededonnées['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
            </script>
        </div>

        <div class="tab-pane fade" id="sauvegarde">
            <!--     <form id="myForm2">
                     <div class="btn-group btn-group-justified"
                          data-toggle="buttons">
                         <label class="btn btn-primary active" id="sort_graph">
                             <input type="radio" name="options" id="option1" value="mscombi2d">
                             <i class='fa fa-bar-chart'></i> Représentation en Barres
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option2" value="radar">
                             Représentation en Toiles
                         </label>
                         <label class="btn btn-primary">
                             <input type="radio" name="options" id="option3" value="msline">
                             <i class='fa fa-line-chart'></i>
                             Représentation en Lignes
                         </label>
                     </div>
                 </form> !-->
            <div id="bar_sauvegarde"></div>
            <button id="export_charts" onclick='batchExportConfig1();'>
                Export Charts
            </button>
            <br>
            <div id="toile_sauvegarde"></div>

            <script type="text/javascript">
                /*  $('#myForm2 input').on('change', function () {*/
                $("#bar_sauvegarde").insertFusionCharts({
                    type: "mscombi2d",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des sauvegardes",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean",
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($Sauvegarde['label']['Sauvegarde']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Sauvegarde['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {


                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Sauvegarde['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($Sauvegarde['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                $("#toile_sauvegarde").insertFusionCharts({
                    type: "radar",
                    width: "900",
                    height: "600",
                    dataFormat: "json",
                    dataSource: {
                        "chart": {
                            "caption": "Comparaison des sauvegardes",
                            "xaxisname": "Type de données",
                            "yaxisname": "Valeurs",
                            "exportEnabled": "1",
                            "theme": "ocean"
                        },
                        "categories": [
                            {
                                "category":
                                <?php
                                echo json_encode($Sauvegarde['label']['Sauvegarde']);
                                ?>
                            }
                        ],
                        "dataset": [
                            {
                                "seriesname": "Prévisionel <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Sauvegarde['MOULIBEX']);
                                ?>
                            },
                            {
                                "seriesname": "Réalisé <?php
                                    $input_range = $this->input->post('range_1');
                                    $input_range = explode(";", $input_range);
                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {


                                        echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                    } else
                                        echo "";?>",
                                "data":
                                <?php
                                echo json_encode($Sauvegarde['Fisher']);
                                ?>
                            }
                            <?php if (array_key_exists('gpf', $HostingVirtualisé)) {
                            echo(',{ "seriesname": "Correctif GPF');
                            $input_range = $this->input->post('range_1');
                            $input_range = explode(";", $input_range);
                            if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                if ($input_range[0] == $input_range[1])
                                    echo(" de " . $input_range[0]);
                                else
                                    echo(" de " . $input_range[0] . " à " . $input_range[1]);
                            } else
                                echo "";

                            echo('",
                            "data":');
                            echo json_encode($Sauvegarde['gpf']);
                            echo("}");
                        }?>
                        ]
                    }
                });
                /*   $('#chart-container').updateFusionCharts({
                 'type': $('input[name="options"]:checked', '#myForm').val()
                 });*/
                /*     if ($('input[name="options"]:checked', '#myForm2').val() == "pie3d") {

                 }
                 });*/

                batchExportConfig1 = function () {
                    FusionCharts.batchExport({
                        "charts": [{
                            "id": "bar_serveur",
                        }, {
                            "id": "toile_serveur",
                        }, {
                            "id": "bar_sauvegarde",
                        }, {
                            "id": "toile_sauvegarde",
                        }],
                        "exportFileName": "batchExport",
                        "exportFormats": "jpg",
                        "exportAtClientSide": "1"
                    })
                }
            </script>
        </div>

        <!--
        <div class="tab-pane fade" id="toile_3">
            Ici le graphique des toiles
        </div>
        <div class="tab-pane fade" id="pie_3">
            Ici le graphique des camemberts
        </div> !-->
    </div>


</div>