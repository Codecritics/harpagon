<div class="tab-pane fade in active" id="famille_services">
    <div class="form-group">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#graphes" data-toggle="tab">
                        <i class='fa fa-table'></i> Graphes
                    </a>
                </li>
                <li><a href="#tab_2" data-toggle="tab">
                        <i class='fa fa-bar-chart'></i> Tableau
                    </a>
                </li>

            </ul>
            <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
            <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
            <div class="tab-content">

                <div class="tab-pane fade in active" id="tab_2">
                    <div class="box-body table-responsive no-padding">
                        <?php
                        $array_graph_realise = array();
                        if (array_key_exists('sumrealisé',$HostingVirtualisé)) {
                            array_push($array_graph_realise, $HostingVirtualisé['sumrealisé'][0]);
                            array_push($array_graph_realise, $HostingDédié['sumrealisé'][0]);
                            array_push($array_graph_realise, $Stockagededonnées['sumrealisé'][0]);
                            array_push($array_graph_realise, $Sauvegarde['sumrealisé'][0]);
                            array_push($array_graph_realise, $ApplicationHosting['sumrealisé'][0]);

                            $array_graph_previ = array();
                            array_push($array_graph_previ, $HostingVirtualisé['sumprévisionel'][0]);
                            array_push($array_graph_previ, $HostingDédié['sumprévisionel'][0]);
                            array_push($array_graph_previ, $Stockagededonnées['sumprévisionel'][0]);
                            array_push($array_graph_previ, $Sauvegarde['sumprévisionel'][0]);
                            array_push($array_graph_previ, $ApplicationHosting['sumprévisionel'][0]);
                        }

                        ?>
                        <table class="table  table-hover table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Hosting Virtualisé</th>
                                <th>Hosting Dédié</th>
                                <th>Stockage de données</th>
                                <th>Sauvegarde</th>
                                <th>Application Hosting</th>
                                <th>Type de Fichier</th>
                                <!--  <th>Location</th> -->
                                <!--  <th>Intégration/Qualification</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (array_key_exists('sumrealisé',$HostingVirtualisé)) {
                                echo '<tr>' .
                                    '<td >' . '' .$HostingVirtualisé['sumrealisé'][0]['value'].'' . '</td >' .
                                    '<td >' . $HostingDédié['sumrealisé'][0]['value'] . '</td >' .
                                    '<td >' . $Stockagededonnées['sumrealisé'][0]['value'] . '</td >' .
                                    '<td >' . $Sauvegarde['sumrealisé'][0]['value'] . '</td >' .
                                    '<td >' . $ApplicationHosting['sumrealisé'][0]['value'] . '</td >' .
                                    '<td>Réalisé</td >'
                                    . '</tr>';
                                echo '<tr>' .
                                    '<td >' . $HostingVirtualisé['sumprévisionel'][0]['value'] . '</td >' .
                                    '<td >' . $HostingDédié['sumprévisionel'][0]['value'] . '</td >' .
                                    '<td >' . $Stockagededonnées['sumprévisionel'][0]['value'] . '</td >' .
                                    '<td >' . $Sauvegarde['sumprévisionel'][0]['value'] . '</td >' .
                                    '<td >' . $ApplicationHosting['sumprévisionel'][0]['value'] . '</td >' .
                                    '<td>Prévisionnel</td >'
                                    . '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="toile_2">
                    <p> Ici Le Graphique en toile </p>
                </div>
                <div class="tab-pane fade" id="graphes">
                    <div>
                        <br>
                        <div id="chart-contain">
                            <div id="chart-container"></div>

                            <script type="text/javascript">

                                $(".select2").select2();

                                $("#chart-container").insertFusionCharts({
                                    type: "radar",
                                    width: "900",
                                    height: "600",
                                    dataFormat: "json",
                                    dataSource: {
                                        "chart": {
                                            "caption": "Comparaison par familles de services",
                                            "xaxisname": "Type de données",
                                            "yaxisname": "Valeurs",
                                            "exportEnabled": "1",
                                            "exportFileName": "Comparaison par familles de services",
                                            "theme": "ocean",
                                        },
                                        "categories": [
                                            {
                                                "category": [
                                                    {
                                                        "label": " Hosting virtualisé"
                                                    },
                                                    {
                                                        "label": "Hosting Dédié"
                                                    },
                                                    {
                                                        "label": "Stockage de données "
                                                    },
                                                    {
                                                        "label": "Sauvegarde"
                                                    },
                                                    {
                                                        "label": "Application Hosting"
                                                    }
                                                ]
                                            }
                                        ],
                                        "dataset": [
                                            {
                                                "seriesname": "Prévisionel <?php
                                                    $input_range = $this->input->post('range_1');
                                                    $input_range = explode(";", $input_range);
                                                    $app_input = $this->input->post('application');
                                                    echo " " . $app_input . " ";
                                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {
                                                        if ($input_range[0] == $input_range[1])
                                                            echo(" de " . $input_range[0]);
                                                        else
                                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                                    } else
                                                        echo "";?>",
                                                "data":
                                                <?php
                                                echo json_encode($array_graph_previ);
                                                ?>
                                            },
                                            {
                                                "seriesname": "Réalisé <?php
                                                    $input_range = $this->input->post('range_1');
                                                    $input_range = explode(";", $input_range);
                                                    $app_input = $this->input->post('application');
                                                    echo " " . $app_input . " ";
                                                    if (array_key_exists(0, $input_range) && array_key_exists(1, $input_range)) {

                                                        if ($input_range[0] == $input_range[1])
                                                            echo(" de " . $input_range[0]);
                                                        else
                                                            echo(" de " . $input_range[0] . " à " . $input_range[1]);
                                                    } else
                                                        echo "";?>",
                                                "data":
                                                <?php
                                                echo json_encode($array_graph_realise);
                                                ?>
                                            }
                                        ]
                                    }
                                });

                                $('#myTab a').click(function (e) {
                                    e.preventDefault();
                                    $(this).tab('show');
                                });

                                // store the currently selected tab in the hash value
                                $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
                                    var id = $(e.target).attr("href").substr(1);
                                    window.location.hash = id;
                                });

                                // on load of the page: switch to the currently selected tab
                                var hash = window.location.hash;
                                $('#myTab a[href="' + hash + '"]').tab('show');

                                var from = 0,
                                    to = 0, table = [
                                        <?php
                                        for ($i = 0; $i < count($date); $i++) {
                                            //  if ($i == 0)
                                            //     echo "<option selected value='null' >Choisir une Date</option>";
                                            foreach ($date[$i] as $row) {
                                                if ($i == 0)
                                                    echo("'$row'");
                                                else
                                                    echo("" . ",'$row'");
                                            }
                                        }
                                        ?>
                                    ];

                                var saveResult = function (data) {
                                    from = data.from;
                                    to = data.to;
                                };

                                var writeResult = function () {
                                    var result = "from: " + from + ", to: " + to;
                                    $result.html(result);
                                };

                                $("#range_1").ionRangeSlider({
                                    type: "double",
                                    grid: true,

                                    values: [
                                        <?php
                                        for ($i = 0; $i < count($date); $i++) {
                                            //  if ($i == 0)
                                            //     echo "<option selected value='null' >Choisir une Date</option>";
                                            foreach ($date[$i] as $row) {
                                                if ($i == 0)
                                                    echo("'" . date("F", strtotime($row)) . "'");
                                                //echo ("'".$row."'");
                                                else
                                                    //echo (",'".$row."'");
                                                    echo("," . "'" . date("F", strtotime($row)) . "'");
                                            }
                                        }
                                        ?>

                                    ], onFinish: function (data) {
                                        saveResult(data);
                                        console.log(from);
                                        console.log(table[from]);
                                    }

                                });
                            </script>
                        </div>
                        <div id="contain">

                        </div>
                    </div>
                </div>
                <!--
                                                        <div class="tab-pane" id="toile_2">
                                                            <p> Ici Le Graphique en toile </p>
                                                        </div>
                                                        <div class="tab-pane" id="pie_2">
                                                            <p> Ici Le Graphique en Camembert </p>
                                                        </div> !-->
            </div>
        </div>


    </div>
</div>