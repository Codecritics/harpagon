<div class="tab-pane" id="total">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#courbe" data-toggle="tab">
                    <i class='fa fa-table'></i>
                    Courbes
                </a>
            </li>
            <li>
                <a href="#tab_1" data-toggle="tab">
                    <i class='fa fa-bar-chart'></i>
                    Tableau
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade" id="tab_1">
            <label for="tableau_service">Le tableau de chaque services</label>
            <?php
            echo "<div class='table-responsive'>";
            //  echo"<table class ='table table-striped table-hover'>";
            echo $main_value;
            //  echo"</table>";
            echo "</div>";
            ?>
            <br>
            <label for="tableau_service1">Le tableau des sommes des UOs Fisher</label>
            <?php
            echo "<div id='tableau_service1' class='table-responsive'>";
            //  echo"<table class ='table table-striped table-hover'>";
            echo $test_data_table_generated_gpf;
            //  echo"</table>";
            echo "</div>";
            ?>
            <br>
            <label for="tableau_service2">Le tableau des sommes des UOs Moulibex</label>
            <?php
            echo "<div id='tableau_service2' class='table-responsive'>";
            //  echo"<table class ='table table-striped table-hover'>";
            echo $test_data_table_generated_moulibex;
            //  echo"</table>";
            echo "</div>";
            ?>
        </div>
        <div class="tab-pane fade" id="courbe">
            <div id="courbe-container">
                <script type="text/javascript">
                    $("#courbe-container").insertFusionCharts({
                        type: "zoomline",
                        width: "900",
                        height: "600",
                        dataFormat: "json",
                        dataSource: {
                            "chart": {
                                "caption": "Comparaison des Services",
                                "xaxisname": "Mois",
                                "yaxisname": "Valeurs",
                                "theme": "ocean",
                            },
                            "categories": [
                                {
                                    "category": [
                                        <?php
                                        foreach ($graph_mois_gpf as $value) {
                                            echo '{"label":"' . $value . '"},';
                                        }
                                        ?>
                                    ]
                                }
                            ],
                            "dataset": [
                                {
                                    "seriesname": "  <?php
                                        $input_app = $this->input->post('application');
                                        if ($input_app == " null")
                                            echo(" Aucune Application sélectionnée " . $input_range[0]);
                                        else
                                            echo(" Total Services Réalisés de " . $input_app);
                                        ?>",
                                    "data":
                                    <?php
                                    if (isset($graph_value_gpf)) {
                                        echo '[';
                                        $top =0;
                                        for ($i = 0; $i < count($graph_value_gpf); $i++) {
                                            $top += $graph_value_gpf[$i];
                                            if ($i >= 1) {
                                                echo '{"value":"' . $top . '"},';
                                            }
                                            else
                                                echo '{"value":"' . $graph_value_gpf[0] . '"},';
                                        }
                                        echo ']';
                                    } else
                                        echo(' [{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0 "},{"value":"0"},{"value":"0"}]');?>
                                },
                                {
                                    "seriesname": "<?php
                                        $input_app = $this->input->post('application');
                                        if ($input_app == " null")
                                            echo(" Aucune Application sélectionnée " . $input_range[0]);
                                        else
                                            echo(" Total Services Prévisionnels BA de " . $input_app);
                                        ?>",
                                    "data": <?php
                                    if (isset($graph_value_moulibex_ba)) {
                                        echo '[';
                                        $top = 0;
                                        for ($i = 0; $i < count($graph_value_moulibex_ba); $i++) {
                                            $top += $graph_value_moulibex_ba[$i];
                                            if ($i >= 1) {
                                                echo '{"value":"' . $top . '"},';
                                            } else
                                                echo '{"value":"' . $graph_value_moulibex_ba[0] . '"},';
                                        }
                                        echo ']';
                                    }
                                    else
                                        echo(' [{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0 "},{"value":"0"},{"value":"0"}]');?>
                                },
                                {
                                    "seriesname": "  <?php
                                        $input_app = $this->input->post('application');
                                        if ($input_app == " null")
                                            echo(" Aucune Application sélectionnée " . $input_range[0]);
                                        else
                                            echo(" Total Services Prévisionnels R1 " . $input_app);
                                        ?>",
                                    "data":
                                    <?php
                                    if (isset($graph_value_moulibex_r1)) {
                                        echo '[';
                                        $top = 0;
                                        for ($i = 0; $i < count($graph_value_moulibex_r1); $i++) {
                                            $top += $graph_value_moulibex_r1[$i];
                                            if ($i >= 1) {
                                                echo '{"value":"' . $top . '"},';
                                            } else
                                                echo '{"value":"' . $graph_value_moulibex_r1[0] . '"},';
                                        }
                                        echo ']';
                                    }
                                    else
                                        echo(' [{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0 "},{"value":"0"},{"value":"0"}]');?>
                                },
                                {
                                    "seriesname": "  <?php
                                        $input_app = $this->input->post('application');
                                        if ($input_app == " null")
                                            echo(" Aucune Application sélectionnée " . $input_range[0]);
                                        else
                                            echo(" Total Services Prévisionnels R2 " . $input_app);
                                        ?>",
                                    "data":
                                    <?php
                                    if (isset($graph_value_moulibex_r2)) {
                                        echo '[';
                                        $top = 0;
                                        for ($i = 0; $i < count($graph_value_moulibex_r2); $i++) {
                                            $top += $graph_value_moulibex_r2[$i];
                                            if ($i >= 1) {
                                                echo '{"value":"' . $top . '"},';
                                            } else
                                                echo '{"value":"' . $graph_value_moulibex_r2[0] . '"},';
                                        }
                                        echo ']';
                                    }
                                    else
                                        echo(' [{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0"},{"value":"0 "},{"value":"0"},{"value":"0"}]');?>
                                }
                            ]
                        }
                    });

                </script>
            </div>
        </div>
    </div>

</div>