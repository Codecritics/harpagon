<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Comparaison des réalisés et prévisionnels</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div>
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs" id="myTab">
                    <li>
                        <a href="#total" data-toggle="tab">
                            Total
                        </a>
                    </li>
                    <li class="active">
                        <a href="#famille_services" data-toggle="tab">
                            Familles services
                        </a>
                    </li>
                    <li>
                        <a href="#details_services" data-toggle="tab">
                            Détails services
                        </a>
                    </li>
                    <li>
                        <a href="#cloture_sr" data-toggle="tab">
                            Clôture SR
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <?php $this->load->view('table/compare/layout_total'); ?>
                    <?php $this->load->view('table/compare/layout_familles_services'); ?>
                    <?php $this->load->view('table/compare/layout_details_services'); ?>
                    <?php $this->load->view('table/compare/layout_cloture_sr'); ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>