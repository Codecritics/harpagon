<div class="container">
    <div class="row">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Liste Déroulante 1</h3>
                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <span class="label label-primary">Applications</span>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">SIMM</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-striped table-hover " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Application</th>
                                <th>Nom du pilote</th>
                                <th>UO Hébergement</th>
                                <th>UO Stockage prenium et Option Image</th>
                                <th>Uo stockage classic</th>
                                <th>UO Serveur puissance</th>
                                <th>Type de Données</th>
                                <th>Date dernier Inventaire</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data['simm'] as $row) {
                                echo '<tr>' .
                                    '<td>' . $row->app . '</td >' .
                                    '<td >' . $row->pname . '</td >' .
                                    '<td >' . $row->{'SUM(uoh)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uospo)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosc)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosp)'} . '</td >' .
                                    '<td >' . $row->type . '</td >' .
                                    '<td >' . $row->ddi . '</td >'
                                    . '</tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">AEL</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-striped table-hover " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Application</th>
                                <th>Nom du pilote</th>
                                <th>UO Hébergement</th>
                                <th>UO Stockage prenium et Option Image</th>
                                <th>Uo stockage classic</th>
                                <th>UO Serveur puissance</th>
                                <th>Type de Données</th>
                                <th>Date dernier Inventaire</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data['ael'] as $row) {
                                echo '<tr>' .
                                    '<td>' . $row->app . '</td >' .
                                    '<td >' . $row->pname . '</td >' .
                                    '<td >' . $row->{'SUM(uoh)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uospo)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosc)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosp)'} . '</td >' .
                                    '<td >' . $row->type . '</td >' .
                                    '<td >' . $row->ddi . '</td >'
                                    . '</tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="row">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Liste Déroulante 2</h3>
                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <span class="label label-primary">Prévisionels</span>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Prévisionnel R1</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-striped table-hover " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Application</th>
                                <th>Nom du pilote</th>
                                <th>UO Hébergement</th>
                                <th>UO Stockage prenium et Option Image</th>
                                <th>Uo stockage classic</th>
                                <th>UO Serveur puissance</th>
                                <th>Type de Données</th>
                                <th>Date dernier Inventaire</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data['r1'] as $row) {
                                echo '<tr>' .
                                    '<td>' . $row->app . '</td >' .
                                    '<td >' . $row->pname . '</td >' .
                                    '<td >' . $row->{'SUM(uoh)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uospo)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosc)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosp)'} . '</td >' .
                                    '<td >' . $row->type . '</td >' .
                                    '<td >' . $row->ddi . '</td >'
                                    . '</tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Prévisionnel R2</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-striped table-hover " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Application</th>
                                <th>Nom du pilote</th>
                                <th>UO Hébergement</th>
                                <th>UO Stockage prenium et Option Image</th>
                                <th>Uo stockage classic</th>
                                <th>UO Serveur puissance</th>
                                <th>Type de Données</th>
                                <th>Date dernier Inventaire</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data['r2'] as $row) {
                                echo '<tr>' .
                                    '<td>' . $row->app . '</td >' .
                                    '<td >' . $row->pname . '</td >' .
                                    '<td >' . $row->{'SUM(uoh)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uospo)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosc)'} . '</td >' .
                                    '<td >' . $row->{'SUM(uosp)'} . '</td >' .
                                    '<td >' . $row->type . '</td >' .
                                    '<td >' . $row->ddi . '</td >'
                                    . '</tr>';
                            } ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Prévisionnel R3</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>