
<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 04/07/2016
 * Time: 17:25
 */?>
<?php
foreach($output->css_files as $file)
{?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file;
}?>" />

<?php foreach($output->js_files as $file): ?>

    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<div>
    <?php echo $output->output; ?>
</div>
