<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 01/12/2016
 * Time: 15:48
 */?>
<script type="text/javascript" src="../assets/plugins/fusioncharts-suite-xt/js/fusioncharts.js"></script>
<script type="text/javascript" src="../assets/plugins/angular/angular.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-chart.js"></script>
<script type="text/javascript" src="../assets/plugins/angular-chart.js/angular-fusioncharts.min.js"></script>
<script type="text/javascript"
        src="../assets/plugins/fusioncharts-suite-xt/js/fusioncharts-jquery-plugin.min.js"></script>

<script type="text/javascript" src="http://momentjs.com/downloads/moment.min.js"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="../assets/plugins/ionslider/ion.rangeSlider.min.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
<script src="../assets/plugins/select2/select2.full.min.js"></script>
<!-- Ion Slider -->
<link rel="stylesheet" href="../assets/plugins/ionslider/ion.rangeSlider.css">
<!-- ion slider Nice -->
<link rel="stylesheet" href="../assets/plugins/ionslider/ion.rangeSlider.skinNice.css">
<script type="text/javascript"
        src="../assets/plugins/fusioncharts-suite-xt/js/themes/fusioncharts.theme.ocean.js"></script>

<link href="../assets/plugins/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
<script src="../assets/plugins/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
<div class="container-fluid">
    <?php $this->load->view('cormoran/cormoran_filters'); ?><!-- /.box -->

    <?php $this->load->view('cormoran/cormoran_compare'); ?>
</div>