<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Tableaux</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div>
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs" id="myTab">
                    <li>
                        <a href="#general" data-toggle="tab">
                            Général
                        </a>
                    </li>
                    <li>
                        <a href="#total" data-toggle="tab">
                            Total
                        </a>
                    </li>
                    <li class="active">
                        <a href="#delta" data-toggle="tab">
                            Delta
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <?php $this->load->view('cormoran/compare/layout_general'); ?>
                    <?php $this->load->view('cormoran/compare/layout_total'); ?>
                    <?php $this->load->view('cormoran/compare/layout_delta'); ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>