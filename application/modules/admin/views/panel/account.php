<?php echo $form1->messages(); ?>

<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Informations du Compte</h3>
			</div>
			<div class="box-body">
				<?php echo $form1->open(); ?>
					<?php echo $form1->bs3_text('Nom', 'first_name', $user->first_name); ?>
					<?php echo $form1->bs3_text('Prénom', 'last_name', $user->last_name); ?>
					<?php echo $form1->bs3_submit('Mettre à jour'); ?>
				<?php echo $form1->close(); ?>
			</div>
		</div>
	</div>
	
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Modifier le mot de passe</h3>
			</div>
			<div class="box-body">
				<?php echo $form2->open(); ?>
					<?php echo $form2->bs3_password('Nouveau Mot de Passe', 'new_password'); ?>
					<?php echo $form2->bs3_password('Retaper à nouveau le mot de passe', 'retype_password'); ?>
					<?php echo $form2->bs3_submit(); ?>
				<?php echo $form2->close(); ?>
			</div>
		</div>
	</div>
	
</div>