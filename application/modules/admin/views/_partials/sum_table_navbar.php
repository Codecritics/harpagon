
<header class="main-header">
    <a href="" class="logo"><b><?php echo $site_name; ?></b></a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><? echo $user->first_name; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <p><?php echo $user->first_name; ?></p>
                            <?php if ($this->ion_auth->in_group('webmaster')) {
                                echo "<img src=\"../assets/dist/images/darkvador.png\" class=\"img-circle\" alt=\"User Image\">";
                            }
                            else if($this->ion_auth->in_group('staff'))
                            {
                                echo "<img src=\"../assets/dist/images/stt.png\" class=\"img-circle\" alt=\"User Image\">";
                            }
                            else if($this->ion_auth->in_group('admin'))
                            {
                                echo "<img src=\"../assets/dist/images/yoda.png\" class=\"img-circle\" alt=\"User Image\">";
                            }?>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="panel/account" class="btn btn-default btn-flat">Mon Compte</a>
                            </div>
                            <div class="pull-right">
                                <a href="panel/logout" class="btn btn-default btn-flat">Déconnexion</a>
                            </div>
                        </li>
                    </ul>
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
                </li>
            </ul>
        </div>
    </nav>
</header>