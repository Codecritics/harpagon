<?php

/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 02/12/2016
 * Time: 09:43
 */
class Cormoran_model extends CI_Model
{


    function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->load->database();
    }

    function read_table_parametrage($categorie)
    {
        if (!isset($categorie)) {
            $query = 'SELECT * FROM table_parametrage';
        } else {
            $query = 'SELECT * FROM table_parametrage WHERE categorie = "' . $categorie . '"';
        }
        $result = $this->db->query($query);
        return $result->result_array();
    }

    function read_code_prestation_table_parametrage($libelle_categorie)
    {
        $result = array();
        foreach ($libelle_categorie as $value) {
            $query = 'SELECT code_prestation FROM table_parametrage WHERE categorie = "' . $value . '"';
            $result_query = $this->db->query($query)->result_array();
            $result[$value] = array();
            for ($i = 0; $i < count($result_query); $i++) {
                array_push($result[$value], $result_query[$i]['code_prestation']);
            }
        }
        return $result;
    }

    function main_get_data($service, $date1, $otp, $app, $convert)
    {
        $string = "";
        if ($date1) {
            if (count($otp) == 0 && $app != null)
                $string = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND application LIKE " . "'$app'";

            if (count($otp) == 0 && $app == null)
                $string = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'";

            for ($i = 0; $i < count($otp); $i++) {
                if ($app != null) {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'" . "  AND application LIKE " . "'$app'";
                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                    }
                } else {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                    }
                }
            }
        }
        $query = "";
        $query_cormoran = "";
        $final_query = array();
        $label_result = array();
        $column_result = array();
        $array_query_cormoran = array();
        $array_query_total = array();
        $value1 = array();
        $query_cormoran = "";
        $sub_query_full = "";
        $service = array_filter($service);
        foreach ($service as $key => $value) {
            $diff_query = "";
            $libelle_fisher[$key] = array();
            $temp = $this->read_table_parametrage($key);
            $temp_fisher = array();
            $temp_moulibex = array();
            $temp_cp = array();
            $subquery = "";
            $subquery1="";
            $subquery_cormoran_fisher = "";
            $subquery_cormoran_gpf = "";
            $family_query = "";
            $sub_family_query = "";
            foreach ($temp as $ok) {
                array_push($temp_fisher, $ok['libelle_fisher']);
                array_push($temp_moulibex, $ok['libelle_moulibex']);
                array_push($temp_cp, $ok['code_prestation']);
            }
            $temp_cp = array_unique($temp_cp);
            $value1 = array_unique($value);
            for ($i = 0; $i < count($value1); $i++) {
                if (array_key_exists($i, $temp_cp) && $value1[$i] == $temp_cp[$i]) {
                    $var_len = strlen($temp_fisher[$i]);
                    if ($temp_fisher[$i] == "N/A" || $temp_fisher[$i] == "") {
                        $temp_libelle = str_replace(' ', '_', $temp_moulibex[$i]);
                       // $temp_libelle = str_replace('"', '', $temp_libelle);
                      //  $temp_libelle = str_replace('+', 'et', $temp_libelle);
                        if (strlen($temp_libelle) > 64) {
                            $temp_libelle = substr($temp_libelle, 0, 64);
                        }
                    } else if ($temp_fisher[$i] !== "N/A" || $temp_fisher[$i] !== "") {
                        $temp_libelle = str_replace(' ', '_', $temp_fisher[$i]);
                       // $temp_libelle = str_replace('"', '', $temp_libelle);
                      //  $temp_libelle = str_replace('+', 'et', $temp_libelle);
                        if (strlen($temp_libelle) > 64) {
                            $temp_libelle = substr($temp_libelle, 0, 64);
                        }
                    }
                    //array_push($libelle_fisher[$key], $temp_libelle);
                    $libelle_fisher[$key][$i] = $temp_libelle;
                }
                if ($key == "Stockage de données" || $key == "Sauvegarde") {
                    $subquery .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value1[$i] . '`/' . $convert . ') ELSE `' . $value1[$i] . '` END) AS `' . $value1[$i] . '`,';
                    $subquery1 .= 'CASE WHEN type_fichier = "Fisher" THEN (`' . $value1[$i] . '`/' . $convert . ') ELSE `' . $value1[$i] . '` END AS `' . $value1[$i] . '`,';
                    $subquery_cormoran_fisher .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value1[$i] . '`/' . $convert . ') ELSE `' . $value1[$i] . '` END) AS `' . $libelle_fisher[$key][$i] . '`,';
                    $subquery_cormoran_gpf .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value1[$i] . '`/' . $convert . ') ELSE `' . $value1[$i] . '` END) AS `' . $libelle_fisher[$key][$i] . '`,';
                    //$sub_family_query .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value1[$i] . '`/' . $convert . ') ELSE `' . $value1[$i] . '` END) + ';
                } else {
                    $subquery .= 'SUM(`' . $value1[$i] . '`)' . ' AS `' . $libelle_fisher[$key][$i] . '`,';
                    $subquery1 .= '`' . $value1[$i] . '`' . ' AS `' . $libelle_fisher[$key][$i] . '`,';
                    $subquery_cormoran_fisher .= 'SUM(`' . $value1[$i] . '`) AS `' . $libelle_fisher[$key][$i] . '`,';
                    $subquery_cormoran_gpf .= 'SUM(`' . $value1[$i] . '`) AS `' . $libelle_fisher[$key][$i] . '`,';
                    //   $subquery_cormoran_gpf .= 'SUM(`' . $value[$i] . '`) AS ' . $value[$i] . '_GPF,';
                    $sub_family_query .= 'SUM(`' . $value1[$i] . '`) + ';
                }
            }
            $sub_query_full .= $subquery1;
            $subquery = substr($subquery, 0, -1);
            $subquery_cormoran_gpf = substr($subquery_cormoran_gpf, 0, -1);
            $subquery_cormoran_fisher = substr($subquery_cormoran_fisher, 0, -1);
            $sub_family_query = substr($sub_family_query, 0, -2);
            $query = 'SELECT type_fichier,' . $subquery . ' FROM ( ' . $string . ')' . '`' . $key . '` WHERE type_fichier != "MOULIBEX" AND type_fichier != "sr"GROUP BY type_fichier ASC';
            $query_cormoran_fisher = '(SELECT type_fichier,' . $subquery_cormoran_fisher . ' FROM ( ' . $string . ' AND type_fichier = "Fisher" )' . ' AS `' . $key . '`) AS `' . $key . '_fisher`';
            $query_cormoran_gpf = '(SELECT type_fichier,' . $subquery_cormoran_gpf . ' FROM ( ' . $string . ' AND type_fichier = "gpf")' . ' AS `' . $key . '`) AS `' . $key . '_gpf`';
            for ($i = 0; $i < count($value1); $i++) {
                $diff_query .= "`" . $key . "_fisher`" . ".`" . $libelle_fisher[$key][$i] . "`-`" . $key . "_gpf`" . ".`" . $libelle_fisher[$key][$i] . "` AS `" . $libelle_fisher[$key][$i] . "`,";
            }
            $diff_query = substr($diff_query, 0, -1);
            $query_cormoran = 'SELECT ' . $diff_query . ' FROM ' . $query_cormoran_fisher . ' INNER JOIN' . $query_cormoran_gpf . '';
            // echo $query;

            $family_query = 'SELECT ' . $sub_family_query . " AS \"" . $key . "\"" . ' FROM ( ' . $string . ')' . '`' . $key . '` WHERE type_fichier != "MOULIBEX" GROUP BY type_fichier ASC';
            // var_dump($query);
            $final_query[$key] = $query;
            $final_family_query[$key] = $family_query;
            //$label = ucwords($key);
            //var_dump ($label);
            $label = str_replace(' ', '', $key);
            $label = str_replace('/', '', $label);
          // $label1 = strtolower($label);

            if ($this->db->table_exists($label)) {
                $this->db->query('DROP TABLE ' . $label);
            }
            array_push($label_result, $label);
            /*
             * Pour l'instant on a un problème avec le stockage de données.
             * Il faudra gerer la multiplicité des codes de prestations dans la table de paramétrage.
             */

            if ($key !== "Stockage de données") {
                $final_query_test = 'CREATE TABLE  IF NOT EXISTS ' . $label . ' (id INT AUTO_INCREMENT PRIMARY KEY) AS ' . $query;
                $this->db->query($final_query_test);
                $result_query_total = $this->db->query($query);
                $result_query_cormoran = $this->db->query($query_cormoran);
                // $solution_test = $this->db->get($label)->result_array();
                array_push($array_query_cormoran, $result_query_cormoran);
                array_push($array_query_total, $result_query_total);
                //  var_dump($this->db->query($query_cormoran)->result_array());
                $column_result[$label] = $this->db->list_fields($label);
                // $this->db->query('SELECT * from ' . $label .' WHERE type_fichier = "MOULIBEX"')->result_array();
                // var_dump($solution_test);
            }
        }
        $sub_query_full = substr($sub_query_full, 0, -1);
        $general_query = 'SELECT type_fichier, otp,nom_serveur, ' . $sub_query_full . ' FROM ( ' . $string . ')' . '` general ` WHERE type_fichier != "MOULIBEX" ';
        if ($this->db->table_exists('general_view_cormoran')) {
            $this->db->query('DROP TABLE IF EXISTS general_view_cormoran');
        }
        $general_table ='CREATE TABLE  IF NOT EXISTS general_view_cormoran (id INT AUTO_INCREMENT PRIMARY KEY) AS ' . $general_query;
        $this->db->query($general_table);
        return [$label_result, $column_result, $array_query_cormoran, $array_query_total];
    }

} ?>
