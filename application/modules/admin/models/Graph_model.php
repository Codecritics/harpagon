<?php

/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 04/05/2016
 * Time: 17:17
 */
class Graph_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

    }

    function get_data()
    {
        $test_query1 = $this->db->query('SELECT sname FROM table2')->result();
        $test_query2 = $this->db->query('SELECT otp FROM table2')->result();
        //  $this->db->select('sname, otp, uoh, uospo');
        //  $this->db->from('table2');
        //  $query = $this->db->get();
        $result = array($test_query1, $test_query2);
        return $result;
        //  return $query->result();
    }

    function get_column()
    {
        $fields = $this->db->list_fields('table2');
        return $fields;
    }

    function delete()
    {
        $this->db->truncate('suivi_stockage');
        $this->db->truncate('uploaded_file');
        $this->db->truncate('uploaded_file1');
    }

    function delete_db_converse()
    {
        $this->dbforge->drop_table('suivi_convert', TRUE);
    }

    /*
        function sum_app($app)
        {
            $query = $this->db->select('app, pname, SUM(uoh), SUM(uospo), SUM(uosc), SUM(uosp), type, ddi')->from('table2')->where('app', $app)->group_by('app')->get();
            return $query->result();
        }
    */

    function read_table_parametrage($categorie)
    {
        if (!isset($categorie)) {
            $query = 'SELECT * FROM table_parametrage';
        } else {
            $query = 'SELECT * FROM table_parametrage WHERE categorie = "' . $categorie . '"';
        }
        $result = $this->db->query($query);
        return $result->result_array();
    }

    function conversion()
    {
        $query2 = $this->db->query('CREATE TABLE IF NOT EXISTS suivi_convert LIKE suivi_stockage');
        $query = $this->db->query('INSERT suivi_convert SELECT * FROM suivi_stockage');
        redirect('admin/home');
    }

    function get_data_for_compare($code_prestation, $convert, $from, $to, $input_otp, $input_app)
    {

    }


    function read_table_conversion()
    {
        $query = $this->db->query('SELECT libelle_abrevation, cout_uo FROM table_conversion');
        $result = $query->result_array();
        return $result;
    }


    function main_get_data($service, $date1, $date2, $otp, $app, $convert)
    {
        $string = "";
        $string_for_total_query="";
        if ($date1 == $date2) {
            if (count($otp) == 0 && $app != null) {
                $string = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND application LIKE " . "'$app'";
                $string_for_total_query = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND application LIKE " . "'$app'";
            }
            if (count($otp) == 0 && $app == null) {
                $string = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'";
                $string_for_total_query = "SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'";
            }

            for ($i = 0; $i < count($otp); $i++) {
                if ($app != null) {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'" . "  AND application LIKE " . "'$app'";
                        $string_for_total_query .= " SELECT *  FROM suivi_stockage WHERE" . "  otp LIKE " . "'$otp[$i]'" . "  AND application LIKE " . "'$app'";

                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " UNION ALL SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                    }
                } else {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE MONTHNAME(mois_concerne) = '" . $date1 . "'" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " UNION ALL SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                    }
                }
            }
        } else if ($date1 !== $date2) {
            if (count($otp) == 0 && $app != null) {
                $string = "SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))" . "  AND application LIKE " . "'$app'";
                $string_for_total_query = "SELECT *  FROM suivi_stockage WHERE application LIKE " . "'$app'";
            }
            if (count($otp) == 0 && $app == null) {
                $string = "SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))";
                $string_for_total_query = "SELECT *  FROM suivi_stockage ";
            }
            for ($i = 0; $i < count($otp); $i++) {
                if ($app != null) {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))" . "  AND otp LIKE " . "'$otp[0]'" . "  AND application LIKE " . "'$app'";
                        $string_for_total_query .= " SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[0]'" . "  AND application LIKE " . "'$app'";
                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " UNION ALL SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                    }
                } else {
                    if ($i == 0) {
                        $string .= " SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                        continue;
                    } else {
                        $string .= " UNION ALL SELECT *  FROM suivi_stockage WHERE (mois_concerne >= STR_TO_DATE('" . $date1 . " 1','%M %d') AND mois_concerne <= STR_TO_DATE('" . $date2 . " 1','%M %d'))" . "  AND otp LIKE " . "'$otp[$i]'";
                        $string_for_total_query .= " UNION ALL SELECT *  FROM suivi_stockage WHERE otp LIKE " . "'$otp[$i]'";
                    }
                }
            }
        }
        $query = "";
        $sub_query_full = "";
        $sub_query_full_total = "";
        $final_query = array();
        $service = array_filter($service);
        foreach ($service as $key => $value) {
            $libelle_fisher[$key] = array();
            $libelle_fisher1[$key] = array();
            $temp_count = $this->read_table_parametrage($key);
            $temp_fisher = array();
            $temp_moulibex = array();
            $temp_cp = array();
            $subquery = "";
            $subquery1 = "";
            $family_query = "";
            $sub_family_query = "";
            $sub_family_query1 = "";
            foreach ($temp_count as $ok) {
                array_push($temp_fisher, $ok['libelle_fisher']);
                array_push($temp_moulibex, $ok['libelle_moulibex']);
                array_push($temp_cp, $ok['code_prestation']);
            }
            //    var_dump($temp_cp);
            //    var_dump($value);
            $value = array_unique($value);
            for ($i = 0; $i < count($value); $i++) {
                if (array_key_exists($i, $temp_cp) && in_array($value[$i], $temp_cp)) {
                    $var_len = strlen($temp_fisher[$i]);
                    if ($temp_fisher[$i] == "N/A" || $temp_fisher[$i] == "") {
                        $temp_libelle = str_replace(' ', '_', $temp_moulibex[$i]);
                        // $temp_libelle = str_replace('"', '', $temp_libelle);
                        //  $temp_libelle = str_replace('+', 'et', $temp_libelle);
                        if (strlen($temp_libelle) > 64) {
                            $temp_libelle = substr($temp_libelle, 0, 64);
                        }
                    } else if ($temp_fisher[$i] !== "N/A" || $temp_fisher[$i] !== "") {
                        $temp_libelle = str_replace(' ', '_', $temp_fisher[$i]);
                        // $temp_libelle = str_replace('"', '', $temp_libelle);
                        //  $temp_libelle = str_replace('+', 'et', $temp_libelle);
                        if (strlen($temp_libelle) > 64) {
                            $temp_libelle = substr($temp_libelle, 0, 64);
                        }
                    }
                    //array_push($libelle_fisher[$key], $temp_libelle);
                    $libelle_fisher1[$key][$i] = $temp_libelle;
                }
                //    var_dump($libelle_fisher1);
                if ($key == "Stockage de données" || $key == "Sauvegarde") {
                    $subquery .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value[$i] . '`/' . $convert . ') ELSE `' . $value[$i] . '` END) AS "SUM(`' . $value[$i] . '`)",';
                    $sub_family_query .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value[$i] . '`/' . $convert . ') ELSE `' . $value[$i] . '` END) + ';
                    $sub_family_query1 .= 'SUM(CASE WHEN type_fichier = "Fisher" THEN (`' . $value[$i] . '`/' . $convert . ') ELSE `' . $value[$i] . '` END) + ';

                } else {
                    $subquery .= 'SUM(`' . $value[$i] . '`),';
                    if (array_key_exists($i, $libelle_fisher1[$key]))
                        $subquery1 .= 'SUM(`' . $value[$i] . '`)' . ' AS `' . $libelle_fisher1[$key][$i] . '`,';
                    $sub_family_query .= 'SUM(`' . $value[$i] . '`) + ';
                    if (array_key_exists($i, $libelle_fisher1[$key]))
                        $sub_family_query1 .= 'SUM(`' . $value[$i] . '`) '  . ' + ';
                }
            }
            $sub_query_full .= $subquery1;
            $sub_query_full_total .=$sub_family_query1;
            $subquery = substr($subquery, 0, -1);
            $sub_family_query = substr($sub_family_query, 0, -2);
            $query = 'SELECT type_fichier,' . $subquery . ' FROM ( ' . $string . ')' . '`' . $key . '` GROUP BY type_fichier ASC';
            $family_query = 'SELECT ' . $sub_family_query . " AS \"" . $key . "\"" . ' FROM ( ' . $string . ')' . '`' . $key . '` GROUP BY type_fichier ASC';
            // var_dump($query);
            $final_query[$key] = $query;
            $final_family_query[$key] = $family_query;
        }
        // var_dump($final_family_query);
        $final_sql = array();
        $final_sql_table = array();
        $final_family_sql = array();
        foreach ($final_query as $key => $value) {
            $temp_result = $this->db->query($value)->result_array();
            $temp_result_table = $this->db->query($value);
            $final_sql[$key] = $temp_result;
            $final_sql_table[$key] = $temp_result_table;
        }
        foreach ($final_family_query as $key => $value) {
            $temp_family_result = $this->db->query($value)->result_array();
            $final_family_sql[$key] = $temp_family_result;
        }
        //  var_dump($final_family_sql);

        foreach ($final_sql as $label => $value) {
            if (!empty($value)) {
                foreach ($value[0] as $label1 => $value1) {
                    if ($value[0][$label1] == '0' && $value[0][$label1] == $value[1][$label1]) {
                        unset($final_sql[$label][0][$label1], $final_sql[$label][1][$label1]);
                    }
                    if (($value[0][$label1] == '0' && $value[1][$label1] == '0') || ($value[0][$label1] == '0' && $value[1][$label1] == '0')) {
                        unset($final_sql[$label][0][$label1], $final_sql[$label][1][$label1]);
                    }
                }
            }
        }

        $type_fichier = ['Fisher', 'gpf', 'MOULIBEX', 'sr'];
        foreach ($final_sql as $label => $value) {
            for ($i = 0; $i < count($value); $i++) {
                $temp_sql[$label][$value[$i]['type_fichier']] = $value[$i];
                unset($temp_sql[$label][$value[$i]['type_fichier']]['type_fichier']);
            }
        }
        $final_sql = $temp_sql;
        foreach ($final_sql as $key => $value) {
            array_filter($libelle_fisher[$key]);
            $temp = $this->read_table_parametrage($key);
            if (!empty($value)) {
                foreach ($value['Fisher'] as $key1 => $value1) {
                    $temp_str = substr($key1, 5, -2);
                    for ($i = 0; $i < count($temp); $i++) {
                        if ($temp_str == $temp[$i]['code_prestation']) {
                            if ($temp[$i]['libelle_fisher'] == "N/A") {
                                array_push($libelle_fisher[$key], $temp[$i]['libelle_moulibex']);
                            } else
                                array_push($libelle_fisher[$key], $temp[$i]['libelle_fisher']);
                        }
                    }
                }
            }
        }

        foreach ($libelle_fisher as $key => $value) {
            $libelle_fisher[$key] = array_filter($value);
        }
        //   var_dump($libelle_fisher);

        foreach ($libelle_fisher as $key => $value) {
            $copy_temp[$key] = array();
            for ($i = 0; $i < count($value); $i++) {

                for ($j = 0; $j < count($copy_temp[$key]); $j++) {
                    if (array_key_exists($i, $value) && $value[$i] == $copy_temp[$key][$j])
                        $i++;
                }
                if (array_key_exists($i, $value))
                    array_push($copy_temp[$key], $value[$i]);
            }
        }
        $sub_query_full = substr($sub_query_full, 0, -1);
        $sub_query_full_total = substr($sub_query_full_total, 0, -3);

        $general_query = 'SELECT type_fichier,  ' . $sub_query_full . ' FROM ( ' . $string . ')' . ' AS `general1` WHERE type_fichier != "MOULIBEX" AND type_fichier != "gpf" AND type_fichier != "sr"';
        $general_query_total_fisher = 'SELECT MONTHNAME(mois_concerne) AS Mois, ' . $sub_query_full_total . 'AS Somme FROM ( ' . $string_for_total_query . ')' . ' AS `general1` WHERE type_fichier != "MOULIBEX" AND type_fichier != "gpf" AND type_fichier != "sr" Group BY mois_concerne';
        $general_query_total_moulibex_ba = 'SELECT MONTHNAME(mois_concerne) AS Mois, ' . $sub_query_full_total . 'AS Somme FROM ( ' . $string_for_total_query . ')' . ' AS `general1` WHERE type_fichier != "Fisher" AND type_fichier != "gpf" AND type_fichier != "sr" AND type_previsionnel = "BA" Group BY mois_concerne';
        $general_query_total_moulibex_r1 = 'SELECT MONTHNAME(mois_concerne) AS Mois, ' . $sub_query_full_total . 'AS Somme FROM ( ' . $string_for_total_query . ')' . ' AS `general1` WHERE type_fichier != "Fisher" AND type_fichier != "gpf" AND type_fichier != "sr" AND type_previsionnel = "R1" Group BY mois_concerne';
        $general_query_total_moulibex_r2 = 'SELECT MONTHNAME(mois_concerne) AS Mois, ' . $sub_query_full_total . 'AS Somme FROM ( ' . $string_for_total_query . ')' . ' AS `general1` WHERE type_fichier != "Fisher" AND type_fichier != "gpf" AND type_fichier != "sr" AND type_previsionnel = "R2" Group BY mois_concerne';

        //echo $general_query_total;
        $result_generate_query_total_fisher = $this->db->query($general_query_total_fisher);
        $result_generate_query_total_moulibex_ba = $this->db->query($general_query_total_moulibex_ba);
        $result_generate_query_total_moulibex_r1 = $this->db->query($general_query_total_moulibex_r1);
        $result_generate_query_total_moulibex_r2 = $this->db->query($general_query_total_moulibex_r2);
        /* if ($this->db->table_exists('general_view_compare')) {
             $this->db->query('DROP TABLE IF EXISTS general_view_compare');
         }
         $general_table ='CREATE TABLE  IF NOT EXISTS general_view_compare (id INT AUTO_INCREMENT PRIMARY KEY) AS ' . $general_query;
         $this->db->query($general_table);*/
        //  var_dump($copy_temp);
        $result_generate_query = $this->db->query($general_query);
        $libelle_fisher = $copy_temp;
        $result = [$libelle_fisher, $final_sql, $final_family_sql, $final_sql_table, $result_generate_query, $result_generate_query_total_fisher,$result_generate_query_total_moulibex_ba,$result_generate_query_total_moulibex_r1,$result_generate_query_total_moulibex_r2];
        return $result;
    }


    function sum_all_tab_fisher($app, $div)
    {
        $tab_array = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $tab_array1 = ['January-01', 'February-01', 'March-01', 'April-01', 'May-01', 'June-01', 'July-01', 'August-01', 'September-01', 'October-01', 'November-01', 'December-01'];
        $tab_alias = ['null', 'a', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'];
        $query = '';
        for ($i = 0; $i < count($tab_array); $i++) {
            if ($i == 0) {
                if ($app == null)
                    $query = 'SELECT  
                            SUM(cout_direct)+
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+
                            SUM(shds)+
                            SUM(shdm)+
                            SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+'
                        .
                        'SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf/' . $div . ') ELSE sbmsf END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf_option/' . $div . ') ELSE sbmsf_option END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_classic/' . $div . ') ELSE sbm_classic END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmco_image/' . $div . ')ELSE sbmco_image END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_premium/' . $div . ') ELSE  sbm_premium END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_image/' . $div . ') ELSE sbmpo_image END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_performance/' . $div . ') ELSE sbmpo_performance END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpoi_performance/' . $div . ') ELSE sbmpoi_performance END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmst_masqué/' . $div . ') ELSE sbmst_masqué END) +'
                        .
                        'SUM(CASE WHEN type_fichier = "Fisher" THEN (smbd_go/' . $div . ') ELSE smbd_go END) + 
                        SUM(CASE WHEN type_fichier = "Fisher" THEN (sms_go/' . $div . ') ELSE sms_go END)+ 
                        SUM(CASE WHEN type_fichier = "Fisher" THEN (smhd_go/' . $div . ') ELSE smhd_go END) + 
                        SUM(CASE WHEN type_fichier = "Fisher" THEN (smthd_go/' . $div . ') ELSE smthd_go END) + 
                        SUM(CASE WHEN type_fichier = "Fisher" THEN (smejh_go/' . $div . ') ELSE smejh_go END) +'
                        .
                        'SUM(archivage)+
                         SUM(oahpac)+
                         SUM(oahpsp)+
                         SUM(sbm_basic) 
                            AS "total", type_fichier, (\'' . $tab_array[$i] . '\')AS \'C\' 
                            FROM suivi_stockage 
                            WHERE mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'Fisher\'';
                else
                    $query = 'SELECT  
                            SUM(cout_direct)+ 
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+SUM(shds)+
                            SUM(shdm)+SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+'
                        .
                        ' SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf/' . $div . ') ELSE sbmsf END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf_option/' . $div . ') ELSE sbmsf_option END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_classic/' . $div . ') ELSE sbm_classic END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmco_image/' . $div . ')ELSE sbmco_image END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_premium/' . $div . ') ELSE  sbm_premium END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_image/' . $div . ') ELSE sbmpo_image END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_performance/' . $div . ') ELSE sbmpo_performance END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpoi_performance/' . $div . ') ELSE sbmpoi_performance END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmst_masqué/' . $div . ') ELSE sbmst_masqué END) +'
                        .
                        'SUM(CASE WHEN type_fichier = "Fisher" THEN (smbd_go/' . $div . ') ELSE smbd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sms_go/' . $div . ') ELSE sms_go END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smhd_go/' . $div . ') ELSE smhd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smthd_go/' . $div . ') ELSE smthd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smejh_go/' . $div . ') ELSE smejh_go END) +'
                        .

                        'SUM(archivage)+
                             SUM(oahpac)+
                             SUM(oahpsp)+
                             SUM(sbm_basic) 
                             AS "total", type_fichier, (\'' . $tab_array[$i] . '\') AS \'C\'  
                             FROM suivi_stockage
                             WHERE (mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'Fisher\') AND application =\'' . $app . '\'';
            } else {
                if ($app == null)
                    $query .= ' UNION ALL SELECT  
                            SUM(cout_direct)+
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+
                            SUM(shds)+
                            SUM(shdm)+
                            SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+
                         ' .
                        ' SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf/' . $div . ') ELSE sbmsf END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf_option/' . $div . ') ELSE sbmsf_option END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_classic/' . $div . ') ELSE sbm_classic END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmco_image/' . $div . ')ELSE sbmco_image END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_premium/' . $div . ') ELSE  sbm_premium END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_image/' . $div . ') ELSE sbmpo_image END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_performance/' . $div . ') ELSE sbmpo_performance END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpoi_performance/' . $div . ') ELSE sbmpoi_performance END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmst_masqué/' . $div . ') ELSE sbmst_masqué END) +'
                        .
                        'SUM(CASE WHEN type_fichier = "Fisher" THEN (smbd_go/' . $div . ') ELSE smbd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sms_go/' . $div . ') ELSE sms_go END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smhd_go/' . $div . ') ELSE smhd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smthd_go/' . $div . ') ELSE smthd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smejh_go/' . $div . ') ELSE smejh_go END) +'
                        .
                        'SUM(archivage)+
                            SUM(oahpac)+
                            SUM(oahpsp)+
                            SUM(sbm_basic) 
                            AS "total", type_fichier, (\'' . $tab_array[$i] . '\')AS \'C\' 
                            FROM  (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'Fisher\'))`' . $tab_alias[$i] . '`';
                else
                    $query .= ' UNION ALL SELECT  
                            SUM(cout_direct)+ 
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+SUM(shds)+
                            SUM(shdm)+SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+
                      ' .
                        ' SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf/' . $div . ') ELSE sbmsf END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmsf_option/' . $div . ') ELSE sbmsf_option END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_classic/' . $div . ') ELSE sbm_classic END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmco_image/' . $div . ')ELSE sbmco_image END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbm_premium/' . $div . ') ELSE  sbm_premium END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_image/' . $div . ') ELSE sbmpo_image END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpo_performance/' . $div . ') ELSE sbmpo_performance END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmpoi_performance/' . $div . ') ELSE sbmpoi_performance END) +
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sbmst_masqué/' . $div . ') ELSE sbmst_masqué END) +'
                        .

                        'SUM(CASE WHEN type_fichier = "Fisher" THEN (smbd_go/' . $div . ') ELSE smbd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (sms_go/' . $div . ') ELSE sms_go END)+ 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smhd_go/' . $div . ') ELSE smhd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smthd_go/' . $div . ') ELSE smthd_go END) + 
                            SUM(CASE WHEN type_fichier = "Fisher" THEN (smejh_go/' . $div . ') ELSE smejh_go END) +'
                        .
                        'SUM(archivage)+
                             SUM(oahpac)+
                             SUM(oahpsp)+
                             SUM(sbm_basic) 
                             AS "total", type_fichier,(\'' . $tab_array[$i] . '\') AS \'C\'  
                             FROM (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'Fisher\' AND application =\'' . $app . '\'))`' . $tab_alias[$i] . '`';

            }
        }
        $sql = $this->db->query($query);
        return $sql->result();
    }

    function sum_all_tab_moulibex($app)
    {
        $tab_array = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $tab_array1 = ['January-01', 'February-01', 'March-01', 'April-01', 'May-01', 'June-01', 'July-01', 'August-01', 'September-01', 'October-01', 'November-01', 'December-01'];
        $tab_alias = ['null', 'a', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak'];
        $previ_ba = '';
        $previ_r1 = '';
        $previ_r2 = '';
        for ($i = 0; $i < count($tab_array); $i++) {
            if ($i == 0) {
                $previ_ba = 'SELECT  
                            SUM(cout_direct)+
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+
                            SUM(shds)+
                            SUM(shdm)+
                            SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+
                            SUM(sbmsf)+
                            SUM(sbmsf_option)+
                            SUM(sbm_classic)+
                            SUM(sbmco_image)+
                            SUM(sbm_premium)+
                            SUM(sbmpo_image)+ 
                            SUM(sbmpo_performance)+
                            SUM(sbmpoi_performance)+
                            SUM(sbmst_masqué)+
                            SUM(smbd_go)+
                            SUM(sms_go)+
                            SUM(smhd_go)+
                            SUM(smthd_go)+
                            SUM(smejh_go)+
                            SUM(archivage)+
                            SUM(oahpac)+
                            SUM(oahpsp)+
                            SUM(sbm_basic) 
                            AS "total", type_fichier, (\'' . $tab_array[$i] . '\')AS \'C\' 
                            FROM suivi_stockage ';
                if ($app == null) {
                    $previ_r1 = $previ_ba . ' WHERE mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'MOULIBEX\' AND type_previsionnel = \'R1\'';
                    $previ_ba .= ' WHERE mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'MOULIBEX\' AND type_previsionnel = \'BA\'';
                } else if ($app != null) {
                    $previ_r1 = $previ_ba . ' WHERE (mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'MOULIBEX\') AND application =\'' . $app . '\' AND type_previsionnel = \'R1\' ';
                    $previ_ba .= ' WHERE (mois_concerne = STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND type_fichier = \'MOULIBEX\') AND application =\'' . $app . '\' AND type_previsionnel = \'BA\' ';
                }
            } else {
                $previ_ba .= 'UNION ALL SELECT  
                            SUM(cout_direct)+
                            SUM(oahpsb)+ 
                            SUM(oahpsc)+ 
                            SUM(osc)+ 
                            SUM(shvaf_standard)+
                            SUM(shvaf_securise)+
                            SUM(shva_puissance)+
                            SUM(shvif_standard)+
                            SUM(shvif_securise)+
                            SUM(shvi_puissance)+
                            SUM(shvciif_standard)+
                            SUM(shvcii_puissance)+
                            SUM(shvcppaf_standard)+
                            SUM(shvcppa_puissance)+ 
                            SUM(shvcppif_standard)+
                            SUM(shvcppi_puissance)+
                            SUM(shdes)+
                            SUM(shds)+
                            SUM(shdm)+
                            SUM(shdl)+
                            SUM(shdel)+
                            SUM(shdp_intel)+
                            SUM(shdp_autre)+
                            SUM(shdrs)+
                            SUM(sbmsf)+
                            SUM(sbmsf_option)+
                            SUM(sbm_classic)+
                            SUM(sbmco_image)+
                            SUM(sbm_premium)+
                            SUM(sbmpo_image)+ 
                            SUM(sbmpo_performance)+
                            SUM(sbmpoi_performance)+
                            SUM(sbmst_masqué)+
                            SUM(smbd_go)+
                            SUM(sms_go)+
                            SUM(smhd_go)+
                            SUM(smthd_go)+
                            SUM(smejh_go)+
                            SUM(archivage)+
                            SUM(oahpac)+
                            SUM(oahpsp)+
                            SUM(sbm_basic) 
                            AS "total", type_fichier, (\'' . $tab_array[$i] . '\')AS \'C\' ';
                if ($app == null) {
                    $previ_r1 = $previ_ba . 'FROM  (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'MOULIBEX\' AND type_previsionnel = \'R1\' ))`' . $tab_alias[$i] . '`';
                    $previ_ba .= 'FROM  (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'MOULIBEX\' AND type_previsionnel = \'BA\' ))`' . $tab_alias[$i] . '`';
                } else {
                    $previ_r1 = $previ_ba . 'FROM (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'MOULIBEX\' AND application =\'' . $app . '\' AND type_previsionnel = \'R1\' ))`' . $tab_alias[$i] . '`';
                    $previ_ba .= 'FROM (SELECT * FROM suivi_stockage WHERE ((mois_concerne <= STR_TO_DATE(\'' . $tab_array1[$i] . '\',\'%M-%d\') AND mois_concerne >= STR_TO_DATE(\'' . $tab_array1[0] . '\',\'%M-%d\')) AND type_fichier = \'MOULIBEX\' AND application =\'' . $app . '\' AND type_previsionnel = \'BA\' ))`' . $tab_alias[$i] . '`';
                }
            }
        }
        $sql_ba = $this->db->query($previ_ba)->result();
        $sql_r1 = $this->db->query($previ_r1)->result();
        $result = array($sql_ba, $sql_r1);
        return $result;
    }

    function get_application_fisher()
    {
        $sql = 'SELECT * FROM (SELECT DISTINCT application FROM  suivi_stockage WHERE type_fichier="Fisher") AS t2 INNER JOIN (SELECT DISTINCT application FROM  suivi_stockage WHERE type_fichier="MOULIBEX") AS t1 ON t1.application = t2.application';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_nna_fisher()
    {
        $sql = 'SELECT DISTINCT nna FROM suivi_stockage';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_previsionnel()
    {
        $sql = 'SELECT DISTINCT type_previsionnel FROM suivi_stockage WHERE type_fichier = "moulibex" ';
        // $this->db->select('otp')->group_by('otp');
        //$query = $this->db->get();
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_by_date()
    {
        $sql = 'SELECT DISTINCT mois_concerne FROM suivi_stockage WHERE type_fichier ="fisher"';
        // $this->db->select('otp')->group_by('otp');
        //$query = $this->db->get();
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_otp_fisher()
    {
        $sql = 'SELECT t1.otp FROM (SELECT DISTINCT otp FROM  suivi_stockage WHERE type_fichier="Fisher") AS t2 INNER JOIN (SELECT DISTINCT otp FROM  suivi_stockage WHERE type_fichier="MOULIBEX") AS t1  INNER JOIN (SELECT DISTINCT otp FROM  suivi_stockage WHERE type_fichier="gpf") AS t3 ON t1.otp = t2.otp AND t2.otp = t3.otp';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_date_insert_fisher()
    {
        $sql = 'SELECT DISTINCT mois_concerne FROM suivi_stockage WHERE type_fichier = "Fisher" ORDER BY mois_concerne';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
     function post_config_conversion($i)
    {
        if($i == 1)
            $mysql_query= 'UPDATE configuration_converstion SET uo = 0, ke = 1 WHERE id = 1';
        else if($i == 0)
            $mysql_query= 'UPDATE configuration_converstion SET uo = 1, ke = 0 WHERE id = 1';
        $this->db->query($mysql_query);
    }

}
