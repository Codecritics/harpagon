<?php

/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 04/05/2016
 * Time: 15:48
 */
class Table_upload extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function upload($file, $file1, $end_array)
    {
        $sql2 = "";
        $var = count($end_array);
        $b = 0;
        if (isset($file)) {
            $sql = "LOAD DATA LOCAL INFILE '" . $file1 . "' INTO TABLE table2 FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' IGNORE 1 LINES (";
            foreach ($end_array as $end_array) {
                $sql2 .= $end_array;
                if ($b < $var - 1) {
                    $sql2 .= ', ';
                    ++$b;
                }
            }
            $sql3 = $sql . $sql2 . ") SET ddi = DATE_FORMAT(STR_TO_DATE(@date,'%d/%m/%Y'),'%Y/%m/%d')";
            if ($query = $this->db->query($sql3)) {
                echo json_encode($sql3);
                return $query;
            } else {
                echo json_encode(['Impossible d\'importer le fichier' . $this->db->error()]);
                exit();
            }
        }

    }

    function add_column_main_table($newcol)
    {
        $query = 'ALTER TABLE suivi_stockage ADD `' . $newcol . '` float';
        $this->db->query($query);
    }

    function read_column_main_table()
    {
        $query = 'SHOW COLUMNS FROM suivi_stockage';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    function read_table_parametrage($categorie)
    {
        if (!isset($categorie)) {
            $query = 'SELECT * FROM table_parametrage';
        } else {
            $query = 'SELECT * FROM table_parametrage WHERE categorie = "' . $categorie . '"';
        }
        $result = $this->db->query($query);
        return $result->result_array();
    }

    function read_type_data()
    {
        $query = 'SELECT DISTINCT type_valeur FROM suivi_stockage';
        $this->db->query($query)->result_array;
    }

    function division_data($value)
    {
        $column = "";
        $temp = $this->read_table_parametrage('Stockage de données');
        for ($i = 0; $i < count($temp); $i++) {
            $column .= "" . $temp[$i]['code_prestation'] . " = " . $temp[$i]['code_prestation'] . "/ $value  , ";
        }
        $column = substr($column, 0, -2);
        $query = "UPDATE suivi_stockage " . $column . "";
        $this->db->query($query);
    }

    function read_code_prestation_table_parametrage($libelle_categorie)
    {
        $result = array();
        foreach ($libelle_categorie as $value) {
            $query = 'SELECT code_prestation FROM table_parametrage WHERE categorie = "' . $value . '" AND libelle_moulibex != "" AND libelle_fisher != "" ';
            $result_query = $this->db->query($query)->result_array();
            $result[$value] = array();
            for ($i = 0; $i < count($result_query); $i++) {
                array_push($result[$value], $result_query[$i]['code_prestation']);
            }
        }
        return $result;
    }

    function update_type($string)
    {
        $this->db->update('uploaded_file', array("type_fichier" => $string));
    }

    function upload_moulibex($sheetDataCount, $sheetData, $application, $otp, $type_previsionnel, $final, $commentaire, $libelle)
    {
        $string = "INSERT INTO suivi_stockage (`application`, `otp`, `mois_concerne`, `type_previsionnel`, `date_importation`,`type_fichier`, `cout_direct`,  `shvaf_standard`, `shvif_standard`, `shvi_puissance`, `oahpac`, `oahpsp`,`shdes`,`shdp_intel`,`smbd_go`,`sms_go`,`smhd_go`, `smthd_go`, `smejh_go`, `archivage`,`sbm_premium`,`sbm_classic`,`sbmpo_image`,`sbmpo_performance`,`sbm_basic`,`shva_puissance`,`oahpsc`, `commentaire`) VALUES ";

        $string .= '( "' . $application . '", "' . $otp . '","0000-01-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[0][1] . '", 
            "' . $final[0][2] . '",
            "' . $final[0][3] . '",
            "' . $final[0][4] . '",
            "' . $final[0][5] . '",
            "' . $final[0][6] . '",
            "' . $final[0][7] . '",
            "' . $final[0][8] . '",
            "' . $final[0][9] . '",
            "' . $final[0][10] . '",
            "' . $final[0][11] . '",
            "' . $final[0][12] . '",
            "' . $final[0][13] . '",
            "' . $final[0][14] . '",
            "' . $final[0][15] . '",
            "' . $final[0][16] . '",
            "' . $final[0][17] . '",
            "' . $final[0][18] . '",
            "' . $final[0][19] . '",
            "' . $final[0][20] . '",
            "' . $final[0][21] . '",
            "' . $commentaire . '"
            ),
            ( "' . $application . '", "' . $otp . '","0000-02-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[1][1] . '", 
            "' . $final[1][2] . '",
            "' . $final[1][3] . '",
            "' . $final[1][4] . '",
            "' . $final[1][5] . '",
            "' . $final[1][6] . '",
            "' . $final[1][7] . '",
            "' . $final[1][8] . '",
            "' . $final[1][9] . '",
            "' . $final[1][10] . '",
            "' . $final[1][11] . '",
            "' . $final[1][12] . '",
            "' . $final[1][13] . '",
            "' . $final[1][14] . '",
            "' . $final[1][15] . '",
            "' . $final[1][16] . '",
            "' . $final[1][17] . '",
            "' . $final[1][18] . '",
            "' . $final[1][19] . '",
            "' . $final[1][20] . '", 
            "' . $final[1][21] . '",
            "' . $commentaire . '"),
            ( "' . $application . '", "' . $otp . '","0000-03-01", "' . $type_previsionnel . '", NOW(), "MOULIBEX",
            "' . $final[2][1] . '", 
            "' . $final[2][2] . '",
            "' . $final[2][3] . '",
            "' . $final[2][4] . '",
            "' . $final[2][5] . '",
            "' . $final[2][6] . '",
            "' . $final[2][7] . '",
            "' . $final[2][8] . '",
            "' . $final[2][9] . '",
            "' . $final[2][10] . '",
            "' . $final[2][11] . '",
            "' . $final[2][12] . '",
            "' . $final[2][13] . '",
            "' . $final[2][14] . '",
            "' . $final[2][15] . '",
            "' . $final[2][16] . '",
            "' . $final[2][17] . '",
            "' . $final[2][18] . '",
            "' . $final[2][19] . '",
            "' . $final[2][20] . '", 
             "' . $final[2][21] . '", 
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-04-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[3][1] . '", 
            "' . $final[3][2] . '",
            "' . $final[3][3] . '",
            "' . $final[3][4] . '",
            "' . $final[3][5] . '",
            "' . $final[3][6] . '",
            "' . $final[3][7] . '",
            "' . $final[3][8] . '",
            "' . $final[3][9] . '",
            "' . $final[3][10] . '",
            "' . $final[3][11] . '",
            "' . $final[3][12] . '",
            "' . $final[3][13] . '",
            "' . $final[3][14] . '",
            "' . $final[3][15] . '",
            "' . $final[3][16] . '",
            "' . $final[3][17] . '",
            "' . $final[3][18] . '",
            "' . $final[3][19] . '",
            "' . $final[3][20] . '",
            "' . $final[3][21] . '",
             "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-05-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[4][1] . '", 
            "' . $final[4][2] . '",
            "' . $final[4][3] . '",
            "' . $final[4][4] . '",
            "' . $final[4][5] . '",
            "' . $final[4][6] . '",
            "' . $final[4][7] . '",
            "' . $final[4][8] . '",
            "' . $final[4][9] . '",
            "' . $final[4][10] . '",
            "' . $final[4][11] . '",
            "' . $final[4][12] . '",
            "' . $final[4][13] . '",
            "' . $final[4][14] . '",
            "' . $final[4][15] . '",
            "' . $final[4][16] . '",
            "' . $final[4][17] . '",
            "' . $final[4][18] . '",
            "' . $final[4][19] . '",
            "' . $final[4][20] . '", 
            "' . $final[4][21] . '",
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-06-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[5][1] . '", 
            "' . $final[5][2] . '",
            "' . $final[5][3] . '",
            "' . $final[5][4] . '",
            "' . $final[5][5] . '",
            "' . $final[5][6] . '",
            "' . $final[5][7] . '",
            "' . $final[5][8] . '",
            "' . $final[5][9] . '",
            "' . $final[5][10] . '",
            "' . $final[5][11] . '",
            "' . $final[5][12] . '",
            "' . $final[5][13] . '",
            "' . $final[5][14] . '",
            "' . $final[5][15] . '",
            "' . $final[5][16] . '",
            "' . $final[5][17] . '",
            "' . $final[5][18] . '",
            "' . $final[5][19] . '",
            "' . $final[5][20] . '", 
            "' . $final[5][21] . '",
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-07-01", "' . $type_previsionnel . '", NOW(), "MOULIBEX",
            "' . $final[6][1] . '", 
            "' . $final[6][2] . '",
            "' . $final[6][3] . '",
            "' . $final[6][4] . '",
            "' . $final[6][5] . '",
            "' . $final[6][6] . '",
            "' . $final[6][7] . '",
            "' . $final[6][8] . '",
            "' . $final[6][9] . '",
            "' . $final[6][10] . '",
            "' . $final[6][11] . '",
            "' . $final[6][12] . '",
            "' . $final[6][13] . '",
            "' . $final[6][14] . '",
            "' . $final[6][15] . '",
            "' . $final[6][16] . '",
            "' . $final[6][17] . '",
            "' . $final[6][18] . '",
            "' . $final[6][19] . '",
            "' . $final[6][20] . '",
            "' . $final[6][21] . '", 
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-08-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[7][1] . '", 
            "' . $final[7][2] . '",
            "' . $final[7][3] . '",
            "' . $final[7][4] . '",
            "' . $final[7][5] . '",
            "' . $final[7][6] . '",
            "' . $final[7][7] . '",
            "' . $final[7][8] . '",
            "' . $final[7][9] . '",
            "' . $final[7][10] . '",
            "' . $final[7][11] . '",
            "' . $final[7][12] . '",
            "' . $final[7][13] . '",
            "' . $final[7][14] . '",
            "' . $final[7][15] . '",
            "' . $final[7][16] . '",
            "' . $final[7][17] . '",
            "' . $final[7][18] . '",
            "' . $final[7][19] . '",
            "' . $final[7][20] . '", 
            "' . $final[7][21] . '", 
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-09-01", "' . $type_previsionnel . '", NOW(), "MOULIBEX",
            "' . $final[8][1] . '", 
            "' . $final[8][2] . '",
            "' . $final[8][3] . '",
            "' . $final[8][4] . '",
            "' . $final[8][5] . '",
            "' . $final[8][6] . '",
            "' . $final[8][7] . '",
            "' . $final[8][8] . '",
            "' . $final[8][9] . '",
            "' . $final[8][10] . '",
            "' . $final[8][11] . '",
            "' . $final[8][12] . '",
            "' . $final[8][13] . '",
            "' . $final[8][14] . '",
            "' . $final[8][15] . '",
            "' . $final[8][16] . '",
            "' . $final[8][17] . '",
            "' . $final[8][18] . '",
            "' . $final[8][19] . '",
            "' . $final[8][20] . '",
              "' . $final[8][21] . '",
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-10-01", "' . $type_previsionnel . '", NOW(), "MOULIBEX",
            "' . $final[9][1] . '", 
            "' . $final[9][2] . '",
            "' . $final[9][3] . '",
            "' . $final[9][4] . '",
            "' . $final[9][5] . '",
            "' . $final[9][6] . '",
            "' . $final[9][7] . '",
            "' . $final[9][8] . '",
            "' . $final[9][9] . '",
            "' . $final[9][10] . '",
            "' . $final[9][11] . '",
            "' . $final[9][12] . '",
            "' . $final[9][13] . '",
            "' . $final[9][14] . '",
            "' . $final[9][15] . '",
            "' . $final[9][16] . '",
            "' . $final[9][17] . '",
            "' . $final[9][18] . '",
            "' . $final[9][19] . '",
            "' . $final[9][20] . '", 
            "' . $final[9][21] . '", 
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-11-01", "' . $type_previsionnel . '", NOW(),"MOULIBEX",
            "' . $final[10][1] . '", 
            "' . $final[10][2] . '",
            "' . $final[10][3] . '",
            "' . $final[10][4] . '",
            "' . $final[10][5] . '",
            "' . $final[10][6] . '",
            "' . $final[10][7] . '",
            "' . $final[10][8] . '",
            "' . $final[10][9] . '",
            "' . $final[10][10] . '",
            "' . $final[10][11] . '",
            "' . $final[10][12] . '",
            "' . $final[10][13] . '",
            "' . $final[10][14] . '",
            "' . $final[10][15] . '",
            "' . $final[10][16] . '",
            "' . $final[10][17] . '",
            "' . $final[10][18] . '",
            "' . $final[10][19] . '",
            "' . $final[10][20] . '", 
            "' . $final[10][21] . '", 
            "' . $commentaire . '"),
                        ( "' . $application . '", "' . $otp . '","0000-12-01", "' . $type_previsionnel . '", NOW(), "MOULIBEX",
            "' . $final[11][1] . '", 
            "' . $final[11][2] . '",
            "' . $final[11][3] . '",
            "' . $final[11][4] . '",
            "' . $final[11][5] . '",
            "' . $final[11][6] . '",
            "' . $final[11][7] . '",
            "' . $final[11][8] . '",
            "' . $final[11][9] . '",
            "' . $final[11][10] . '",
            "' . $final[11][11] . '",
            "' . $final[11][12] . '",
            "' . $final[11][13] . '",
            "' . $final[11][14] . '",
            "' . $final[11][15] . '",
            "' . $final[11][16] . '",
            "' . $final[11][17] . '",
            "' . $final[11][18] . '",
            "' . $final[11][19] . '",
            "' . $final[11][20] . '", 
             "' . $final[11][21] . '", 
            "' . $commentaire . '"
            )';

        // $string = substr($string, 0, -1);
        $this->db->query($string);
        /*   $insert_conversion = "INSERT INTO table_conversion (`libelle`) VALUES";
           for($i=0; $i < count($libelle);$i++) {
               $insert_conversion .=  "('".$libelle[$i]."'),";
           }
           $insert_conversion = substr($insert_conversion, 0, -1);
           $this->db->query($insert_conversion);*/
    }

    function upload_moulibex1($sheetDataCount, $sheetData, $nna, $application, $otp, $type_previsionnel, $final, $commentaire, $code_prestation)
    {
        $date = ["0000-01-01", "0000-02-01", "0000-03-01", "0000-04-01", "0000-05-01", "0000-06-01", "0000-07-01", "0000-08-01", "0000-09-01", "0000-10-01", "0000-11-01", "0000-12-01"];
        $sub_query = "`nna`, `application`, `otp`, `mois_concerne`, `type_previsionnel`, `date_importation`, `type_fichier`, `type_valeur`,";
        foreach ($code_prestation as $key => $value) {
            if ($key < count($code_prestation) - 1)
                $sub_query .= "`$value`,";
        }
        $sub_query .= "`commentaire`";
        $sub_query2 = "";
        for ($i = 0; $i < count($final); $i++) {
            $sub_query3 = "";
            for ($j = 0; $j < $sheetDataCount - 1; $j++) {
                $sub_query3 .= '"' . $final[$i][$j] . '",';
            }
            $sub_query2 .= '( "' . $nna . '", "' . $application . '", "' . $otp . '", "' . $date[$i] . '", "' . $type_previsionnel . '", NOW(),"MOULIBEX", "uo", ' . $sub_query3 . '"' . $commentaire . '"),' . "\n";
        }
        $sub_query2 = substr($sub_query2, 0, -2);
        $main_query = "INSERT INTO suivi_stockage (" . $sub_query . ") VALUES" . "\n" . $sub_query2;
        $this->db->query($main_query);
    }

    function upload_fisher($sheetDataCount, $sheetData, $mois_c, $commentaire)
    {
        $string = "INSERT INTO suivi_stockage (`nom_serveur`, `application`, `environnement`,`mois_concerne`, `prod_horsprod`, `clone`, `id_scope_du_serveur`, `nna`, `cle_repartition_eac`, `otp`, `lpp`, `statut_serveur`, `type_os`, `commenditaire_eac`, `plateau_du_serveur`, `pe_serveur`, `chaine_de_service`, `application_rcs`, `service_de _disponibilite_serveur`, `reserve_1`, `reserve_2`, `reserve_3`, `oahpsb`, `oahpsc`, `osc`, `shvaf_standard`, `shvaf_securise`, `shva_puissance`, `shvif_standard`, `shvif_securise`, `shvi_puissance`, `shvciif_standard`, `shvcii_puissance`, `shvcppaf_standard`, `shvcppa_puissance`, `shvcppif_standard`, `shvcppi_puissance`, `shdes`, `shds`, `shdm`, `shdl`, `shdel`, `shdp_intel`, `shdp_autre`, `shdrs`, `sbmsf`,`sbmsf_option`, `sbm_classic`, `sbmco_image`, `sbm_premium`, `sbmpo_image`, `sbmpo_performance`, `sbmpoi_performance`, `sbmst_masqué`, `smbd_go`, `sms_go`, `smhd_go`, `smthd_go`, `smejh_go`, `archivage`, `rs_datacenter`, `h_scientifique`, `date_importation`, `type_fichier`, `commentaire`) VALUES ";
        for ($i = 2; $i <= $sheetDataCount; $i++) {
            $A = trim($sheetData[$i]["A"]);
            $B = trim($sheetData[$i]["B"]);
            $C = trim($sheetData[$i]["C"]);
            $D = trim($sheetData[$i]["D"]);
            $E = trim($sheetData[$i]["E"]);
            $F = trim($sheetData[$i]["F"]);
            $G = trim($sheetData[$i]["G"]);
            $H = trim($sheetData[$i]["H"]);
            $I = trim($sheetData[$i]["I"]);
            $J = trim($sheetData[$i]["J"]);
            $K = trim($sheetData[$i]["K"]);
            $L = trim($sheetData[$i]["L"]);
            $M = trim($sheetData[$i]["M"]);
            $N = trim($sheetData[$i]["N"]);
            $O = trim($sheetData[$i]["O"]);
            $P = trim($sheetData[$i]["P"]);
            $Q = trim($sheetData[$i]["Q"]);
            $R = trim($sheetData[$i]["R"]);
            $S = trim($sheetData[$i]["S"]);
            $T = trim($sheetData[$i]["T"]);
            $U = trim($sheetData[$i]["U"]);
            $V = trim($sheetData[$i]["V"]);
            $W = trim($sheetData[$i]["W"]);
            $X = trim($sheetData[$i]["X"]);
            $Y = trim($sheetData[$i]["Y"]);
            $Z = trim($sheetData[$i]["Z"]);
            $AA = trim($sheetData[$i]["AA"]);
            $AB = trim($sheetData[$i]["AB"]);
            $AC = trim($sheetData[$i]["AC"]);
            $AD = trim($sheetData[$i]["AD"]);
            $AE = trim($sheetData[$i]["AE"]);
            $AF = trim($sheetData[$i]["AF"]);
            $AG = trim($sheetData[$i]["AG"]);
            $AH = trim($sheetData[$i]["AH"]);
            $AI = trim($sheetData[$i]["AI"]);
            $AJ = trim($sheetData[$i]["AJ"]);
            $AK = trim($sheetData[$i]["AK"]);
            $AL = trim($sheetData[$i]["AL"]);
            $AM = trim($sheetData[$i]["AM"]);
            $AN = trim($sheetData[$i]["AN"]);
            $AO = trim($sheetData[$i]["AO"]);
            $AP = trim($sheetData[$i]["AP"]);
            $AQ = trim($sheetData[$i]["AQ"]);
            $AR = trim($sheetData[$i]["AR"]);
            $AS = trim($sheetData[$i]["AS"]);
            $AT = trim($sheetData[$i]["AT"]);
            $AU = trim($sheetData[$i]["AU"]);
            $AV = trim($sheetData[$i]["AV"]);
            $AW = trim($sheetData[$i]["AW"]);
            $AX = trim($sheetData[$i]["AX"]);
            $AY = trim($sheetData[$i]["AY"]);
            $AZ = trim($sheetData[$i]["AZ"]);
            $BA = trim($sheetData[$i]["BA"]);
            $BB = trim($sheetData[$i]["BB"]);
            $BC = trim($sheetData[$i]["BC"]);
            $BD = trim($sheetData[$i]["BD"]);
            $BE = trim($sheetData[$i]["BE"]);
            $BF = trim($sheetData[$i]["BF"]);
            $BG = trim($sheetData[$i]["BG"]);
            $BH = trim($sheetData[$i]["BH"]);
            $BI = trim($sheetData[$i]["BI"]);
            $string .= '("' . $A . '" , "' . $B . '","' . $C . '","' . $mois_c . '", "' . $D . '", "' . $E . '", "' . $F . '", "' . $G . '", "' . $H . '", "' . $I . '", "' . $J . '", "' . $K . '", "' . $L . '", "' . $M . '" , "' . $N . '", "' . $O . '", "' . $P . '", "' . $Q . '", "' . $R . '", "' . $S . '", "' . $T . '", "' . $U . '", "' . $V . '", "' . $W . '", "' . $X . '", "' . $Y . '", "' . $Z . '", "' . $AA . '" , "' . $AB . '","' . $AC . '", "' . $AD . '", "' . $AE . '", "' . $AF . '", "' . $AG . '", "' . $AH . '", "' . $AI . '", "' . $AJ . '", "' . $AK . '", "' . $AL . '", "' . $AM . '" , "' . $AN . '", "' . $AO . '", "' . $AP . '", "' . $AQ . '", "' . $AR . '", "' . $AS . '", "' . $AT . '", "' . $AU . '", "' . $AV . '", "' . $AW . '", "' . $AX . '", "' . $AY . '", "' . $AZ . '","' . $BA . '" , "' . $BB . '","' . $BC . '", "' . $BD . '", "' . $BE . '", "' . $BF . '", "' . $BG . '", "' . $BH . '", "' . $BI . '",NOW(),"Fisher","' . $commentaire . '"),';
        }
        $string = substr($string, 0, -1);
        $this->db->query($string);
    }

    function upload_photo_serveur($sheetData_number, $fisher_val, $code_prestation, $mois_c, $commentaire)
    {
        $main_string = "INSERT INTO suivi_stockage (";
        $sub_query = "";
        $sub_query2 = "";
        $sub_query3 = "";
        foreach ($code_prestation as $key => $value) {
            $sub_query .= "`$value`,";
        }

        $main_string .= $sub_query . "`mois_concerne`,`commentaire`, `date_importation`, `type_fichier`, `type_valeur` ) VALUES \n";
        for ($i = 1; $i < count($sheetData_number); $i++) {
            $sub_query2 = "";
            $sub_query2 .= "(";
            foreach ($fisher_val as $key => $fisher_value) {
                $sub_query2 .= '"' . $sheetData_number[$i][$key] . '"' . ',';
            }
            $sub_query2 .= '"' . $mois_c . '",' . '"' . $commentaire . '"' . ', NOW(), "Fisher", "uo"' . "),\n";
            $sub_query3 .= $sub_query2;
        }
        $sub_query3 = substr($sub_query3, 0, -2);
        $main_string .= $sub_query3;
        $this->db->query($main_string);
    }

    /**
     * @param $sheetData_number
     * @param $libelle
     * @param $fisher_val
     * @param $code_prestation
     * @param $mois_c
     * @param $commentaire
     */
    function upload_photo_application($sheetData_number, $libelle, $fisher_val, $code_prestation, $mois_c, $commentaire)
    {
        $main_string = "INSERT INTO suivi_stockage (";
        $sub_query = "";
        $sub_query2 = "";
        $sub_query3 = "";
        foreach ($code_prestation as $key => $value) {
            $sub_query .= "`$value`,";
        }

        $main_string .= $sub_query . "`mois_concerne`,`commentaire`, `date_importation`, `type_fichier`, `type_valeur` ) VALUES ";
        for ($i = 1; $i < count($sheetData_number); $i++) {
            $sub_query2 = "";
            $sub_query2 .= "(";
            foreach ($fisher_val as $key => $fisher_value) {

                if ($libelle[$key] == "APH - Pilotage Applicatif Classic" && $sheetData_number[$i][$key] == "OUI") {
                    $key_uo = array_search("APH - Pilotage Applicatif Nb UO", $libelle);
                    $sub_query2 .= '"' . $sheetData_number[$i][$key_uo] . '"' . ',';
                } else if ($libelle[$key] == "APH - Pilotage Applicatif Premium" && $sheetData_number[$i][$key] == "OUI") {
                    $key_uo = array_search("APH - Pilotage Applicatif Nb UO", $libelle);
                    $sub_query2 .= '"' . $sheetData_number[$i][$key_uo] . '"' . ',';
                } else
                    $sub_query2 .= '"' . $sheetData_number[$i][$key] . '"' . ',';
            }
            $sub_query2 .= '"' . $mois_c . '",' . '"' . $commentaire . '"' . ', NOW(), "Fisher", "uo"' . "),\n";
            $sub_query3 .= $sub_query2;
        }
        $sub_query3 = substr($sub_query3, 0, -2);
        $main_string .= $sub_query3;
        $this->db->query($main_string);
    }

    function upload_gpf($code_prestation, $value, $otp, $mois_c, $commentaire)
    {
        $sql = 'INSERT INTO suivi_stockage (`' . $code_prestation . '`, `otp`, `type_fichier`, `mois_concerne`,`commentaire`, `date_importation`) VALUES ("' . $value . '", "' . $otp . '", "gpf","' . $mois_c . '","' . $commentaire . '",NOW())';
        $this->db->query($sql);
    }
    function upload_sr($code_prestation, $value, $otp, $mois_c, $commentaire)
    {
        $sql = 'INSERT INTO suivi_stockage (`' . $code_prestation . '`, `otp`, `type_fichier`, `mois_concerne`,`commentaire`, `date_importation`) VALUES ("' . $value . '", "' . $otp . '", "sr", "' . $mois_c . '","' . $commentaire . '",NOW())';
        $this->db->query($sql);
    }

    function upload_file_fisher($mois_c, $fichier, $commentaire, $type_fichier)
    {
        $sql = 'INSERT INTO uploaded_file (`mois_c`,`date_import`,`fichier`,`commentaire`, `type_fichier`) VALUES ("' . $mois_c . '", NOW(),"' . $fichier . '","' . $commentaire . '", "' . $type_fichier . '")';
        $this->db->query($sql);
    }

    function upload_file_moulibex($fichier, $application, $previsionel, $commentaire)
    {
        $sql = 'INSERT INTO uploaded_file1 (`date_import`,`fichier`,`application`,`previsionnel`,`commentaire`, `type_fichier`) VALUES ( NOW(),"' . $fichier . '","' . $application . '","' . $previsionel . '", "' . $commentaire . '", "MOULIBEX")';
        $this->db->query($sql);
    }

    function get_resume_date_uploaded($name_file)
    {
        $sql = 'SELECT DISTINCT MONTHNAME(mois_c) AS MOIS FROM uploaded_file WHERE (type_fichier = "' . $name_file . '")';
        return $this->db->query($sql)->result_array();

    }
    function get_resume_date_uploaded_moulibex()
    {
        $sql = 'SELECT DISTINCT MONTHNAME(mois_concerne) AS MOIS FROM suivi_stockage WHERE (type_fichier = "MOULIBEX")';
        return $this->db->query($sql)->result_array();

    }


    function check_month()
    {
        $sql = 'SELECT DISTINCT MONTHNAME (mois_concerne) FROM suivi_stockage WHERE type_fichier = "Fisher" ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}