<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// default controller for this module
$route['admin'] = 'home';
$route['upload'] = 'Upload';
$route['upload/do_upload'] = 'upload/do_upload';