<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 16/06/2016
 * Time: 09:44
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 *    - Admin Users CRUD
 *    - Admin User Groups CRUD
 *    - Admin User Reset Password
 *    - Account Settings (for login user)
 */
class Upload_fisher extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('Grocery_CRUD');
    }

    function index()
    {
        $crud = $this->generate_crud('uploaded_file');
        $crud->set_theme('datatables');
        $crud->unset_export();
        $crud->unset_print();
        $crud->columns('fichier', 'mois_c', 'date_import');
        $crud->field_type('mois_c','dropdown',
            array('0000-01-01' => 'Janvier', '0000-02-01' => 'Février','0000-03-01' => 'Mars' , '0000-04-01' => 'Avril', '0000-05-01' => 'Mai', 'Juin' => 'Juin', 'Juillet' => 'Juillet', 'Août' => 'Août', 'Septembre' => 'Septembre', 'Octobre' => 'Octobre', 'Novembre' => 'Novembre', 'Decembre' => 'Decembre'));
        $crud->required_fields('mois_c');
        $crud->add_fields('mois_c', 'fichier','commentaire');
        $crud->field_type('commentaire', 'text');
        $crud->display_as('fichier', 'Nom du fichier')->display_as('mois_c','Mois Concerné')->display_as('date_import','Date d\'import du fichier');
        $this->unset_crud_fields('date_import');
        // $crud->callback_before_insert(array($this, 'log_user_before_insert'));
        $this->mTitle .= 'Fichiers Fisher';
        $crud->set_subject('Fichier Fisher');
        $crud->set_field_upload('fichier', 'uploads');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types',
            'csv|xlsx|xls');
        $this->config->set_item('grocery_crud_file_upload_max_file_size',
        '10MB');
        $crud->callback_before_upload(array($this, 'example_callback_before_upload'));
        $crud->callback_after_upload(array($this, 'example_callback_after_upload'));
        $crud->unset_add();
        $crud->unset_delete();
        //    $crud->set_relation('type_fichier','type_fichier','name');
        $this->render_crud();
    }

    function example_callback_before_upload($files_to_upload, $field_info)
    {

        /**
         * Examples of what the $files_to_upload and $field_info will be:
         * $files_to_upload = Array
         * (
         * [sd1e6fec1] => Array
         * (
         * [name] => 86.jpg
         * [type] => image/jpeg
         * [tmp_name] => C:\wamp\tmp\phpFC42.tmp
         * [error] => 0
         * [size] => 258177
         * )
         *
         * )
         *
         * $field_info = stdClass Object
         * (
         *      [field_name] => file_url
         *      [upload_path] => assets/uploads/files
         *      [encrypted_field_name] => sd1e6fec1
         * )
         **/
        $encrypted = $field_info->encrypted_field_name;
        $upload_path = $field_info->upload_path;
        if ((strpos($files_to_upload[$encrypted]['name'], "moulibex") !== false)) {
            $something = $this->input->post('mois_c');
            return $something   ;//
        } else if ((strpos($files_to_upload[$encrypted]['name'], "moulibex") !== true)) {

            return true;


            //return 'Veuillez entrer un fichier contentant le terme moulibex';
            //  $this->render('table/upload_excel');
            return true;
        }
    }

    /**
     * Examples of what the $uploader_response, $files_to_upload and $field_info will be:
     * $uploader_response = Array
     * (
     * [0] => stdClass Object
     * (
     * [name] => 6d9c1-52.jpg
     * [size] => 495375
     * [type] => image/jpeg
     * [url] => http://grocery_crud/assets/uploads/files/6d9c1-52.jpg
     * )
     *
     * )
     *
     * $field_info = stdClass Object
     * (
     * [field_name] => file_url
     * [upload_path] => assets/uploads/files
     * [encrypted_field_name] => sd1e6fec1
     * )
     *
     * $files_to_upload = Array
     * (
     * [sd1e6fec1] => Array
     * (
     * [name] => 86.jpg
     * [type] => image/jpeg
     * [tmp_name] => C:\wamp\tmp\phpFC42.tmp
     * [error] => 0
     * [size] => 258177
     * )
     *
     * )
     */
    function example_callback_after_upload($uploader_response, $field_info, $files_to_upload)
    {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $this->load->library('excel');
        /**
         * On ouvre le fichier Excel et on recupere la cellcollection
         */
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        // $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($file_uploaded);
        //  return $_SERVER['DOCUMENT_ROOT']."/".$upload_path."/".$files_to_upload[$encrypted]['name'];
        /**
         * @param  mixed $nullValue Value returned in the array entry if a cell doesn't
         *                                      exist
         * @param  boolean $calculateFormulas Should formulas be calculated?
         * @param  boolean $formatData Should formatting be applied to cell values?
         * @param  boolean $returnCellRef False - Return a simple array of rows and
         *                                      columns indexed by number counting from zero
         *                                      True - Return rows and columns indexed by their
         *                                      actual row and column IDs
         **/
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $sheetDataCount = count($sheetData);
        $this->load->model('table_upload');
        $something = $this->input->post('mois_c');
        $commentaire = $this->input->post('commentaire');
        $this->table_upload->upload_fisher($sheetDataCount, $sheetData,$something,$commentaire);

        return true;
    }


    function log_user_before_insert($post_array)
    {
        if ($post_array['type_fichier'] == "Moulibex") {
            $post_array['type_fichier'] = 'testo';

        }
        return $post_array;
    }
}