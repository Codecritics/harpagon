<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 03/05/2016
 * Time: 13:15
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 *    - Admin Users CRUD
 *    - Admin User Groups CRUD
 *    - Admin User Reset Password
 *    - Account Settings (for login user)
 */
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Upload extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->model('table_upload');
        $this->load->model('graph_model');
        $this->load->library('excel');
    }

    function index()
    {
        $this->mTitle = 'Uploadez un Fichier';
        $otp = $this->graph_model->get_otp_fisher();
        $application = $this->graph_model->get_application_fisher();
        $nna = $this->graph_model->get_nna_fisher();
        $this->mViewData['error'] = array('error' => ' ');
        $this->load->model('table_upload');
        $array_file_type = ["Serveur", "Application", "MOULIBEX", "gpf", "sr"];
        $month_array = ["0000-01-01", "0000-02-01", "0000-03-01", "0000-04-01", "0000-05-01", "0000-06-01", "0000-07-01", "0000-08-01", "0000-09-01", "0000-10-01", "0000-11-01", "0000-12-01"];
        $array_result = array();
        $temp1 = array();
        foreach ($array_file_type as $value) {
            $temp = $this->table_upload->get_resume_date_uploaded($value);
            $i=0;
            foreach ($temp as $value1) {

                array_push($temp1, $value1['MOIS']);
                array_push($temp1, $month_array[$i]);
                $i++;
            }
            $array_result[$value] = $temp1;
            $temp1 = array();
        }
     //   var_dump($array_result);
        $this->mViewData['result_array_value'] = $array_result;
        $check = $this->table_upload->check_month();
        $this->mViewData['check'] = $check;
        $this->mViewData['nna'] = $nna;
        $this->mViewData['application'] = $application;
        $this->render('table/upload');
        $test = $this->input->get('mois_c');
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = "csv|xls|xlsx|application/json";
        $config['max_size'] = '0';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $this->load->library('upload', $config);


        //  $this->load->view('upload_form');
    }

    function do_upload()
    {

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = "csv|xls|xlsx";
        $config['max_size'] = '0';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('input-name')) {
            $error = array('error' => $this->upload->display_errors());
            $this->mViewData['error'] = $error;
            // $this->render('table/upload');
        } else {
            $data = array('upload_data' => $this->upload->data());
            $filepath = $data['upload_data']['full_path'];
            $filename = $data['upload_data']['file_name'];
            $fileext = $data['upload_data']['file_ext'];
            echo $fileext;
            if ($fileext == ".xlsx" || $fileext == ".xls") {
                $this->mViewData['upload_data'] = $data;
                /**
                 * Si c'est un fichier moulibex
                 */

                if (strpos($filename, "moulibex") !== false) {
                    //  echo "il s'agit d'un fichier moulibex";
                    ini_set('memory_limit', '1024M');
                    ini_set('max_execution_time', 300);
                    /**
                     * On ouvre le fichier Excel et on recupere la cellcollection
                     */
                    $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
                    $reader->open($filepath);
                    $tab = array();
                    $janvier = array();
                    $fevrier = array();
                    $mars = array();
                    $avril = array();
                    $mai = array();
                    $juin = array();
                    $juillet = array();
                    $aout = array();
                    $sept = array();
                    $oct = array();
                    $nov = array();
                    $dec = array();
                    $libelle = array();
                    foreach ($reader->getSheetIterator() as $sheet) {
                        $sheetName = $sheet->getName();
                        if ($sheetName == "Budget") {
                            foreach ($sheet->getRowIterator() as $key => $row) {
                                if ($key == 2) {
                                    $otp_test = strval($row[1]);
                                    break;
                                }
                            }
                            echo $otp_test;
                        }
                        if ($sheetName == "TableauMensuel") {
                            foreach ($sheet->getRowIterator() as $key => $row) {
                                if ($key > 5) {
                                    array_push($tab, $row);
                                    array_push($libelle, $row[0]);
                                    array_push($janvier, $row[1]);
                                    array_push($fevrier, $row[2]);
                                    array_push($mars, $row[3]);
                                    array_push($avril, $row[4]);
                                    array_push($mai, $row[5]);
                                    array_push($juin, $row[6]);
                                    array_push($juillet, $row[7]);
                                    array_push($aout, $row[8]);
                                    array_push($sept, $row[9]);
                                    array_push($oct, $row[10]);
                                    array_push($nov, $row[11]);
                                    array_push($dec, $row[12]);
                                }
                                // var_dump($tab);
                            }
                            break;
                        }
                    }
                    /*
                    * Ici on crée les tableaux où on va stocker les valeurs qui sont des les mois du fichier moulibex
                    * On récupère les inputs envoyés par l'utilisateur
                    */
                    $final = array($janvier, $fevrier, $mars, $avril, $mai, $juin, $juillet, $aout, $sept, $oct, $nov, $dec);
                    $application = $this->input->post('application');
                    $nna = $this->input->post('nna');
                    $previsionnel = $this->input->post('previsionel');
                    $mois_c = empty($_POST['mois_c']) ? '' : $_POST['mois_c'];
                    $commentaire = empty($_POST['commentaire']) ? '' : $_POST['commentaire'];
                    /*
                    * On récupère tous les  éléments qui sont dans la table de paramétrage
                    */
                    $array_table_param = $this->table_upload->read_table_parametrage(null);
                    $curent_array_moulibex = array();
                    $key_moulibex = array();
                    /*
                    * On récupère les Libéllés de la moulibex du tableau de paramétrage
                    */
                    for ($i = 0; $i < count($array_table_param); $i++) {
                        array_push($curent_array_moulibex, $array_table_param[$i]['libelle_moulibex']);
                    }
                    for ($i = 0; $i < count($libelle); $i++) {
                        array_push($key_moulibex, array_search($libelle[$i], $curent_array_moulibex));
                    }

                    $final_val_moulibex = array_filter($key_moulibex, function ($value) {
                        return $value !== '';
                    });

                    $code_prestation = array();
                    $error_check = false;

                    foreach ($final_val_moulibex as $key => $value) {
                        array_push($code_prestation, $array_table_param[$value]['code_prestation']);
                        if ($value !== 0 && $value == null) {
                            echo('Pas de libellé' . $libelle[$key] . ' correspondant dans la table de paramétrage, veuillez tout d\'abord la créer' . "\n");
                            $error_check = true;
                        } else if ($array_table_param[$value]['libelle_moulibex'] == null && $array_table_param[$value]['libelle_moulibex'] !== "Stockage de Données Basic") {
                            echo('Pas de correspondant libellé fisher au libellé "' . $libelle[$key] . '". Veuillez l\'indiquer dans la table de paramétrage.' . "\n");
                            $error_check = true;
                        }
                    }


                    /*
                    * Lis la table principale et on va chequer dans la table principale si le code de  prestation est présent
                    *
                    */
                    $column_main = $this->table_upload->read_column_main_table();
                    $column_mini = array();
                    for ($i = 0; $i < count($column_main); $i++) {
                        array_push($column_mini, $column_main[$i]['Field']);
                    }
                    for ($j = 0; $j < count($code_prestation); $j++) {
                        if (in_array($code_prestation[$j], $column_mini)) {
                            /*
                            * Si on trouve le code prestation en colonne dans la table principale
                            */
                            echo 'trouvé ' . $code_prestation[$j];
                        } else {
                            /*
                            * Sinon on ajoute le code en question
                            */
                            $this->table_upload->add_column_main_table($code_prestation[$j]);
                        }
                    }
                    $this->table_upload->upload_moulibex1(count($tab), $tab, $nna, $application, $otp_test, $previsionnel, $final, $commentaire, $code_prestation);
                     $this->table_upload->upload_file_moulibex($filename, $application, $previsionnel, $commentaire);
                    //$this->table_upload->upload_file_fisher($mois_c, $filename, $commentaire, "MOULIBEX");
                } /**
                 * Si c'est un fichier FISHER
                 */
                else if (strpos($filename, "Photo") !== false && strpos($filename, "Serveurs") !== false) {
                    $mois_c = empty($_POST['mois_c']) ? '' : $_POST['mois_c'];
                    $commentaire = empty($_POST['commentaire']) ? '' : $_POST['commentaire'];
                    ini_set('memory_limit', '1024M');
                    ini_set('max_execution_time', 400);
                    $sheetData = array();
                    /**
                     * On ouvre le fichier Excel et on recupere la cellcollection
                     */
                    $reader = ReaderFactory::create(Type::XLSX);
                    $reader->open($filepath);
                    $libelle = array();
                    foreach ($reader->getSheetIterator() as $sheet) {
                        foreach ($sheet->getRowIterator() as $key => $row) {
                            if ($key == 1) {
                                $libelle = $row;
                            } else if ($key > 1) {
                                array_push($sheetData, $row);
                            }
                        }
                    }
                    print_r($libelle);
                    /**
                     * @param  mixed $nullValue Value returned in the array entry if a cell doesn't
                     *                                      exist
                     * @param  boolean $calculateFormulas Should formulas be calculated?
                     * @param  boolean $formatData Should formatting be applied to cell values?
                     * @param  boolean $returnCellRef False - Return a simple array of rows and
                     *                                      columns indexed by number counting from zero
                     *                                      True - Return rows and columns indexed by their
                     *                                      actual row and column IDs
                     **/
                    /*
                    * On récupère tous les  éléments qui sont dans la table de paramétrage
                    */

                    $array_table_param = $this->table_upload->read_table_parametrage(null);
                    $curent_array_fisher = array();
                    $key_fisher = array();
                    /*
                    * On récupère les Libéllés de fisher du tableau de paramétrage
                    */
                    for ($i = 0; $i < count($array_table_param); $i++) {
                        array_push($curent_array_fisher, $array_table_param[$i]['libelle_fisher']);
                    }
                    for ($i = 0; $i < count($libelle); $i++) {
                        array_push($key_fisher, array_search($libelle[$i], $curent_array_fisher));
                    }
                    $final_val_fisher = array_filter($key_fisher, 'is_numeric');
                    print_r($final_val_fisher);
                    $code_prestation = array();
                    foreach ($final_val_fisher as $key => $value) {
                        array_push($code_prestation, $array_table_param[$value]['code_prestation']);
                        if ($value !== 0 && $value == null) {
                            echo('Pas de libellé ' . $libelle[$key] . ' correspondant dans la table de paramétrage,veuillez tout d\'abord la créer' . "\n");
                            $error_check = true;
                        } else if ($array_table_param[$value]['libelle_fisher'] == null && $array_table_param[$value]['libelle_moulibex'] !== "Stockage de Données Basic") {
                            echo('Pas de correspondant libellé fisher au libellé "' . $libelle[$key] . '". Veuillez l\'indiquer dans la table de paramétrage.' . "\n");
                            $error_check = true;
                        }
                    }

                    print_r($code_prestation);
                    /*
                    * Lis la table principale et on va chequer dans la table principale si le code de  prestation est présent
                    *
                    */
                    $column_main = $this->table_upload->read_column_main_table();
                    $column_mini = array();

                    for ($i = 0; $i < count($column_main); $i++) {
                        array_push($column_mini, $column_main[$i]['Field']);
                    }
                    for ($j = 0; $j < count($code_prestation); $j++) {
                        if (in_array($code_prestation[$j], $column_mini)) {
                            /*
                            * Si on trouve le code prestation en colonne dans la table principale
                            */
                            echo "\n" . 'trouvé ' . $code_prestation[$j] . "\n";
                        } else {
                            /*
                            * Sinon on ajoute le code en question
                            */
                            $this->table_upload->add_column_main_table($code_prestation[$j]);
                        }
                    }
                    $this->load->model('table_upload');
                    $this->table_upload->upload_photo_serveur($sheetData, $final_val_fisher, $code_prestation, $mois_c, $commentaire);
                    /**
                     * On parcours la cell collection et on enregistre les colonnes et les lignes
                     */
                    $this->table_upload->upload_file_fisher($mois_c, $filename, $commentaire, "Serveur");
                } else if (strpos($filename, "Photo") !== false && strpos($filename, "Applications") !== false) {
                    $mois_c = empty($_POST['mois_c']) ? '' : $_POST['mois_c'];
                    $commentaire = empty($_POST['commentaire']) ? '' : $_POST['commentaire'];
                    ini_set('memory_limit', '1024M');
                    ini_set('max_execution_time', 400);
                    /**
                     * On ouvre le fichier Excel et on recupere la cellcollection
                     */
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel = $objReader->load($filepath);
                    /**
                     * @param  mixed $nullValue Value returned in the array entry if a cell doesn't
                     *                                      exist
                     * @param  boolean $calculateFormulas Should formulas be calculated?
                     * @param  boolean $formatData Should formatting be applied to cell values?
                     * @param  boolean $returnCellRef False - Return a simple array of rows and
                     *                                      columns indexed by number counting from zero
                     *                                      True - Return rows and columns indexed by their
                     *                                      actual row and column IDs
                     **/
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $sheetData_number = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
                    $sheetDataCount_number = count($sheetData_number);
                    $sheetDataCount_number_col = count($sheetData_number[0]);
                    /*
                    * On récupère tous les  éléments qui sont dans la table de paramétrage
                    */
                    $array_table_param = $this->table_upload->read_table_parametrage(null);
                    $curent_array_fisher = array();
                    $key_fisher = array();
                    /*
                    * On récupère les Libéllés de fisher du tableau de paramétrage
                    */
                    $libelle = array();
                    for ($i = 0; $i < $sheetDataCount_number_col; $i++) {
                        array_push($libelle, $sheetData_number[0][$i]);
                    }
                    print_r($libelle);
                    for ($i = 0; $i < count($array_table_param); $i++) {
                        array_push($curent_array_fisher, $array_table_param[$i]['libelle_fisher']);
                    }
                    for ($i = 0; $i < count($libelle); $i++) {
                        array_push($key_fisher, array_search($libelle[$i], $curent_array_fisher));
                    }
                    $final_val_fisher = array_filter($key_fisher, 'is_numeric');
                    print_r($final_val_fisher);
                    $code_prestation = array();

                    foreach ($final_val_fisher as $key => $value) {
                        array_push($code_prestation, $array_table_param[$value]['code_prestation']);
                        if ($value !== 0 && $value == null) {
                            echo('Pas de libellé' . $libelle[$key] . 'correspondant dans la table de paramétrage,veuillez tout d\'abord la créer' . "\n");
                            $error_check = true;
                        } else if ($array_table_param[$value]['libelle_fisher'] == null && $array_table_param[$value]['libelle_moulibex'] !== "Stockage de Données Basic") {
                            echo('Pas de correspondant libellé fisher au libellé "' . $libelle[$key] . '". Veuillez l\'indiquer dans la table de paramétrage.' . "\n");
                            $error_check = true;
                        }
                    }

                    print_r($code_prestation);
                    /*
                    * Lis la table principale et on va chequer dans la table principale si le code de  prestation est présent.
                    */
                    $column_main = $this->table_upload->read_column_main_table();
                    $column_mini = array();

                    for ($i = 0; $i < count($column_main); $i++) {
                        array_push($column_mini, $column_main[$i]['Field']);
                    }

                    for ($j = 0; $j < count($code_prestation); $j++) {
                        if (in_array($code_prestation[$j], $column_mini)) {
                            /*
                            * Si on trouve le code prestation en colonne dans la table principale
                            */
                            echo "\n" . 'trouvé ' . $code_prestation[$j] . "\n";
                        } else {
                            /*
                            * Sinon on ajoute le code en question
                            */
                            $this->table_upload->add_column_main_table($code_prestation[$j]);
                        }
                    }
                    $this->load->model('table_upload');
                    $this->table_upload->upload_photo_application($sheetData_number, $libelle, $final_val_fisher, $code_prestation, $mois_c, $commentaire);
                    $this->table_upload->upload_file_fisher($mois_c, $filename, $commentaire, "Application");
                }
            } else if ($fileext == ".csv") {
                if (strpos($filename, "UO") !== false && strpos($filename, "Services") !== false && strpos($filename, "Exploitation") !== false && strpos($filename, "Cormoran") !== false) {
                    $sheetData = array();
                    echo 'GPF';
                    $reader = ReaderFactory::create(Type::CSV);
                    $reader->setFieldDelimiter(';');
                    $reader->setEndOfLineCharacter("\n");
                    $reader->open($filepath);
                    foreach ($reader->getSheetIterator() as $sheet) {
                        foreach ($sheet->getRowIterator() as $key => $row) {
                            if ($key == 1) {
                                $libelle = $row;
                            } else if ($key > 1) {
                                $this->table_upload->upload_gpf($row[2], $row[5], $row[3], $this->input->post('mois_c'), $this->input->post('commentaire'));
                            }
                        }
                    }
                    $reader->close();

                    /*   $row = 1;
                    if (($handle = fopen($filepath, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    echo "<p> $num fields in line $row: <br /></p>\n";
                    $row++;
                    for ($c=0; $c < $num; $c++) {
                    echo $data[$c] . "<br />\n";
                  }
                }
                fclose($handle);
              }*/
                    $mois_c = empty($_POST['mois_c']) ? '' : $_POST['mois_c'];
                    $commentaire = empty($_POST['commentaire']) ? '' : $_POST['commentaire'];
                    $this->table_upload->upload_file_fisher($mois_c, $filename, $commentaire, "gpf");
                } else if (strpos($filename, "UO") !== false && strpos($filename, "Service") !== false && strpos($filename, "IE") !== false && strpos($filename, "IQ") !== false && strpos($filename, "Cormoran") !== false) {
                    echo 'SR';
                    $reader = ReaderFactory::create(Type::CSV);
                    $reader->setFieldDelimiter(';');
                    $reader->setEndOfLineCharacter("\n");
                    $reader->open($filepath);
                    foreach ($reader->getSheetIterator() as $sheet) {
                        foreach ($sheet->getRowIterator() as $key => $row) {
                            if ($key == 1) {
                                $libelle = $row;
                            } else if ($key > 1) {
                                $this->table_upload->upload_sr($row[2], $row[5], $row[3], $this->input->post('mois_c'), $this->input->post('commentaire'));
                            }
                        }
                    }
                    $reader->close();
                    $mois_c = empty($_POST['mois_c']) ? '' : $_POST['mois_c'];
                    $commentaire = empty($_POST['commentaire']) ? '' : $_POST['commentaire'];
                    $this->table_upload->upload_file_fisher($mois_c, $filename, $commentaire, "sr");
                }
            }

        }
    }
}

