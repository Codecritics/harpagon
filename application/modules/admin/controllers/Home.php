<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller
{
    public function index()
    {
        $this->load->model('admin_user_model', 'admin_users');

        $this->load->model('table_upload');
        $array_file_type = ["Serveur", "Application","MOULIBEX","gpf","sr"];
        $array_result = array();
        $temp1 = array();
        foreach ($array_file_type as $value) {
            if($value != "MOULIBEX") {
                $temp = $this->table_upload->get_resume_date_uploaded($value);
                foreach ($temp as $value1) {
                    array_push($temp1, $value1['MOIS']);
                }
                $array_result[$value] = $temp1;
                $temp1 = array();
            }
            else if ($value == "MOULIBEX")
            {
                $temp = $this->table_upload->get_resume_date_uploaded_moulibex();
                foreach($temp as $value1){
                    array_push($temp1, $value1['MOIS']);
                }
                $array_result[$value] = $temp1;
                $temp1 = array();
            }
        }
      // var_dump($array_result);
        $this->mViewData['result_array_value'] = $array_result;
        $this->mViewData['count'] = array(
            'admin_users' => $this->admin_users->count_all(),
        );
        $this->mBodyClass .= 'sidebaqsdfmini';
        $this->mTitle = 'Accueil - Menu';
        $this->render('home');
    }
}
