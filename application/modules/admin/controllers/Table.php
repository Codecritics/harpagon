<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 03/05/2016
 * Time: 13:15
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 *    - Admin Users CRUD
 *    - Admin User Groups CRUD
 *    - Admin User Reset Password
 *    - Account Settings (for login user)
 */
class Table extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_builder');
        $this->load->library('table');
        $this->load->model('graph_model');
        $this->mTitle = 'Application - ';
    }

    public function main_table()
    {
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $crud = $this->generate_crud('suivi_stockage');
        $crud->set_subject("Table Principale");
        /*  $crud->unset_jquery();
          $crud->unset_jquery_ui();
          $crud->unset_bootstrap();*/
        $columns = $this->graph_model->read_table_parametrage(null);
        $code_prestation = array();
        foreach ($columns as $value1) {
            array_push($code_prestation, $value1['code_prestation']);
            $crud->display_as($value1['code_prestation'], $value1['libelle_moulibex']);
        }
        $crud->columns($code_prestation);
        $crud->set_theme('flexigrid');
        $this->unset_crud_fields('uoso', 'dm');
        $crud->display_as('application', 'Application')->display_as('cout_direct', 'Coût Direct')->display_as('nom_serveur', 'Nom du Serveur')->display_as('date_importation', 'Date D\'import du fichier')->display_as('shvaf_standard', 'Serveur virtualisé AIX Fixe Std')->display_as('shvif_securise', 'Serveur virtualisé INTEL Fixe Std');
        $crud->callback_column('mois_concerne', array($this, 'product_scale_callback'));
        // cannot change Admin User groups once created
        /*if ($crud->getState()=='list')
        {
            $crud->set_relation_n_n('groups', 'admin_users_groups', 'admin_groups', 'user_id', 'group_id', 'name');
        }*/
        // disable direct create / delete Admin User
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();

        $this->mTitle .= 'Affichage du Tableau';
        $this->render_crud();
    }

    function product_scale_callback($value, $row)
    {
        setlocale(LC_TIME, "fr_FR");
        return date("F", strtotime($value));
    }


    public function echange(&$tab, $i, $j)
    {
        if ($i != $j) {
            $temporaire = $tab[$i];
            $tab[$i] = $tab[$j];
            $tab[$j] = $temporaire;
        }
    }

    public function tri_selection(&$tab)
    {
        $taille = count($tab);
        for ($i = 0; $i < $taille - 1; ++$i) {
            $i_min = $i;
            for ($j = $i + 1; $j < $taille; ++$j)
                if ($tab[$j] < $tab[$i_min])
                    $i_min = $j;
            $this->echange($tab, $i, $i_min);
        }
    }

    public function main_get_data($libelle_categorie, $code_prestation_categorie)
    {
        $service = array();
        foreach ($libelle_categorie as $value) {
            $service[$value] = $code_prestation_categorie[$value];
        }
        return $service;
    }

    public function conversion()
    {
        $this->graph_model->conversion();
    }

    public function table_conversion()
    {

        $crud = $this->generate_crud('table_parametrage');
        $crud->set_theme('flexigrid');
        $crud->columns('libelle_moulibex', 'libelle_fisher', 'categorie', 'code_prestation');
        //  $crud->unset_delete();
        //  $this->unset_crud_fields('libelle', 'categorie', 'libelle_abrevation');
        //   $crud->unset_add();
        // $crud->unset_edit_fields('libelle');
        $this->mTitle .= 'Table de paramétrage';
        $this->render_crud();
    }

    public function sum_table()
    {
        /*
            Creation du formulaire et recuperation de toutes les valeurs postées
        */
        $form = $this->form_builder->create_form();
        $application = $this->graph_model->get_application_fisher();
        $previ = $this->graph_model->get_previsionnel();
        $input_otp = $this->input->post('otp');
        $input_range = $this->input->post('range_1');
        if (!isset($input_range)) {
            $input_range = ["January", "December"];
        } else {
            $input_range = explode(";", $input_range);
            if (!array_key_exists(0, $input_range))
                $input_range[0] = "January";
            if (!array_key_exists(1, $input_range))
                $input_range[1] = "December";
        }

        $input_previ = $this->input->post('previ');
        $input_nna = $this->input->post('nna');
        $input_app = $this->input->post('application');
        $input_convert = $this->input->post('trigger_convert');
        if ($input_convert == null) {
            $input_convert = 'uo';
        }
        $date = $this->graph_model->get_by_date();

        $this->tri_selection($date);

        /*  $test=  date_parse_from_format('F',$input_range[0]);
          $new_date = '0000-'.$test['month'].'-01';
          echo $new_date;*/


        $otp = $this->graph_model->get_otp_fisher();

        $table_conversion = $this->graph_model->read_table_conversion();
        $key = array();
        $value = array();
        $array_convert = array();

        for ($i = 0; $i < count($table_conversion); $i++) {
            array_push($key, $table_conversion[$i]['libelle_abrevation']);
            array_push($value, (int)$table_conversion[$i]['cout_uo']);
            // echo($table_conversion[$i]['libelle_abrevation'].',');
        }

        $a = array_combine($key, $value);
        $b = array_fill_keys($key, 1);
        if ($input_convert == 'k€') {
            $array_convert = $a;
        }
        if ($input_convert == 'uo') {
            $array_convert = $b;
        }
        // echo ($date('Y-m-d',$input_range[0]));
        //  $stockage_moulibex = $this->graph_model->sum_stockage($input_date);

        $this->load->model('table_upload');
        $libelle_categorie = ['Hosting Virtualisé', 'Hosting Dédié', 'Application Hosting', 'Stockage de données', 'Sauvegarde', 'Intégration/Qualification'];
        ///$hosting_virtualise = $this->table_upload->read_code_prestation_table_parametrage($libelle_categorie);

        $code_prestation_categorie = $this->table_upload->read_code_prestation_table_parametrage($libelle_categorie);


        $process = $this->graph_model->main_get_data($code_prestation_categorie, $input_range[0], $input_range[1], $input_otp, $input_app, 1000);
        $process_temp = $process[1];
        $process_temp1 = $process[0];
        $process_temp2 = $process[2];
        // var_dump($process_temp);
        // var_dump($process_temp);
        //var_dump($process_temp2);
        $temp = array();
        foreach ($process_temp as $label => $value) {
            $label = str_replace(' ', '', $label);
            $label = str_replace('/', '', $label);
            foreach ($value as $label1 => $value1) {
                $temp[$label1] = array();
                foreach ($value1 as $label2 => $value2) {
                    array_push($temp[$label1], $value2);
                    $temp_value = array();
                    foreach ($temp[$label1] as $i => $k) {
                        $temp_value[]['value'] = $k;
                    }
                }
                // var_dump($temp_value);
                $this->mViewData[$label][$label1] = $temp_value;
            }
            //var_dump($this->mViewData['HostingVirtualisé']['MOULIBEX']);
        }

        foreach ($process_temp2 as $label => $value) {
            $label = str_replace(' ', '', $label);
            $sa = array();
            if (array_key_exists(0, $value) && $value[0] !== null) {
                foreach ($value[0] as $key => $code) {
                    array_push($sa, $value[0][$key]);
                }
                $temp_sa = array();
                foreach ($sa as $i => $k) {
                    $temp_sa[]['value'] = $k;
                }
                $this->mViewData[$label]['sumrealisé'] = $temp_sa;
            }

            $sb = array();
            if (array_key_exists(1, $value) && $value[1] !== null) {
                $object_suivi_hosting = $value[1];
                foreach ($value[1] as $key => $code) {
                    array_push($sb, $value[1][$key]);
                }
                $temp_sha1 = array();
                foreach ($sb as $i => $k) {
                    $temp_sha1[]['value'] = $k;
                }
                $this->mViewData[$label]['sumprévisionel'] = $temp_sha1;
            }
        }

        foreach ($process_temp1 as $label => $value) {
            $label = str_replace(' ', '', $label);
            $label = str_replace('/', '', $label);
            $count_label[$label] = array();
            //    $count_label[$label] = array_diff( $count_label[$label], array( '' ) );
            foreach ($value as $key => $code) {
                array_push($count_label[$label], $value[$key]);
                // $count_label[$label] = $value[$key];
            }
        }

        //   var_dump($l);


        foreach ($count_label as $i => $k) {
            $temp_label[$i][] = array();
            for ($j = 0; $j < count($k); $j++) {
                $temp_label[$i][]['label'] = $k[$j];
            }
            //  $temp_label[$i] = array_filter($temp_label[$i]);

            array_shift($temp_label[$i]);
            $this->mViewData[$i]['label'] = $temp_label;
        }
        // var_dump($this->mViewData['IntégrationQualification']['label']);
        //  array_shift($temp_label['HostingVirtualisé']);
        //  var_dump($temp_label['Stockagededonnées']);
        //  var_dump($this->mViewData['Stockagededonnées']['prévisionel']);
        //  var_dump($this->mViewData['Stockagededonnées']['realisé']);


        $this->mViewData['date'] = $date;
        $this->mViewData['otp'] = $otp;
        $this->mViewData['application'] = $application;
        $this->mViewData['previ'] = $previ;

        $template = array(
            'table_open' => '<table id="tableau_service" class="table table-striped table-hover table-bordered">'
        );

        $this->table->set_template($template);
        $generated_array_cormoran = $this->table->generate($process[4]);
        $this->table->set_template($template);
        $generated_array_total_fisher = $this->table->generate($process[5]);
        $this->mViewData['main_value'] = $generated_array_cormoran;
        $this->mViewData['total_array_fisher'] = $generated_array_total_fisher;
        $generated_array_total_moulibex = $this->table->generate($process[7]);
        $this->mViewData['total_array_moulibex'] = $generated_array_total_moulibex;
        /*
         * Processus pour envoyer les valeurs du graphique principale.
         */
        $array_graph_value_gpf = $process[5]->result_array();
        $array_graph_value_moulibex_ba = $process[6]->result_array();
        $array_graph_value_moulibex_r1 = $process[7]->result_array();
        $array_graph_value_moulibex_r2 = $process[8]->result_array();
        $graph_mois_gpf = array();
        $graph_value_gpf = array();
        $graph_mois_moulibex_ba = array();
        $graph_value_moulibex_ba = array();
        $graph_mois_moulibex_r1 = array();
        $graph_value_moulibex_r1 = array();
        $graph_mois_moulibex_r2 = array();
        $graph_value_moulibex_r2 = array();
        foreach ($array_graph_value_gpf as $array_1) {
            array_push($graph_mois_gpf, $array_1['Mois']);
            array_push($graph_value_gpf, $array_1['Somme']);
        }
        foreach ($array_graph_value_moulibex_ba as $array_2) {
            array_push($graph_mois_moulibex_ba, $array_2['Mois']);
            array_push($graph_value_moulibex_ba, $array_2['Somme']);
        }
        foreach ($array_graph_value_moulibex_r1 as $array_2) {
            array_push($graph_mois_moulibex_r1, $array_2['Mois']);
            array_push($graph_value_moulibex_r1, $array_2['Somme']);
        }
        foreach ($array_graph_value_moulibex_r2 as $array_2) {
            array_push($graph_mois_moulibex_r2, $array_2['Mois']);
            array_push($graph_value_moulibex_r2, $array_2['Somme']);
        }
        $copy_graph_value_gpf = $graph_value_gpf;
        $top = 0;
        for ($i = 0; $i < count($graph_value_gpf); $i++) {
            $top += $graph_value_gpf[$i];
            $copy_graph_value_gpf[0] = $graph_value_gpf[0];
            if ($i >= 1) {
                $copy_graph_value_gpf[$i] = $top;
            }
        }
        $copy_graph_value_moulibex_r1 = $graph_value_gpf;
        $top = 0;
        for ($i = 0; $i < count($graph_value_moulibex_r1); $i++) {
            $top += $graph_value_moulibex_r1[$i];
            $copy_graph_value_moulibex_r1[0] = $graph_value_moulibex_r1[0];
            if ($i >= 1) {
                $copy_graph_value_moulibex_r1[$i] = $top;
            }
        }
        $data1 = array($graph_mois_gpf, $copy_graph_value_gpf);
        $data2 = array($graph_mois_moulibex_r1, $copy_graph_value_moulibex_r1);
        $test_data_table_generated_gpf = $this->table->generate($data1);
        $test_data_table_generated_moulibex = $this->table->generate($data2);
        $this->mViewData['test_data_table_generated_gpf'] = $test_data_table_generated_gpf;
        $this->mViewData['test_data_table_generated_moulibex'] = $test_data_table_generated_moulibex;
        $this->mViewData['graph_mois_gpf'] = $graph_mois_gpf;
        $this->mViewData['graph_value_gpf'] = $graph_value_gpf;
        $this->mViewData['graph_mois_moulibex_ba'] = $graph_mois_moulibex_ba;
        $this->mViewData['graph_value_moulibex_ba'] = $graph_value_moulibex_ba;
        $this->mViewData['graph_mois_moulibex_r1'] = $graph_mois_moulibex_r1;
        $this->mViewData['graph_value_moulibex_r1'] = $graph_value_moulibex_r1;
        $this->mViewData['graph_mois_moulibex_r2'] = $graph_mois_moulibex_r2;
        $this->mViewData['graph_value_moulibex_r2'] = $graph_value_moulibex_r2;
        // var_dump($graph_value_gpf);
        /*
         * Fin du processus graphique principale
         */

        //  var_dump($process[5]->result_array());

        //  var_dump($process[3]);
        $this->table->set_template($template);
        $this->mViewData['table_sum'] = $this->table;
        $this->mViewData['table_sum_array'] = $process[3];
        $this->mViewData['form'] = $form;
        $this->render('table/sum_table');
    }


    public function test1()
    {
        $this->load->library('Grocery_CRUD');
        $crud = new Grocery_CRUD();
        // $crud->set_model('Sum_gc_model');
        $crud->set_theme('flexigrid');
        $crud->set_table('test');
        // $crud->basic_model->set_query_str('SELECT * FROM test');
        $this->mTitle .= 'Affichage du Tableau';
        $output = $crud->render();
        $this->mViewData['output'] = $output;
        $this->render('table/test');
    }

    public function convert_uo($i)
    {
        $this->graph_model->post_config_conversion($i);

    }

    public
    function table_edit()
    {
        $crud = $this->generate_crud('table2');
        $crud->columns('sname', 'app', 'otp', 'mc', 'uoh', 'uospo', 'uosc', 'uosp', 'type', 'ddi');
        $this->unset_crud_fields('uoso', 'pname', 'dm');
        $crud->display_as('sname', 'Nom de Serveur')->display_as('app', 'Application')->display_as('otp', 'OTP')->display_as('mc', 'Mois concerné')->display_as('uoh', 'UO Hébergement')->display_as('uospo', 'UO Stockage Prénium et Option Image')->display_as('uosp', 'UO Serveur Puissance')->display_as('uosc', 'UO Stockage Classic')->display_as('type', 'Type de données')->display_as('ddi', 'Date de dernier inventaire');
        // disable direct create / delete Admin User
        $this->mTitle .= 'Modification du Tableau';
        $this->render_crud();
    }

    public
    function table_upload()
    {
        //  $this->load->model('table_upload');
        // $this->table_upload->upload();
        $this->mTitle .= 'Upload';
        //   $this->mViewData['table_upload'] = $demo_id;
        $this->render('table/upload');
    }

// Create Admin User
    public
    function admin_elt_create()
    {
        // (optional) only top-level admin user groups can create Admin User
        //$this->verify_auth(array('webmaster'));

        $form = $this->form_builder->create_form();

        if ($form->validate()) {
            // passed validation
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
            );
            $groups = $this->input->post('groups');

            // create user (default group as "members")
            $user = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);
            if ($user) {
                // success
                $messages = $this->ion_auth->messages();
                $this->system_message->set_success($messages);
            } else {
                // failed
                $errors = $this->ion_auth->errors();
                $this->system_message->set_error($errors);
            }
            refresh();
        }

        $groups = $this->ion_auth->groups()->result();
        unset($groups[0]);    // disable creation of "webmaster" account
        $this->mViewData['groups'] = $groups;
        $this->mTitle .= 'Create Admin User';

        $this->mViewData['form'] = $form;
        $this->render('panel/admin_user_create');
    }

// Admin User Groups CRUD
    public
    function admin_user_group()
    {
        $crud = $this->generate_crud('admin_groups');
        $this->mTitle .= 'Admin User Groups';
        $this->render_crud();
    }

// Admin User Reset password
    public
    function admin_user_reset_password($user_id)
    {
        // only top-level users can reset Admin User passwords
        $this->verify_auth(array('webmaster'));

        $form = $this->form_builder->create_form();
        if ($form->validate()) {
            // pass validation
            $data = array('password' => $this->input->post('new_password'));
            if ($this->ion_auth->update($user_id, $data)) {
                $messages = $this->ion_auth->messages();
                $this->system_message->set_success($messages);
            } else {
                $errors = $this->ion_auth->errors();
                $this->system_message->set_error($errors);
            }
            refresh();
        }

        $this->load->model('admin_user_model', 'admin_users');
        $target = $this->admin_users->get($user_id);
        $this->mViewData['target'] = $target;

        $this->mViewData['form'] = $form;
        $this->mTitle .= 'Reset Admin User Password';
        $this->render('panel/admin_user_reset_password');
    }

// Account Settings
    public
    function account()
    {
        // Update Info form
        $form1 = $this->form_builder->create_form('admin/panel/account_update_info');
        $form1->set_rule_group('panel/account_update_info');
        $this->mViewData['form1'] = $form1;

        // Change Password form
        $form2 = $this->form_builder->create_form('admin/panel/account_change_password');
        $form1->set_rule_group('panel/account_change_password');
        $this->mViewData['form2'] = $form2;

        $this->mTitle = "Account Settings";
        $this->render('panel/account');
    }

// Submission of Update Info form
    public
    function account_update_info()
    {
        $data = $this->input->post();
        if ($this->ion_auth->update($this->mUser->id, $data)) {
            $messages = $this->ion_auth->messages();
            $this->system_message->set_success($messages);
        } else {
            $errors = $this->ion_auth->errors();
            $this->system_message->set_error($errors);
        }

        redirect('admin/panel/account');
    }

// Submission of Change Password form
    public
    function account_change_password()
    {
        $data = array('password' => $this->input->post('new_password'));
        if ($this->ion_auth->update($this->mUser->id, $data)) {
            $messages = $this->ion_auth->messages();
            $this->system_message->set_success($messages);
        } else {
            $errors = $this->ion_auth->errors();
            $this->system_message->set_error($errors);
        }

        redirect('admin/panel/account');
    }

    /**
     * Logout user
     */
    public
    function logout()
    {
        $this->ion_auth->logout();
        redirect('admin/login');
    }
}
