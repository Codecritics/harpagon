<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 15/06/2016
 * Time: 10:50
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 *    - Admin Users CRUD
 *    - Admin User Groups CRUD
 *    - Admin User Reset Password
 *    - Account Settings (for login user)
 */
class Upload_moulibex extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('Grocery_CRUD');
    }

    function index()
    {
        $crud = $this->generate_crud('uploaded_file1');
        $crud->set_theme('datatables');
        $crud->unset_export();
        $crud->unset_print();
        $crud->add_fields('application','previsionnel', 'fichier');
        $crud->columns('fichier','application', 'previsionnel', 'date_import');
        $crud->field_type('previsionnel','dropdown',
            array('BA'=>'BA','R1'=>'R1','R2'=>'R2', 'interm_janvier'=>'interm_janv','interm_fev'=>'interm_fev','interm_mars'=>'interm_mars','interm_avril'=>'interm_avril','interm_mai'=>'interm_mai','interm_juin'=>'interm_juin','interm_juillet'=>'interm_juillet','interm_aout'=>'interm_aout','interm_sep'=>'interm_sep','interm_oct'=>'interm_oct','interm_nov'=>'interm_nov','interm_dec'=>'interm_dec'));
        $this->unset_crud_fields('date_import', 'type_fichier');
      //  $crud->callback_before_insert(array($this, 'log_user_before_insert'));
        $this->mTitle .= 'Fichiers Moulibex';
        $crud->set_subject('Fichier Moulibex');
        $crud->set_field_upload('fichier', './uploads/');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types','csv|xlsx|xls');
        $crud->callback_before_upload(array($this, 'example_callback_before_upload'));
        $crud->callback_after_upload(array($this, 'example_callback_after_upload'));
        //    $crud->set_relation('type_fichier','type_fichier','name');
        $crud->unset_add();
        $crud->unset_delete();
        $this->render_crud();
    }

    function example_callback_before_upload($files_to_upload, $field_info)
    {

        /*
 * Examples of what the $files_to_upload and $field_info will be:
$files_to_upload = Array
(
        [sd1e6fec1] => Array
        (
                [name] => 86.jpg
                [type] => image/jpeg
                [tmp_name] => C:\wamp\tmp\phpFC42.tmp
                [error] => 0
                [size] => 258177
        )

)

$field_info = stdClass Object
(
        [field_name] => file_url
        [upload_path] => assets/uploads/files
        [encrypted_field_name] => sd1e6fec1
)

*/
        $encrypted = $field_info->encrypted_field_name;
        if ((strpos($files_to_upload[$encrypted]['name'], "moulibex") !== false)) {
            return true;//
        } else if ((strpos($files_to_upload[$encrypted]['name'], "moulibex") !== true)) {
            return 'Veuillez entrer un fichier contentant le terme moulibex';
        }
    }
    function example_callback_after_upload($uploader_response, $field_info, $files_to_upload)
    {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $this->load->library('excel');

        /**
         * On ouvre le fichier Excel et on recupere la cellcollection
         */
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        // $objReader->setReadDataOnly(true);
        $objReader->setLoadSheetsOnly('TableauMensuel');


        $objPHPExcel = $objReader->load($file_uploaded);

        $otp = $objPHPExcel->getActiveSheet()->getCell('A3')->getValue();

        //  return $_SERVER['DOCUMENT_ROOT']."/".$upload_path."/".$files_to_upload[$encrypted]['name'];
        /**
         * @param  mixed $nullValue Value returned in the array entry if a cell doesn't
         *                                      exist
         * @param  boolean $calculateFormulas Should formulas be calculated?
         * @param  boolean $formatData Should formatting be applied to cell values?
         * @param  boolean $returnCellRef False - Return a simple array of rows and
         *                                      columns indexed by number counting from zero
         *                                      True - Return rows and columns indexed by their
         *                                      actual row and column IDs
         **/
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

        $sheetDataCount = count($sheetData);

        $janvier = array();

        $fevrier = array();
        $mars = array();
        $avril = array();
        $mai = array();
        $juin = array();
        $juillet = array();
        $aout = array();
        $sept = array();
        $oct = array();
        $nov = array();
        $dec = array();

        for ($i = 3; $i < $sheetDataCount - 1; $i++) {
            array_push($janvier, $sheetData[$i][1]);
            array_push($fevrier, $sheetData[$i][2]);
            array_push($mars, $sheetData[$i][3]);
            array_push($avril, $sheetData[$i][4]);
            array_push($mai, $sheetData[$i][5]);
            array_push($juin, $sheetData[$i][6]);
            array_push($juillet, $sheetData[$i][7]);
            array_push($aout, $sheetData[$i][8]);
            array_push($sept, $sheetData[$i][9]);
            array_push($oct, $sheetData[$i][10]);
            array_push($nov, $sheetData[$i][11]);
            array_push($dec, $sheetData[$i][12]);
        }

        $final = array($janvier, $fevrier, $mars, $avril, $mai, $juin, $juillet, $aout, $sept, $oct, $nov, $dec);

        $this->load->model('table_upload');

        $application = $this->input->post('application');

        $previsionnel = $this->input->post('previsionnel');

        $this->table_upload->upload_moulibex($sheetDataCount,$sheetData,$application,$otp,$previsionnel,$final);

        return true;
    }


    function log_user_before_insert($post_array)
    {
        if ($post_array['type_fichier'] == "Moulibex") {
            $post_array['type_fichier'] = 'testo';

        }
        return $post_array;
    }
}
