<?php

/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 01/12/2016
 * Time: 15:46
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cormoran extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');

        $this->load->library('form_builder');
        $this->load->library('table');
        $this->load->model('graph_model');
        $this->mTitle = 'Vision Cormoran ';
    }

    public function echange(&$tab, $i, $j)
    {
        if ($i != $j) {
            $temporaire = $tab[$i];
            $tab[$i] = $tab[$j];
            $tab[$j] = $temporaire;
        }
    }

    public function tri_selection(&$tab)
    {
        $taille = count($tab);
        for ($i = 0; $i < $taille - 1; ++$i) {
            $i_min = $i;
            for ($j = $i + 1; $j < $taille; ++$j)
                if ($tab[$j] < $tab[$i_min])
                    $i_min = $j;
            $this->echange($tab, $i, $i_min);
        }
    }


    public function cormoran()
    {
       // $this->load->helper('url');
        $this->load->model('table_upload');
        $form = $this->form_builder->create_form();
        $application = $this->graph_model->get_application_fisher();
        $previ = $this->graph_model->get_previsionnel();
        $input_otp = $this->input->post('otp');
        $input_range = $this->input->post('range_1');

        // var_dump($input_range);
        if (!isset($input_range)) {
            $input_range = ["January", "December"];
        } else {
            $input_range = explode(";", $input_range);
            if (!array_key_exists(0, $input_range))
                $input_range[0] = "January";
            if (!array_key_exists(1, $input_range))
                $input_range[1] = "December";
        }

        $input_previ = $this->input->post('previ');
        $input_nna = $this->input->post('nna');
        $input_app = $this->input->post('application');
        $input_convert = $this->input->post('trigger_convert');
        if ($input_convert == null) {
            $input_convert = 'uo';
        }
        $date = $this->graph_model->get_by_date();

        $this->tri_selection($date);

        /*  $test=  date_parse_from_format('F',$input_range[0]);
          $new_date = '0000-'.$test['month'].'-01';
          echo $new_date;*/
        $libelle_categorie = ['Hosting Virtualisé', 'Hosting Dédié', 'Application Hosting', 'Stockage de données', 'Sauvegarde'];
        ///$hosting_virtualise = $this->table_upload->read_code_prestation_table_parametrage($libelle_categorie);
        $this->load->model('cormoran_model');
        $code_prestation_categorie = $this->cormoran_model->read_code_prestation_table_parametrage($libelle_categorie);

        $main_value = $this->cormoran_model->main_get_data($code_prestation_categorie, $input_range[0], $input_otp, $input_app, 1000);

        $temp_array_cormoran = array();

        $temp_array_total_cormoran = array();

        foreach ($main_value[3] as $query_temp) {
            $template = array(
                'table_open'  => '<table  class="table table-striped table-hover table-bordered">'
            );
            $this->table->set_template($template);
            $generated_array_total_cormoran = $this->table->generate($query_temp);
            array_push($temp_array_total_cormoran,$generated_array_total_cormoran);
        }
        $this->mViewData['main_value_total'] = $temp_array_total_cormoran;

        foreach ($main_value[2] as $query) {
            $template = array(
                'table_open'  => '<table  class="table table-striped table-hover table-bordered">'
            );
            $this->table->set_template($template);
            $generated_array_cormoran = $this->table->generate($query);
            array_push($temp_array_cormoran,$generated_array_cormoran);
        }
        $this->mViewData['main_value'] = $temp_array_cormoran;

        $this->load->library('Grocery_CRUD');
        $crud = new Grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->unset_operations();
        $crud->set_table('general_view_cormoran');
        $output = $crud->render();

        $this->mViewData['output'] = $output;
        $crud0 = new Grocery_CRUD();
        $crud0->set_theme('datatables');
        $crud0->unset_operations();
      //-  print_r($this->db->list_tables());
        var_dump($main_value[0][0]);
        // var_dump($main_value[0][4]);
        $crud0->set_table($main_value[0][0]);
        // $crud->basic_model->set_query_str('SELECT * FROM test');
        $output0 = $crud0->render();
        $this->mViewData['output0'] = $output0;

        /*
         * Dans cette situation on peut optimiser en faisant une boucle qui va gerener des variables dynamiques, notamment $$a
         */
       $crud1 = new Grocery_CRUD();
        $crud1->set_theme('datatables');
        $crud1->unset_operations();
        $crud1->set_table($main_value[0][1]);
        $output1 = $crud1->render();
        $this->mViewData['output1'] = $output1;

        $crud2 = new Grocery_CRUD();
        $crud2->set_theme('datatables');
        $crud2->unset_operations();
        $crud2->set_table($main_value[0][2]);
        $output2 = $crud2->render();
        $this->mViewData['output2'] = $output2;
        /*
                $crud3 = new Grocery_CRUD();
                $crud3->set_theme('flexigrid');
                $crud3->unset_operations();
                $crud3->set_table($main_value[0][3]);
                $output3 = $crud3->render();
                $this->mViewData['output3']= $output3;
        */
       $crud4 = new Grocery_CRUD();
        $crud4->set_theme('datatables');
        $crud4->unset_operations();
        $crud4->set_table($main_value[0][4]);
        $output4 = $crud4->render();
        $this->mViewData['output4'] = $output4;

        $otp = $this->graph_model->get_otp_fisher();
        $this->mViewData['date'] = $date;
        $this->mViewData['otp'] = $otp;
        $this->mViewData['application'] = $application;
        $this->mViewData['previ'] = $previ;
        $this->mViewData['form'] = $form;
        $this->render('cormoran/cormoran');
    }
}
