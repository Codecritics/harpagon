-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jun 16, 2016 at 02:29 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ci_bootstrap_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups`
--

CREATE TABLE `admin_groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_groups`
--

INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES
(1, 'webmaster', 'Webmaster'),
(2, 'admin', 'Administrator'),
(3, 'manager', 'Manager'),
(4, 'staff', 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login_attempts`
--

CREATE TABLE `admin_login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES
(1, '127.0.0.1', 'webmaster', '$2y$08$/X5gzWjesYi78GqeAv5tA.dVGBVP7C1e1PzqnYCVe5s1qhlDIPPES', NULL, NULL, NULL, NULL, NULL, NULL, 1451900190, 1466063014, 1, 'Webmaster', NULL),
(2, '127.0.0.1', 'admin', '$2y$08$7Bkco6JXtC3Hu6g9ngLZDuHsFLvT7cyAxiz1FzxlX5vwccvRT7nKW', NULL, NULL, NULL, NULL, NULL, NULL, 1451900228, 1462275803, 1, 'Admin', ''),
(3, '127.0.0.1', 'manager', '$2y$08$snzIJdFXvg/rSHe0SndIAuvZyjktkjUxBXkrrGdkPy1K6r5r/dMLa', NULL, NULL, NULL, NULL, NULL, NULL, 1451900430, 1462275670, 1, 'Manager', ''),
(4, '127.0.0.1', 'staff', '$2y$08$NigAXjN23CRKllqe3KmjYuWXD5iSRPY812SijlhGeKfkrMKde9da6', NULL, NULL, NULL, NULL, NULL, NULL, 1451900439, 1462277956, 1, 'Staff', ''),
(5, '::1', 'test', '$2y$08$ePZMyZvymQzmt/JhWmw8jOahwNv5fUz6QH1frQa.dhFoHSobI6v5q', NULL, NULL, NULL, NULL, NULL, NULL, 1462284130, NULL, 1, 'test', 'test'),
(6, '::1', 'super luke', '$2y$08$nZ2PR7.dMnAyfuQGkJAULe/j68RlRVKASTEGrqYt9TgYZMQu1n.N2', NULL, NULL, NULL, NULL, NULL, NULL, 1462286753, 1462286795, 1, 'Tanawa Tsamo', 'Mariu'),
(7, '::1', 'testadmin', '$2y$08$AkvCHeAdF.xfjzAG/m1fLeaZ4McicEZ98DoE1KOmRVO2GoPmMKwf2', NULL, NULL, NULL, NULL, NULL, NULL, 1462373692, 1463996895, 1, 'test', 'admin'),
(8, '::1', 'teststaff', '$2y$08$jEuyZi4wPV/DtvI9c1gU4uGa2lwSZwhho55KYTnYBdfUT.MwufzDa', NULL, NULL, NULL, NULL, NULL, NULL, 1462373707, 1463997248, 1, 'test', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users_groups`
--

CREATE TABLE `admin_users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users_groups`
--

INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 2),
(6, 6, 4),
(7, 7, 2),
(8, 8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `api_access`
--

CREATE TABLE `api_access` (
  `id` int(11) unsigned NOT NULL,
  `key` varchar(40) NOT NULL DEFAULT '',
  `controller` varchar(50) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_keys`
--

CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_limits`
--

CREATE TABLE `api_limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_logs`
--

CREATE TABLE `api_logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `demo_blog_categories`
--

CREATE TABLE `demo_blog_categories` (
  `id` int(11) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `demo_blog_categories`
--

INSERT INTO `demo_blog_categories` (`id`, `pos`, `title`) VALUES
(1, 1, 'Category 1'),
(2, 2, 'Category 2'),
(3, 3, 'Category 3');

-- --------------------------------------------------------

--
-- Table structure for table `demo_blog_posts`
--

CREATE TABLE `demo_blog_posts` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `author_id` int(11) NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `content_brief` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `publish_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('draft','active','hidden') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `demo_blog_posts`
--

INSERT INTO `demo_blog_posts` (`id`, `category_id`, `author_id`, `title`, `image_url`, `content_brief`, `content`, `publish_time`, `status`) VALUES
(1, 1, 2, 'Blog Post 1', '', '<p>\r\n	Blog Post 1 Content Brief</p>\r\n', '<p>\r\n	Blog Post 1 Content</p>\r\n', '2015-09-25 22:00:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `demo_blog_posts_tags`
--

CREATE TABLE `demo_blog_posts_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `demo_blog_posts_tags`
--

INSERT INTO `demo_blog_posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `demo_blog_tags`
--

CREATE TABLE `demo_blog_tags` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `demo_blog_tags`
--

INSERT INTO `demo_blog_tags` (`id`, `title`) VALUES
(1, 'Tag 1'),
(2, 'Tag 2');

-- --------------------------------------------------------

--
-- Table structure for table `demo_cover_photos`
--

CREATE TABLE `demo_cover_photos` (
  `id` int(11) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `image_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','hidden') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `demo_cover_photos`
--

INSERT INTO `demo_cover_photos` (`id`, `pos`, `image_url`, `status`) VALUES
(1, 2, '45296-2.jpg', 'active'),
(2, 1, '2934f-1.jpg', 'active'),
(3, 3, '3717d-3.jpg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `moulibex`
--

CREATE TABLE `moulibex` (
  `id` bigint(20) unsigned NOT NULL,
  `type_presta` text NOT NULL,
  `mois` varchar(20) NOT NULL,
  `valeur` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suivi_stockage`
--

CREATE TABLE `suivi_stockage` (
  `id` bigint(20) unsigned NOT NULL,
  `nom_serveur` varchar(50) NOT NULL,
  `application` varchar(120) NOT NULL,
  `environnement` varchar(120) NOT NULL,
  `mois_concerne` varchar(30) NOT NULL,
  `prod_horsprod` varchar(120) NOT NULL,
  `clone` varchar(120) NOT NULL,
  `id_scope_du_serveur` varchar(120) NOT NULL,
  `nna` varchar(4) NOT NULL,
  `cle_repartition_eac` int(11) NOT NULL,
  `otp` varchar(120) NOT NULL,
  `lpp` varchar(120) NOT NULL,
  `statut_serveur` varchar(50) NOT NULL,
  `type_os` varchar(50) NOT NULL,
  `commenditaire_eac` varchar(50) NOT NULL,
  `plateau_du_serveur` varchar(120) NOT NULL,
  `pe_serveur` varchar(120) NOT NULL,
  `chaine_de_service` varchar(120) NOT NULL,
  `application_rcs` varchar(120) NOT NULL,
  `service_de _disponibilite_serveur` varchar(50) NOT NULL,
  `reserve_1` float NOT NULL,
  `reserve_2` float NOT NULL,
  `reserve_3` float NOT NULL,
  `oahpsb` float NOT NULL,
  `oahpsc` float NOT NULL,
  `osc` float NOT NULL,
  `shvaf_standard` float NOT NULL,
  `shvaf_securise` float NOT NULL,
  `shva_puissance` float NOT NULL,
  `shvif_standard` float NOT NULL,
  `shvif_securise` float NOT NULL,
  `shvi_puissance` float NOT NULL,
  `shvciif_standard` float NOT NULL,
  `shvcii_puissance` float NOT NULL,
  `shvcppaf_standard` float NOT NULL,
  `shvcppa_puissance` float NOT NULL,
  `shvcppif_standard` float NOT NULL,
  `shvcppi_puissance` float NOT NULL,
  `shdes` float NOT NULL,
  `shds` float NOT NULL,
  `shdm` float NOT NULL,
  `shdl` float NOT NULL,
  `shdel` float NOT NULL,
  `shdp_intel` float NOT NULL,
  `shdp_autre` float NOT NULL,
  `shdrs` float NOT NULL,
  `sbmsf` float NOT NULL,
  `sbmsf_option` float NOT NULL,
  `sbm_classic` float NOT NULL,
  `sbmco_image` float NOT NULL,
  `sbm_premium` float NOT NULL,
  `sbmpo_image` float NOT NULL,
  `sbmpo_performance` float NOT NULL,
  `sbmpoi_performance` float NOT NULL,
  `sbmst_masqué` float NOT NULL,
  `smbd_go` float NOT NULL,
  `sms_go` float NOT NULL,
  `smhd_go` float NOT NULL,
  `smthd_go` float NOT NULL,
  `smejh_go` float NOT NULL,
  `archivage` float NOT NULL,
  `rs_datacenter` float NOT NULL,
  `h_scientifique` float NOT NULL,
  `date_importation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suivi_stockage_type_fichier`
--

CREATE TABLE `suivi_stockage_type_fichier` (
  `id` bigint(20) unsigned NOT NULL,
  `suivi_stockage_id` int(11) DEFAULT NULL,
  `type_fichier_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table2`
--

CREATE TABLE `table2` (
  `id` int(11) NOT NULL,
  `sname` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `app` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `mc` int(11) DEFAULT NULL,
  `pname` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `uoh` int(11) DEFAULT NULL,
  `uospo` int(11) DEFAULT NULL,
  `uosc` int(11) DEFAULT NULL,
  `uoso` int(11) DEFAULT NULL,
  `uosp` int(11) DEFAULT NULL,
  `type` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `ddi` date DEFAULT NULL,
  `dm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4851 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table2`
--

INSERT INTO `table2` (`id`, `sname`, `app`, `otp`, `mc`, `pname`, `uoh`, `uospo`, `uosc`, `uoso`, `uosp`, `type`, `ddi`, `dm`) VALUES
(1, 'TEST1', 'SIMM', 1, 0, 'REDA', 2, 1, 1, NULL, 1, 'R6', '2016-03-14', '2016-03-13 23:00:00'),
(2, 'TEST2', 'AEL', 1, 0, 'REDA', 2, 2, 2, NULL, 2, 'R2', '2016-03-16', '2016-03-13 23:00:00'),
(3, 'TEST 3', 'AEL', 3, 0, 'REDA', 9, 3, 3, NULL, 3, 'R3', '2016-03-14', '2016-04-18 14:23:58'),
(4815, 'NOEYYB1', 'SIMM', 981, 0, 'Payen', 1, NULL, 5, NULL, 32, '1', '2016-05-25', '2016-05-23 12:01:00'),
(4816, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, NULL, 10, NULL, 12, '2', NULL, '2016-05-23 12:01:00'),
(4817, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, NULL, 1, NULL, 442, '3', NULL, '2016-05-23 12:01:00'),
(4818, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, NULL, 5, NULL, 1000, '4', NULL, '2016-05-23 12:01:00'),
(4819, 'NOEYYBC', 'SIMM', 180, 0, 'Mant', 1, NULL, 5, NULL, 23, '5', NULL, '2016-05-23 12:01:00'),
(4820, 'PCYYY9U', 'AEL', 120, 0, 'Reda', 10, NULL, 1, NULL, 23, '6', NULL, '2016-05-23 12:01:00'),
(4821, 'PCYYY9V', 'AEL', 120, 0, 'Reda', 10, NULL, 2, NULL, 11, '7', NULL, '2016-05-23 12:01:00'),
(4822, 'PCYYY9W', 'AEL', 120, 0, 'Reda', 10, NULL, 3, NULL, 32, '8', NULL, '2016-05-23 12:01:00'),
(4823, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, NULL, 10, NULL, 12, '2', NULL, '2016-05-23 12:01:00'),
(4824, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, NULL, 1, NULL, 442, '3', NULL, '2016-05-23 12:01:00'),
(4825, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, NULL, 5, NULL, 1000, '4', NULL, '2016-05-23 12:01:00'),
(4826, 'NOEYYBC', 'SIMM', 180, 0, 'Mant', 1, NULL, 5, NULL, 23, '5', NULL, '2016-05-23 12:01:00'),
(4827, 'PCYYY9U', 'AEL', 120, 0, 'Reda', 10, NULL, 1, NULL, 23, '6', NULL, '2016-05-23 12:01:00'),
(4828, 'PCYYY9V', 'AEL', 120, 0, 'Reda', 10, NULL, 2, NULL, 11, '7', NULL, '2016-05-23 12:01:00'),
(4829, 'PCYYY9W', 'AEL', 120, 0, 'Reda', 10, NULL, 3, NULL, 32, '8', NULL, '2016-05-23 12:01:00'),
(4830, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, NULL, 10, NULL, 12, '2', NULL, '2016-05-23 12:01:00'),
(4831, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, NULL, 1, NULL, 442, '3', NULL, '2016-05-23 12:01:00'),
(4832, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, NULL, 5, NULL, 1000, '4', NULL, '2016-05-23 12:01:00'),
(4833, 'NOEYYB1', 'SIMM', 981, 0, 'Payen', 1, 5, 32, NULL, 1, 'R2', '2016-01-10', '2016-05-23 13:03:03'),
(4834, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, 10, 12, NULL, 2, 'R2', '2016-01-11', '2016-05-23 13:03:03'),
(4835, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, 1, 442, NULL, 3, 'R2', '2016-01-12', '2016-05-23 13:03:03'),
(4836, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, 5, 1000, NULL, 4, 'R2', '2016-01-13', '2016-05-23 13:03:03'),
(4837, 'NOEYYBC', 'SIMM', 180, 0, 'Mant', 1, 5, 23, NULL, 5, 'R2', '2016-01-14', '2016-05-23 13:03:03'),
(4838, 'PCYYY9U', 'AEL', 120, 0, 'Reda', 10, 1, 23, NULL, 6, 'R2', '2016-01-15', '2016-05-23 13:03:03'),
(4839, 'PCYYY9V', 'AEL', 120, 0, 'Reda', 10, 2, 11, NULL, 7, 'R2', '2016-01-16', '2016-05-23 13:03:03'),
(4840, 'PCYYY9W', 'AEL', 120, 0, 'Reda', 10, 3, 32, NULL, 8, 'R2', '2016-01-17', '2016-05-23 13:03:03'),
(4841, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, 10, 12, NULL, 2, 'R2', '2016-01-11', '2016-05-23 13:03:03'),
(4842, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, 1, 442, NULL, 3, 'R2', '2016-01-12', '2016-05-23 13:03:03'),
(4843, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, 5, 1000, NULL, 4, 'R2', '2016-01-13', '2016-05-23 13:03:03'),
(4844, 'NOEYYBC', 'SIMM', 180, 0, 'Mant', 1, 5, 23, NULL, 5, 'R2', '2016-01-14', '2016-05-23 13:03:03'),
(4845, 'PCYYY9U', 'AEL', 120, 0, 'Reda', 10, 1, 23, NULL, 6, 'R2', '2016-01-15', '2016-05-23 13:03:03'),
(4846, 'PCYYY9V', 'AEL', 120, 0, 'Reda', 10, 2, 11, NULL, 7, 'R2', '2016-01-16', '2016-05-23 13:03:03'),
(4847, 'PCYYY9W', 'AEL', 120, 0, 'Reda', 10, 3, 32, NULL, 8, 'R2', '2016-01-17', '2016-05-23 13:03:03'),
(4848, 'NOEYYB2', 'SIMM', 981, 0, 'Payen', 1, 10, 12, NULL, 2, 'R2', '2016-01-11', '2016-05-23 13:03:03'),
(4849, 'NOEYYBA', 'SIMM', 180, 0, 'Mant', 2, 1, 442, NULL, 3, 'R2', '2016-01-12', '2016-05-23 13:03:03'),
(4850, 'NOEYYBB', 'SIMM', 180, 0, 'Mant', 0, 5, 1000, NULL, 4, 'R2', '2016-01-13', '2016-05-23 13:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `test_table`
--

CREATE TABLE `test_table` (
  `col1` varchar(20) NOT NULL,
  `col2` varchar(20) NOT NULL,
  `col3` varchar(20) NOT NULL,
  `col4` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_table`
--

INSERT INTO `test_table` (`col1`, `col2`, `col3`, `col4`) VALUES
('1 1', '1  2', '1 3', '1 4'),
('2 1', '2 2', '2 3', '2 4'),
('1 1', '1  2', '1 3', '1 4'),
('2 1', '2 2', '2 3', '2 4');

-- --------------------------------------------------------

--
-- Table structure for table `type_fichier`
--

CREATE TABLE `type_fichier` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_fichier`
--

INSERT INTO `type_fichier` (`id`, `name`) VALUES
(1, 'Fisher'),
(2, 'Moulibex');

-- --------------------------------------------------------

--
-- Table structure for table `uploaded_file`
--

CREATE TABLE `uploaded_file` (
  `id` bigint(20) unsigned NOT NULL,
  `fichier` varchar(100) NOT NULL,
  `mois_c` varchar(20) NOT NULL,
  `date_import` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'member', '$2y$08$kkqUE2hrqAJtg.pPnAhvL.1iE7LIujK5LZ61arONLpaBBWh/ek61G', NULL, 'member@member.com', NULL, NULL, NULL, NULL, 1451903855, 1451905011, 1, 'Member', 'One', NULL, NULL),
(2, '::1', NULL, '$2y$08$wukx4fmRjX5fql8JEsCkceIw.zHhH6/7o932Sj6HIxe6jG0Rf13pO', NULL, 'tanawa_m@epita.fr', NULL, NULL, NULL, NULL, 1462180943, 1462180986, 1, 'Tanawa Tsamo', 'Marius', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_groups`
--
ALTER TABLE `admin_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login_attempts`
--
ALTER TABLE `admin_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users_groups`
--
ALTER TABLE `admin_users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_access`
--
ALTER TABLE `api_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_keys`
--
ALTER TABLE `api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_limits`
--
ALTER TABLE `api_limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_logs`
--
ALTER TABLE `api_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_blog_categories`
--
ALTER TABLE `demo_blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_blog_posts`
--
ALTER TABLE `demo_blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_blog_posts_tags`
--
ALTER TABLE `demo_blog_posts_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_blog_tags`
--
ALTER TABLE `demo_blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_cover_photos`
--
ALTER TABLE `demo_cover_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moulibex`
--
ALTER TABLE `moulibex`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `suivi_stockage`
--
ALTER TABLE `suivi_stockage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `suivi_stockage_type_fichier`
--
ALTER TABLE `suivi_stockage_type_fichier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `table2`
--
ALTER TABLE `table2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mc` (`mc`);

--
-- Indexes for table `type_fichier`
--
ALTER TABLE `type_fichier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `uploaded_file`
--
ALTER TABLE `uploaded_file`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_groups`
--
ALTER TABLE `admin_groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_login_attempts`
--
ALTER TABLE `admin_login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `admin_users_groups`
--
ALTER TABLE `admin_users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `api_access`
--
ALTER TABLE `api_access`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_keys`
--
ALTER TABLE `api_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_limits`
--
ALTER TABLE `api_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_logs`
--
ALTER TABLE `api_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `demo_blog_categories`
--
ALTER TABLE `demo_blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `demo_blog_posts`
--
ALTER TABLE `demo_blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `demo_blog_posts_tags`
--
ALTER TABLE `demo_blog_posts_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `demo_blog_tags`
--
ALTER TABLE `demo_blog_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `demo_cover_photos`
--
ALTER TABLE `demo_cover_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `moulibex`
--
ALTER TABLE `moulibex`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suivi_stockage`
--
ALTER TABLE `suivi_stockage`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suivi_stockage_type_fichier`
--
ALTER TABLE `suivi_stockage_type_fichier`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `table2`
--
ALTER TABLE `table2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4851;
--
-- AUTO_INCREMENT for table `type_fichier`
--
ALTER TABLE `type_fichier`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uploaded_file`
--
ALTER TABLE `uploaded_file`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;